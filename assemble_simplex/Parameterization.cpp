#include "Parameterization.h"
#include "SPARSE\Sparse_Matrix.h"
#include "SPARSE\Sparse_Solver.h"
#include "graph_coloring.h"

parameterization_interface::parameterization_interface()
{
	prepare_ok = false;
}

parameterization_interface::~parameterization_interface()
{
}

void parameterization_interface::prepare_data(Mesh* mesh_)
{
	if (prepare_ok) return;
	unsigned nv = mesh_->n_vertices(); unsigned nf = mesh_->n_faces();
	min_area.clear(); min_area.resize(nv, 0.0);
	std::vector<double> cot_omega(mesh_->n_halfedges(), 0.0);
	OpenMesh::Vec3d p0, p1, p2;  std::vector<double> ori_face_area(nf);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		for (Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it); fhe_it; ++fhe_it)
		{
			p0 = mesh_->point(mesh_->from_vertex_handle(fhe_it));
			p1 = mesh_->point(mesh_->to_vertex_handle(fhe_it));
			p2 = mesh_->point(mesh_->to_vertex_handle(mesh_->next_halfedge_handle(fhe_it)));
			double sin_a = OpenMesh::cross(p0 - p2, p1 - p2).norm();
			double cos_a = OpenMesh::dot(p0 - p2, p1 - p2);
			cot_omega[fhe_it.handle().idx()] = cos_a / sin_a;
			ori_face_area[f_it->idx()] = sin_a;
		}
	}

	new_p_x.resize(nv); new_p_y.resize(nv);
	vertex_v1.resize(nv); vertex_v2.resize(nv); face_area.resize(nv); inv_face_area.resize(nv);
	vertex_face_id.resize(nv); face_vertex_id.resize(mesh_->n_faces());
	omega0.resize(nv); omega1.resize(nv); omega2.resize(nv);
	vertex_radius_ratio.resize(nv); std::vector<int> boundary_v;

	max_vv_size = 0;
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		OpenMesh::Vec3d& p0 = mesh_->data(v_it).get_New_Pos();
		int v_id = v_it.handle().idx(); vertex_radius_ratio[v_id] = 1.0;

		OpenMesh::Vec3d p = mesh_->point(v_it);
		std::vector<double> f_area_vec; std::vector<double> inv_f_area_vec;
		std::vector<int> v1_vec; std::vector<int> v2_vec;
		std::vector<int> one_vertex_face;
		std::vector<double> omega0_vec; std::vector<double> omega1_vec; std::vector<double> omega2_vec;
		/*std::vector<std::vector<double> > vertex_S_vec; std::vector<double> one_S(4);
		Eigen::Matrix2d A, B;*/
		for (Mesh::VertexOHalfedgeIter voh_it = mesh_->voh_iter(v_it); voh_it; ++voh_it)
		{
			Mesh::FaceHandle fh = mesh_->face_handle(voh_it);
			if (fh != Mesh::InvalidFaceHandle)
			{
				OpenMesh::HalfedgeHandle heh0 = mesh_->next_halfedge_handle(voh_it);
				OpenMesh::HalfedgeHandle heh1 = mesh_->prev_halfedge_handle(voh_it);
				OpenMesh::HalfedgeHandle heh2 = voh_it.handle();
				/*double omega0_ = mesh_->data(heh0).get_opposite_angle();
				double omega1_ = mesh_->data(heh1).get_opposite_angle();
				double omega2_ = mesh_->data(heh2).get_opposite_angle();*/
				omega0_vec.push_back(cot_omega[heh0.idx()]); omega1_vec.push_back(cot_omega[heh1.idx()]); omega2_vec.push_back(cot_omega[heh2.idx()]);
				v1_vec.push_back(mesh_->to_vertex_handle(voh_it).idx());
				v2_vec.push_back(mesh_->to_vertex_handle(mesh_->next_halfedge_handle(voh_it)).idx());

				/*OpenMesh::Vec3d p1 = mesh_->point(mesh_->to_vertex_handle(voh_it));
				OpenMesh::Vec3d p2 = mesh_->point(mesh_->to_vertex_handle(mesh_->next_halfedge_handle(voh_it)));
				OpenMesh::Vec3d n = OpenMesh::cross(p1 - p, p2 - p); n.normalize();
				OpenMesh::Vec3d e1 = (p1 - p); e1.normalize();
				OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
				A(0, 0) = OpenMesh::dot(p1 - p, e1); A(1, 0) = OpenMesh::dot(p1 - p, e2);
				A(0, 1) = OpenMesh::dot(p2 - p, e1); A(1, 1) = OpenMesh::dot(p2 - p, e2);
				B = A.inverse();
				one_S[0] = B(0, 0); one_S[1] = B(0, 1); one_S[2] = B(1, 0); one_S[3] = B(1, 1);
				vertex_S_vec.push_back(one_S);*/

				f_area_vec.push_back(ori_face_area[fh.idx()]);
				inv_f_area_vec.push_back(1.0 / f_area_vec.back());

				one_vertex_face.push_back(fh.idx());
			}
		}
		vertex_face_id[v_id] = one_vertex_face;

		omega0[v_id] = omega0_vec; omega1[v_id] = omega1_vec; omega2[v_id] = omega2_vec;
		vertex_v1[v_id] = v1_vec; vertex_v2[v_id] = v2_vec;
		face_area[v_id] = f_area_vec; inv_face_area[v_id] = inv_f_area_vec;
		//vertex_S.push_back(vertex_S_vec);

		int vv_size = vertex_v1[v_id].size();
		if (vv_size > max_vv_size)
		{
			max_vv_size = vv_size;
		}
	}

	//face vertex
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		for (Mesh::FaceVertexIter fv_it = mesh_->fv_iter(f_it); fv_it; ++fv_it)
		{
			face_vertex_id[face_id].push_back(fv_it.handle().idx());
		}
	}

	int max_num_t = omp_get_max_threads();
	p1_vec_x_omp.resize(max_num_t); p1_vec_y_omp.resize(max_num_t);
	p2_vec_x_omp.resize(max_num_t); p2_vec_y_omp.resize(max_num_t);
	l0_n_vec_omp.resize(max_num_t); exp_vec_omp.resize(max_num_t);
	//above_th_omp.resize(max_num_t);
	for (int i = 0; i < max_num_t; ++i)
	{
		p1_vec_x_omp[i].resize(max_vv_size); p1_vec_y_omp[i].resize(max_vv_size);
		p2_vec_x_omp[i].resize(max_vv_size); p2_vec_y_omp[i].resize(max_vv_size);
		l0_n_vec_omp[i].resize(max_vv_size); exp_vec_omp[i].resize(max_vv_size);
		//above_th_omp[i].resize(max_vv_size);
	}

	//coloring
	graph_color_edge.clear(); graph_color_edge.reserve(mesh_->n_edges());
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		Mesh::HalfedgeHandle heh = mesh_->halfedge_handle(e_it, 0);
		int v0 = mesh_->to_vertex_handle(heh).idx();
		int v1 = mesh_->from_vertex_handle(heh).idx();
		graph_color_edge.push_back(OpenMesh::Vec2i(v0,v1));
	}
	graph_coloring(nv, graph_color_edge, v_same_color);

	//amips
	c_v_ratio = 0; amips_s = 4; un_eps = 1e-12; inv_un_eps = 1.0 / un_eps;

	prepare_ok = true;
}

//from CGAL
//void parameterization_interface::setup_two_constraint_LSCM(Mesh* mesh_)
//{
//	// Get mesh's bounding box
//	double xmin = (std::numeric_limits<double>::max)();
//	double ymin = (std::numeric_limits<double>::max)();
//	double zmin = (std::numeric_limits<double>::max)();
//	double xmax = (std::numeric_limits<double>::min)();
//	double ymax = (std::numeric_limits<double>::min)();
//	double zmax = (std::numeric_limits<double>::min)();
//	Mesh::VertexIter v_it;
//	for (v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
//	{
//		if (!mesh_->is_boundary(v_it))
//		{
//			continue;
//		}
//
//		Mesh::Point position = mesh_->point(v_it);
//
//		xmin = (std::min)(position[0], xmin);
//		ymin = (std::min)(position[1], ymin);
//		zmin = (std::min)(position[2], zmin);
//
//		xmax = (std::max)(position[0], xmax);
//		ymax = (std::max)(position[1], ymax);
//		zmax = (std::max)(position[2], zmax);
//	}
//
//	// Find longest bounding box axes
//	double dx = xmax - xmin;
//	double dy = ymax - ymin;
//	double dz = zmax - zmin;
//	enum { X_AXIS, Y_AXIS, Z_AXIS } longest_axis, second_longest_axis;
//	if (dx < dy && dx < dz)
//	{
//		if (dy > dz)
//		{
//			longest_axis = Y_AXIS;
//			second_longest_axis = Z_AXIS;
//		}
//		else
//		{
//			longest_axis = Z_AXIS;
//			second_longest_axis = Y_AXIS;
//		}
//	}
//	else if (dy < dx && dy < dz)
//	{
//		if (dx > dz)
//		{
//			longest_axis = X_AXIS;
//			second_longest_axis = Z_AXIS;
//		}
//		else
//		{
//			longest_axis = Z_AXIS;
//			second_longest_axis = X_AXIS;
//		}
//	}
//	else
//	{ // (dz < dx && dz < dy)
//		if (dx > dy)
//		{
//			longest_axis = X_AXIS;
//			second_longest_axis = Y_AXIS;
//		}
//		else
//		{
//			longest_axis = Y_AXIS;
//			second_longest_axis = X_AXIS;
//		}
//	}
//	Mesh::Point V1,                // bounding box' longest axis
//		V2;               // bounding box' 2nd longest axis
//	double V1_min = 0, V1_max = 0;  // bounding box' dimensions along V1
//	double V2_min = 0, V2_max = 0;  // bounding box' dimensions along V2
//	switch (longest_axis)
//	{
//	case X_AXIS:
//		V1 = Mesh::Point(1.0, 0.0, 0.0);
//		V1_min = xmin;
//		V1_max = xmax;
//		break;
//	case Y_AXIS:
//		V1 = Mesh::Point(0.0, 1.0, 0.0);
//		V1_min = ymin;
//		V1_max = ymax;
//		break;
//	case Z_AXIS:
//		V1 = Mesh::Point(0.0, 0.0, 1.0);
//		V1_min = zmin;
//		V1_max = zmax;
//		break;
//	}
//	switch (second_longest_axis)
//	{
//	case X_AXIS:
//		V2 = Mesh::Point(1.0, 0.0, 0.0);
//		V2_min = xmin;
//		V2_max = xmax;
//		break;
//	case Y_AXIS:
//		V2 = Mesh::Point(0.0, 1.0, 0.0);
//		V2_min = ymin;
//		V2_max = ymax;
//		break;
//	case Z_AXIS:
//		V2 = Mesh::Point(0.0, 0.0, 1.0);
//		V2_min = zmin;
//		V2_max = zmax;
//		break;
//	}
//
//	// Project onto longest bounding box axes,
//	// Set extrema vertices' (u,v) in unit square and mark them as "parameterized"
//	Mesh::VertexHandle vxmin; Mesh::VertexHandle vxmax;
//	double vmin = (std::numeric_limits<double>::max)(), vmax = (std::numeric_limits<double>::min)();
//	double umin = (std::numeric_limits<double>::max)(); double umax = (std::numeric_limits<double>::min)();
//	for (v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
//	{
//		if (!mesh_->is_boundary(v_it))
//		{
//			continue;
//		}
//
//		Mesh::Point position = mesh_->point(v_it);
//		Mesh::Point position_as_vector = position - Mesh::Point(0, 0, 0);
//
//		// coordinate along the bounding box' main axes
//		double u = OpenMesh::dot(position_as_vector, V1);
//		double v = OpenMesh::dot(position_as_vector, V2);
//
//		// convert to unit square coordinates
//		//CGAL_surface_mesh_parameterization_assertion(V1_max > V1_min);
//		//CGAL_surface_mesh_parameterization_assertion(V2_max > V2_min);
//		u = (u - V1_min) / (V1_max - V1_min);
//		v = (v - V2_min) / (V2_max - V2_min);
//
//		//mesh.set_vertex_uv(it, Point_2(u,v)) ; // useful only for vxmin and vxmax
//
//		if (u < umin || (u == umin && v < vmin))
//		{
//			vxmin = v_it;
//			umin = u;
//			vmin = v;
//		}
//		if (u > umax || (u == umax && v > vmax))
//		{
//			vxmax = v_it;
//			umax = u;
//			vmax = v;
//		}
//	}
//
//	umin *= 100.0; vmin *= 100.0; umax *= 100.0; vmax *= 100.0;
//	mesh_->data(vxmin).set_new_pos_fixed(true); mesh_->data(vxmin).set_New_Pos(OpenMesh::Vec3d(umin, vmin, 0.0));
//	mesh_->data(vxmax).set_new_pos_fixed(true); mesh_->data(vxmax).set_New_Pos(OpenMesh::Vec3d(umax, vmax, 0.0));
//}

void parameterization_interface::find_all_UV_using_angle(std::vector<double>& angle, Mesh* mesh_)
{
	//fix two points
	int nv = mesh_->n_vertices(); int nf = mesh_->n_faces();
	Mesh::FaceHandle fh = mesh_->face_handle(0);
	Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(fh);
	double length = mesh_->calc_edge_length(fhe_it);
	std::vector<bool> fix_vertex_id(nv, false);
	std::vector<Mesh::VertexHandle> fix_vertex;
	fix_vertex.push_back(mesh_->from_vertex_handle(fhe_it)); fix_vertex_id[mesh_->from_vertex_handle(fhe_it).idx()] = true;
	mesh_->data(mesh_->from_vertex_handle(fhe_it)).set_New_Pos(OpenMesh::Vec3d(0.0, 0.0, 0.0));
	mesh_->data(mesh_->from_vertex_handle(fhe_it)).set_new_pos_fixed(true);
	fix_vertex.push_back(mesh_->to_vertex_handle(fhe_it)); fix_vertex_id[mesh_->to_vertex_handle(fhe_it).idx()] = true;
	mesh_->data(mesh_->to_vertex_handle(fhe_it)).set_New_Pos(OpenMesh::Vec3d(length, 0.0, 0.0));
	mesh_->data(mesh_->to_vertex_handle(fhe_it)).set_new_pos_fixed(true);

	int vertex_count_ok = 0;
	std::vector<int> map_with_vertex_UV(nv, -1);
	for (unsigned int i = 0; i < fix_vertex_id.size(); ++i)
	{
		if (fix_vertex_id[i])
		{
			continue;
		}
		else
		{
			map_with_vertex_UV[i] = vertex_count_ok;
			++vertex_count_ok;
		}
	}
	Mesh::FaceIter f_it; int face_id; int var_id;
	Mesh::HalfedgeHandle next_heh; Mesh::HalfedgeHandle prev_heh;
	int next_var_id; int prev_var_id;
	Sparse_Matrix M(2 * nf, 2 * (nv - 2));
	for (f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		face_id = f_it.handle().idx();
		fhe_it = mesh_->fh_iter(f_it);
		var_id = face_id * 3 + mesh_->data(fhe_it).get_face_he_var();
		next_heh = mesh_->next_halfedge_handle(fhe_it); next_var_id = face_id * 3 + mesh_->data(next_heh).get_face_he_var();
		prev_heh = mesh_->prev_halfedge_handle(fhe_it); prev_var_id = face_id * 3 + mesh_->data(prev_heh).get_face_he_var();

		double scale = std::sin(angle[next_var_id]) / std::sin(angle[prev_var_id]);
		double cos_1 = scale * std::cos(angle[var_id]); double sin_1 = scale * std::sin(angle[var_id]);

		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(next_heh);
		Mesh::VertexHandle v3 = mesh_->to_vertex_handle(prev_heh);
		//v1
		if (mesh_->data(v1).get_new_pos_fixed())
		{
			OpenMesh::Vec3d p1 = mesh_->data(v1).get_New_Pos();
			M.fill_rhs_entry(2 * face_id + 0, (-cos_1 + 1.0)*p1[0] + sin_1*p1[1]);
			M.fill_rhs_entry(2 * face_id + 1, -sin_1*p1[0] - cos_1*p1[1] + p1[1]);
		}
		else
		{
			M.fill_entry(2 * face_id + 0, 2 * map_with_vertex_UV[v1.idx()] + 0, cos_1 - 1.0);
			M.fill_entry(2 * face_id + 0, 2 * map_with_vertex_UV[v1.idx()] + 1, -sin_1);

			M.fill_entry(2 * face_id + 1, 2 * map_with_vertex_UV[v1.idx()] + 0, sin_1);
			M.fill_entry(2 * face_id + 1, 2 * map_with_vertex_UV[v1.idx()] + 1, cos_1 - 1.0);
		}
		//v2
		if (mesh_->data(v2).get_new_pos_fixed())
		{
			OpenMesh::Vec3d p2 = mesh_->data(v2).get_New_Pos();
			M.fill_rhs_entry(2 * face_id + 0, cos_1*p2[0] - sin_1*p2[1]);
			M.fill_rhs_entry(2 * face_id + 1, sin_1*p2[0] + cos_1*p2[1]);
		}
		else
		{
			M.fill_entry(2 * face_id + 0, 2 * map_with_vertex_UV[v2.idx()] + 0, -cos_1);
			M.fill_entry(2 * face_id + 0, 2 * map_with_vertex_UV[v2.idx()] + 1, +sin_1);

			M.fill_entry(2 * face_id + 1, 2 * map_with_vertex_UV[v2.idx()] + 0, -sin_1);
			M.fill_entry(2 * face_id + 1, 2 * map_with_vertex_UV[v2.idx()] + 1, -cos_1);
		}
		//v3
		if (mesh_->data(v3).get_new_pos_fixed())
		{
			OpenMesh::Vec3d p3 = mesh_->data(v3).get_New_Pos();
			M.fill_rhs_entry(2 * face_id + 0, -p3[0]);
			M.fill_rhs_entry(2 * face_id + 1, -p3[1]);
		}
		else
		{
			M.fill_entry(2 * face_id + 0, 2 * map_with_vertex_UV[v3.idx()] + 0, 1.0);
			M.fill_entry(2 * face_id + 1, 2 * map_with_vertex_UV[v3.idx()] + 1, 1.0);
		}
	}

	Sparse_Matrix* N = TransposeTimesSelf(&M, CCS, SYM_LOWER, true);
	solve_by_CHOLMOD(N);

	std::vector<double> X_UV = N->get_solution();
	OpenMesh::Vec3d uv(0, 0, 0);
	Mesh::VertexIter v_it; int vertex_id;
	for (v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		vertex_id = v_it.handle().idx();
		if (!fix_vertex_id[vertex_id])
		{
			uv[0] = X_UV[2 * map_with_vertex_UV[vertex_id] + 0];//x
			uv[1] = X_UV[2 * map_with_vertex_UV[vertex_id] + 1];//y
			mesh_->data(v_it).set_New_Pos(uv);
		}
		else
		{
			mesh_->data(v_it).set_new_pos_fixed(false);
		}
	}

	delete N;
}

//void parameterization_interface::LSCM(Mesh* mesh_)
//{
//	setup_two_constraint_LSCM(mesh_);
//	int nv = mesh_->n_vertices(); int nf = mesh_->n_faces();
//	//prepare data
//	//prepare_data(mesh_);
//	std::vector<std::vector<int> > all_fv_id(nf); 
//	std::vector<std::vector<double>> M_T(nf); std::vector<double> ori_face_area(nf);
//	std::vector<int> fv_id(3); std::vector<double> face_M_T(6);
//	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
//	{
//		int face_id = f_it.handle().idx();
//		Mesh::FaceVertexIter fv_it = mesh_->fv_iter(f_it);
//		OpenMesh::Vec3d& p0 = mesh_->point(fv_it); fv_id[0] = fv_it.handle().idx();
//		++fv_it; OpenMesh::Vec3d& p1 = mesh_->point(fv_it); fv_id[1] = fv_it.handle().idx();
//		++fv_it; OpenMesh::Vec3d& p2 = mesh_->point(fv_it); fv_id[2] = fv_it.handle().idx();
//
//		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0);
//		ori_face_area[face_id] = n.norm(); n.normalize();
//
//		OpenMesh::Vec3d e1 = (p1 - p0);
//		double x1 = e1.norm(); e1.normalize();
//		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
//		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);
//		face_M_T[0] = -y2 / ori_face_area[face_id]; face_M_T[1] = y2 / ori_face_area[face_id]; face_M_T[2] = 0;
//		face_M_T[3] = (x2 - x1) / ori_face_area[face_id]; face_M_T[4] = -x2 / ori_face_area[face_id]; face_M_T[5] = x1 / ori_face_area[face_id];
//
//		ori_face_area[face_id] *= 0.5;
//		M_T[face_id] = face_M_T; all_fv_id[face_id] = fv_id;
//	}
//
//	std::vector<int> map_vertex_index(nv, -1); int v_ind = 0;
//	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
//	{
//		if (!mesh_->data(v_it).get_new_pos_fixed())
//		{
//			map_vertex_index[v_it.handle().idx()] = v_ind; ++v_ind;
//		}
//	}
//
//	int cols = 2 * nv - 4; int rows = 2 * nf;
//	Sparse_Matrix A(rows, cols, NOSYM, CCS, 1);
//
//	for (int face_id = 0; face_id < nf; ++face_id)
//	{
//		const std::vector<double>& M = M_T[face_id];
//		const std::vector<int>& fv_id = all_fv_id[face_id];
//		double sqrt_fa = std::sqrt(ori_face_area[face_id]);
//
//		for (int i = 0; i < 3; ++i)
//		{
//			int x_id = map_vertex_index[fv_id[i]];
//			if (x_id < 0)
//			{
//				OpenMesh::Vec3d& p = mesh_->data(mesh_->vertex_handle(fv_id[i])).get_New_Pos();
//				A.fill_rhs_entry(face_id * 2 + 0, -(M[i] * p[1] + M[3 + i] * p[0])* sqrt_fa);
//				A.fill_rhs_entry(face_id * 2 + 1, -(M[3 + i] * p[1] - M[i] * p[0])* sqrt_fa);
//			}
//			else
//			{
//				A.fill_entry(face_id * 2 + 0, 2 * x_id + 1, M[i] * sqrt_fa);     A.fill_entry(face_id * 2 + 0, 2 * x_id + 0, M[3 + i] * sqrt_fa);
//				A.fill_entry(face_id * 2 + 1, 2 * x_id + 1, M[i + 3] * sqrt_fa); A.fill_entry(face_id * 2 + 1, 2 * x_id + 0, -M[i] * sqrt_fa);
//			}
//		}
//	}
//
//	Sparse_Matrix* B = TransposeTimesSelf(&A, CCS, SYM_LOWER, true);
//	solve_by_CHOLMOD(B);
//	const std::vector<double>& uv = B->get_solution();
//	for (int i = 0; i < map_vertex_index.size(); ++i)
//	{
//		int x_id = map_vertex_index[i];
//		Mesh::VertexHandle vh = mesh_->vertex_handle(i);
//		if (x_id >= 0)
//		{
//			mesh_->data(vh).set_New_Pos(OpenMesh::Vec3d(uv[2 * x_id + 0], uv[2 * x_id + 1], 0));
//		}
//		else
//		{
//			mesh_->data(vh).set_new_pos_fixed(false);
//		}
//	}
//
//	delete B;
//}

void parameterization_interface::LABF(Mesh* mesh_)
{
	double th_angle_defect = 0.7;
	int nf = mesh_->n_faces();
	std::vector<double> source_angle(3 * nf, 0);
	std::vector<double> angle(3 * nf, 0);
	int nv = mesh_->n_vertices();
	std::vector<double> angle_defect(nv, 0.0);
	Mesh::FaceIter f_it; Mesh::FaceHalfedgeIter fhe_it;
	int face_id; int var_id; int vertex_id;
	OpenMesh::Vec3d p0; OpenMesh::Vec3d p1; OpenMesh::Vec3d p2;
	for (f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		face_id = f_it.handle().idx();
		fhe_it = mesh_->fh_iter(f_it);
		int count = 0;
		for (fhe_it; fhe_it; ++fhe_it)
		{
			var_id = 3 * face_id + count;
			p0 = mesh_->point(mesh_->to_vertex_handle(fhe_it));
			p1 = mesh_->point(mesh_->from_vertex_handle(fhe_it));
			p2 = mesh_->point(mesh_->to_vertex_handle(mesh_->next_halfedge_handle(fhe_it)));

			p1 -= p0; p2 -= p0;
			double angle_cos = OpenMesh::dot(p1, p2) / (p1.norm()*p2.norm());
			source_angle[var_id] = std::acos(angle_cos);

			vertex_id = mesh_->to_vertex_handle(fhe_it).idx();
			angle_defect[vertex_id] += source_angle[var_id];

			mesh_->data(fhe_it).set_face_he_var(count);
			++count;
		}
	}

	Mesh::VertexIter v_it; Mesh::VertexIHalfedgeIter vih_it;
	int count_in_vertex = 0;
	std::vector<int> map_with_in_vertex(nv, -1);
	for (v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		vertex_id = v_it.handle().idx();

		double coef = 1.0;
		if (!mesh_->is_boundary(v_it))
		{
			map_with_in_vertex[vertex_id] = count_in_vertex;
			++count_in_vertex;

			if (std::abs(2 * M_PI - angle_defect[vertex_id]) >= th_angle_defect)
			{
				coef = 2 * M_PI / angle_defect[vertex_id];
			}
		}
		else
		{
			if (2 * M_PI - angle_defect[vertex_id] < 0)
			{
				//printf("Boundary vertex %d angle: %e\n", vertex_id, 2 * M_PI - angle_defect[vertex_id]);
			}
		}

		vih_it = mesh_->vih_iter(v_it);
		for (vih_it; vih_it; ++vih_it)
		{
			face_id = mesh_->face_handle(vih_it).idx();
			if (face_id >= 0)
			{
				var_id = face_id * 3 + mesh_->data(vih_it).get_face_he_var();
				angle[var_id] = source_angle[var_id] * coef;
			}

		}
	}

	Sparse_Matrix C(3 * nf, nf + 2 * count_in_vertex);
	std::vector<double> b(nf + 2 * count_in_vertex, 0.0);
	for (f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		face_id = f_it.handle().idx();
		fhe_it = mesh_->fh_iter(f_it);
		b[face_id] = M_PI;
		for (fhe_it; fhe_it; ++fhe_it)
		{
			var_id = 3 * face_id + mesh_->data(fhe_it).get_face_he_var();
			C.fill_entry(var_id, face_id, angle[var_id]);
			b[face_id] -= angle[var_id];
		}
	}

	Mesh::HalfedgeHandle next_heh; int next_var_id;
	Mesh::HalfedgeHandle prev_heh; int prev_var_id;
	for (v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		if (mesh_->is_boundary(v_it))
		{
			continue;
		}

		vertex_id = v_it.handle().idx();
		vih_it = mesh_->vih_iter(v_it);

		b[nf + map_with_in_vertex[vertex_id]] = 2 * M_PI;
		b[nf + count_in_vertex + map_with_in_vertex[vertex_id]] = 0.0;
		for (vih_it; vih_it; ++vih_it)
		{
			face_id = mesh_->face_handle(vih_it).idx();
			if (face_id >= 0)
			{
				var_id = face_id * 3 + mesh_->data(vih_it).get_face_he_var();

				C.fill_entry(var_id, nf + map_with_in_vertex[vertex_id], angle[var_id]);
				b[nf + map_with_in_vertex[vertex_id]] -= angle[var_id];

				next_heh = mesh_->next_halfedge_handle(vih_it); next_var_id = face_id * 3 + mesh_->data(next_heh).get_face_he_var();
				prev_heh = mesh_->prev_halfedge_handle(vih_it); prev_var_id = face_id * 3 + mesh_->data(prev_heh).get_face_he_var();

				C.fill_entry(next_var_id, nf + count_in_vertex + map_with_in_vertex[vertex_id], +angle[next_var_id] / std::tan(angle[next_var_id]));
				C.fill_entry(prev_var_id, nf + count_in_vertex + map_with_in_vertex[vertex_id], -angle[prev_var_id] / std::tan(angle[prev_var_id]));
				b[nf + count_in_vertex + map_with_in_vertex[vertex_id]] += (std::log(std::sin(angle[prev_var_id])) - std::log(std::sin(angle[next_var_id])));
			}
		}
	}

	Sparse_Matrix* B = TransposeTimesSelf(&C, CCS, SYM_LOWER, false);
	for (unsigned int i = 0; i<b.size(); ++i)
	{
		B->set_rhs_entry(i, b[i]);
	}

	solve_by_CHOLMOD(B);

	std::vector<double> X = B->get_solution();
	std::vector<double> r(3 * nf);
	multiply(&C, &X[0], &r[0]);

	//verify
	/*std::vector<double> rr(b.size());
	transpose_multiply(&C,&r[0],&rr[0]);
	for(unsigned int i=0;i<rr.size();++i)
	{
	printf("%20.18f\n", rr[i] - b[i]);
	}*/
	delete B;

	double max_abs_r = 0.0; int max_r_index = -1; double max_source_angle = 0.0;
	for (unsigned int i = 0; i<angle.size(); ++i)
	{
		if (std::abs(r[i]) > max_abs_r)
		{
			max_abs_r = std::abs(r[i]);
			max_r_index = i;
			max_source_angle = angle[i];
		}

		angle[i] += r[i] * angle[i];
	}
	//printf("[Max Relative Error] : %20.18f; source angle : %f, new angle : %f\n", max_abs_r, max_source_angle, angle[max_r_index]);

	find_all_UV_using_angle(angle, mesh_);
}

void parameterization_interface::find_next_boundary_vertex(Mesh* mesh_, Mesh::VertexHandle& vh, Mesh::VertexHandle& vh0)
{
	for (Mesh::VertexOHalfedgeIter voh_it = mesh_->voh_iter(vh); voh_it; ++voh_it)
	{
		Mesh::HalfedgeHandle opp_heh = mesh_->opposite_halfedge_handle(voh_it);
		if (mesh_->face_handle(opp_heh) == Mesh::InvalidFaceHandle)
		{
			vh0 = mesh_->to_vertex_handle(voh_it); return;
		}
	}
}

void parameterization_interface::compute_circle_UV(Mesh* mesh_)
{
	int start_e = -1;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		if (mesh_->is_boundary(e_it))
		{
			if (start_e < 0) start_e = e_it.handle().idx(); break;
		}
	}

	Mesh::EdgeHandle se = mesh_->edge_handle(start_e);
	Mesh::HalfedgeHandle heh = mesh_->halfedge_handle(se, 0);
	Mesh::VertexHandle vh = mesh_->to_vertex_handle(heh);
	if (mesh_->face_handle(heh) != Mesh::InvalidFaceHandle)
	{
		vh = mesh_->from_vertex_handle(heh);
	}
	double bv_count = 1.0;
	Mesh::VertexHandle svh = vh;
	Mesh::VertexHandle tvh = Mesh::InvalidVertexHandle;
	OpenMesh::Vec3d bbMin = mesh_->point(vh); OpenMesh::Vec3d bbMax = bbMin;
	std::vector<Mesh::VertexHandle> bvh; bvh.push_back(vh);
	std::vector<double> bvh_alpha; bvh_alpha.push_back(0.0);
	while (1)
	{
		find_next_boundary_vertex(mesh_,vh, tvh);
		if (tvh == svh) break;
		bvh.push_back(tvh);
		vh = tvh;

		bbMin.minimize(OpenMesh::vector_cast<OpenMesh::Vec3d>(mesh_->point(vh)));
		bbMax.maximize(OpenMesh::vector_cast<OpenMesh::Vec3d>(mesh_->point(vh)));

		bv_count += 1.0;
	}
	double r = (bbMin - bbMax).norm()*0.5;
	double delta_alpha = 2 * M_PI / bv_count;
	for (unsigned i = 0; i < bvh.size(); ++i)
	{
		mesh_->data(bvh[i]).set_new_pos_fixed(true);
		mesh_->data(bvh[i]).set_New_Pos(OpenMesh::Vec3d(std::cos(delta_alpha*i), std::sin(delta_alpha*i), 0));
	}

	//solve
	unsigned nv = mesh_->n_vertices();
	std::vector<int> map_with_vid(nv, -1); int var_count = 0;
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		if (!mesh_->data(v_it).get_new_pos_fixed())
		{
			map_with_vid[v_it.handle().idx()] = var_count; ++var_count;
		}
	}
	Sparse_Matrix A(var_count, var_count, SYM_LOWER, CCS, 2);
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		if (!mesh_->data(v_it).get_new_pos_fixed())
		{
			int var_i = map_with_vid[v_it.handle().idx()]; double vv_count = 0.0;
			for (Mesh::VertexOHalfedgeIter voh_it = mesh_->voh_iter(v_it); voh_it; ++voh_it)
			{
				Mesh::VertexHandle vh = mesh_->to_vertex_handle(voh_it);
				int var_j = map_with_vid[vh.idx()];
				if (var_j < 0)
				{
					const OpenMesh::Vec3d& np = mesh_->data(vh).get_New_Pos();
					A.fill_rhs_entry(var_i, 0, np[0]);
					A.fill_rhs_entry(var_i, 1, np[1]);
				}
				else
				{
					if (var_i > var_j)
					{
						A.fill_entry(var_i, var_j, -1.0);
					}
				}
				vv_count += 1.0;
			}
			A.fill_entry(var_i, var_i, vv_count);
		}
	}

	solve_by_CHOLMOD(&A);

	const std::vector<double>& uv = A.get_solution();
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		if (!mesh_->data(v_it).get_new_pos_fixed())
		{
			int var_i = map_with_vid[v_it.handle().idx()];
			mesh_->data(v_it).set_New_Pos(OpenMesh::Vec3d(uv[var_i], uv[var_i + var_count], 0.0));
		}
		else
		{
			mesh_->data(v_it).set_new_pos_fixed(false);
		}
	}
}

void parameterization_interface::untangling(Mesh* mesh_, int max_iter_num)
{
	if (!prepare_ok) prepare_data(mesh_);
	
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		int v_id = v_it.handle().idx();
		OpenMesh::Vec3d& p = mesh_->data(v_it).get_New_Pos();
		new_p_x[v_id] = p[0]; new_p_y[v_id] = p[1];
	}
	double min_a = 0.0;
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		Mesh::FaceVertexIter fv_it = mesh_->fv_iter(f_it);
		OpenMesh::Vec3d& p0 = mesh_->data(fv_it).get_New_Pos();
		++fv_it; OpenMesh::Vec3d& p1 = mesh_->data(fv_it).get_New_Pos();
		++fv_it; OpenMesh::Vec3d& p2 = mesh_->data(fv_it).get_New_Pos();
		double area = OpenMesh::cross(p0 - p2, p1 - p2)[2];

		if (area < min_a) min_a = area;
	}
	if (min_a < 0)
	{
		un_eps = min_a*min_a*1e-12;
		if (un_eps < -2 * min_a*1e-10)
		{
			un_eps = -2 * min_a*1e-10;
		}
	}
	else
	{
		un_eps = 1e-10;
	}
	inv_un_eps = 1.0 / un_eps;

	int nv = mesh_->n_vertices();

	long start_t = clock();
	int iter_count = 0; int n_color_ = v_same_color.size();
	while (iter_count < max_iter_num)
	{
		for (unsigned i = 0; i < n_color_; ++i)
		{
			int one_color_size = v_same_color[i].size();
#if 1
#pragma omp parallel for schedule(dynamic)
			for (int j = 0; j < one_color_size; ++j)
			{
				int id = omp_get_thread_num();
				Untangling_one_V(v_same_color[i][j], id);
			}
#else
			for (int j = 0; j < one_color_size; ++j)
			{
				int id = omp_get_thread_num();
				Untangling_one_V(v_same_color[i][j], id);
			}
#endif
			double min_a = 1.0;
			for (int j = 0; j < one_color_size; ++j)
			{
				if (min_area[v_same_color[i][j]] < min_a) min_a = min_area[v_same_color[i][j]];
			}
			if (min_a < 0)
			{
				un_eps = min_a*min_a*1e-12;
				if (un_eps < -2 * min_a*1e-10)
				{
					un_eps = -2 * min_a*1e-10;
				}
			}
			else
			{
				un_eps = 1e-10;
			}
			inv_un_eps = 1.0 / un_eps;
			//printf("%d %e\n", i, un_eps);
		}

		++iter_count;
	}
	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	printf("Untangling: %f s\n", t);

	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		int v_id = v_it.handle().idx();
		OpenMesh::Vec3d p(new_p_x[v_id], new_p_y[v_id], 0);
		mesh_->data(v_it).set_New_Pos(p);
	}
}

void parameterization_interface::AMIPS(Mesh* mesh_, int max_iter_num, bool is_conformal)
{
	if (!prepare_ok) prepare_data(mesh_);

	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		int v_id = v_it.handle().idx();
		OpenMesh::Vec3d& p = mesh_->data(v_it).get_New_Pos();
		new_p_x[v_id] = p[0];
		new_p_y[v_id] = p[1];
	}
	
	if (is_conformal) c_v_ratio = 0.0;
	else c_v_ratio = 0.5;

	long start_t = clock();
	int iter_count = 0; int n_color_ = v_same_color.size();
	amips_s = 2.0;
	while (iter_count < max_iter_num)
	{
		for (unsigned i = 0; i < n_color_; ++i)
		{
			int one_color_size = v_same_color[i].size();
#pragma omp parallel for schedule(dynamic)
			for (int j = 0; j < one_color_size; ++j)
			{
				int id = omp_get_thread_num();
				AMIPS_one_V(v_same_color[i][j], id);
			}
		}
		++iter_count;
		if (iter_count == max_iter_num / 2) amips_s = 2.0;
	}
	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	//printf("AMIPS: %f s\n", t);

	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		int v_id = v_it.handle().idx();
		OpenMesh::Vec3d p(new_p_x[v_id], new_p_y[v_id], 0);
		mesh_->data(v_it).set_New_Pos(p);
	}
}

void parameterization_interface::Untangling_one_V(int i, int omp_id)
{
	double p0_x = new_p_x[i]; double p0_y = new_p_y[i];
	double gx = 0.0; double gy = 0.0;
	double local_e = 0.0; double min_r = 1e30;
	int vv_size = vertex_v1[i].size();
	std::vector<double>& fh_area_vec = face_area[i]; std::vector<double>& inv_face_area_vec = inv_face_area[i];
	std::vector<double>& omega0_vec = omega0[i]; std::vector<double>& omega1_vec = omega1[i]; std::vector<double>& omega2_vec = omega2[i];
	std::vector<int>& v_v1 = vertex_v1[i]; std::vector<int>& v_v2 = vertex_v2[i];
	std::vector<double>& l0_n_vec = l0_n_vec_omp[omp_id];
	std::vector<double>& p1_vec_x = p1_vec_x_omp[omp_id]; std::vector<double>& p1_vec_y = p1_vec_y_omp[omp_id];
	std::vector<double>& p2_vec_x = p2_vec_x_omp[omp_id]; std::vector<double>& p2_vec_y = p2_vec_y_omp[omp_id];
	std::vector<double>& exp_vec = exp_vec_omp[omp_id]; bool negative_tri = false;
	for (unsigned j = 0; j < vv_size; ++j)
	{
		int vv1 = v_v1[j]; int vv2 = v_v2[j];
		double p1_x = new_p_x[vv1]; double p1_y = new_p_y[vv1];
		double p2_x = new_p_x[vv2]; double p2_y = new_p_y[vv2];
		p1_vec_x[j] = p1_x; p1_vec_y[j] = p1_y;
		p2_vec_x[j] = p2_x; p2_vec_y[j] = p2_y;
		double omega0_ = omega0_vec[j]; double omega1_ = omega1_vec[j]; double omega2_ = omega2_vec[j];

		double l1x = p0_x - p2_x; double l1y = p0_y - p2_y; double l1_n = l1x*l1x + l1y*l1y;
		double l2x = p1_x - p0_x; double l2y = p1_y - p0_y; double l2_n = l2x*l2x + l2y*l2y;
		double l0x = p2_x - p1_x; double l0y = p2_y - p1_y; double l0_n = l0x*l0x + l0y*l0y;
		double a = (-l2x * l1y + l2y*l1x); //double ia = 0;
		/*if (a > 0)
		{
			ia = 1.0/a;
		}
		else
		{
			//ia = 2.0*(std::sqrt(a*a + un_eps) - a) / un_eps;
			ia = 2.0*(std::sqrt(a*inv_un_eps*a*inv_un_eps + inv_un_eps) - a* inv_un_eps);
		}*/
		double ia = 2.0 / (a + std::sqrt(a*a + un_eps));
		//double ia = 2.0*(std::sqrt(a*a + un_eps) - a) / un_eps;

		if (a < 0)
		{
			negative_tri = true;
		}

		l0_n_vec[j] = omega0_*l0_n;
		double fa = fh_area_vec[j]; double ifa = inv_face_area_vec[j];
		double mips_e = (l0_n_vec[j] + omega1_*l1_n + omega2_*l2_n) * ia;
		double area_e = (fa * ia + a*ifa);

		double e = mips_e*(1.0 - c_v_ratio) + area_e*c_v_ratio;
		local_e += e;

		//double coef_a = 1;
		/*if (a > 0)
		{
			//coef_a = -2.0*(1.0 + a / std::sqrt(a*a + un_eps)) / ( (a + std::sqrt(a*a + un_eps))*(a + std::sqrt(a*a + un_eps)) );
			coef_a = -1.0 / (a*a);
		}
		else
		{
			coef_a = 2.0*(inv_un_eps*a*inv_un_eps/ std::sqrt(a*inv_un_eps*a*inv_un_eps + inv_un_eps) - inv_un_eps);
		}*/
		double coef_a = 2.0*(a / std::sqrt(a*a + un_eps) - 1.0) / un_eps;
		//double coef_a = -2.0*(1.0 + a / std::sqrt(a*a + un_eps)) / ((a + std::sqrt(a*a + un_eps))*(a + std::sqrt(a*a + un_eps)));

		double mips_gx = 2.0*(omega1_*l1x - omega2_*l2x)*ia - (l0_n_vec[j] + omega1_*l1_n + omega2_*l2_n) *l0y*coef_a;
		double mips_gy = 2.0*(omega1_*l1y - omega2_*l2y)*ia + (l0_n_vec[j] + omega1_*l1_n + omega2_*l2_n) *l0x*coef_a;

		double area_gx = -l0y*ifa - fa*l0y*coef_a;
		double area_gy = l0x*ifa + fa*l0x*coef_a;

		gx += (mips_gx*(1.0 - c_v_ratio) + area_gx*c_v_ratio);
		gy += (mips_gy*(1.0 - c_v_ratio) + area_gy*c_v_ratio);

		if (l2_n < min_r) { min_r = l2_n; }
	}

	if (negative_tri)
	{
		min_r = std::sqrt(min_r) * 2.5;
	}

	//min_r = std::sqrt(min_r) * vertex_radius_ratio[i];
	double xd = -gx; double yd = -gy;
	if (std::abs(xd) > std::abs(yd) && std::abs(xd) > 1e10)
	{
		yd = yd * min_r / std::abs(xd);
		xd = xd * min_r / std::abs(xd);
	}
	else if (std::abs(yd) > std::abs(xd) && std::abs(yd) > 1e10)
	{
		xd = xd * min_r / std::abs(yd);
		yd = yd * min_r / std::abs(yd);
	}

	double d_r = sqrt(xd*xd + yd*yd);
	if (d_r > min_r)
	{
		xd = xd * min_r / d_r;
		yd = yd * min_r / d_r;
		d_r = min_r;
	}
	double npx = p0_x + xd; double npy = p0_y + yd;
	double new_e = 0.0;
	bool energy_d = check_energy_untangling(vv_size, local_e, new_e, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec);
	if (!energy_d)
	{
		while (!energy_d)
		{
			xd *= 0.2; yd *= 0.2; d_r *= 0.2;
			if (d_r < 1e-8)
			{
				npx = p0_x; npy = p0_y;
				break;
			}
			npx = p0_x + xd; npy = p0_y + yd;
			energy_d = check_energy_untangling(vv_size, local_e, new_e, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec);
		}

		if (energy_d)
		{
			if (negative_tri && new_e < 0.001*local_e)
			{

			}
			else
			{
				energy_d = false;
				xd *= 5; yd *= 5; d_r *= 5;
				while (!energy_d)
				{
					xd *= 0.5; yd *= 0.5; d_r *= 0.5;
					if (d_r < 1e-8)
					{
						npx = p0_x; npy = p0_y;
						break;
					}
					npx = p0_x + xd; npy = p0_y + yd;
					energy_d = check_energy_untangling(vv_size, local_e, new_e, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec);
				}

				if (energy_d)
				{
					if (negative_tri && new_e < 0.001*local_e)
					{

					}
					else
					{
						energy_d = false;
						xd *= 2; yd *= 2; d_r *= 2;
						while (!energy_d)
						{
							xd *= 0.75; yd *= 0.75; d_r *= 0.75;
							if (d_r < 1e-8)
							{
								npx = p0_x; npy = p0_y;
								break;
							}
							npx = p0_x + xd; npy = p0_y + yd;
							energy_d = check_energy_untangling(vv_size, local_e, new_e, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec);
						}

						if (energy_d)
						{
							if (negative_tri && new_e < 0.001*local_e)
							{

							}
							else
							{
								energy_d = false;
								xd /= 0.75; yd /= 0.75; d_r /= 0.75;
								while (!energy_d)
								{
									xd *= 0.945; yd *= 0.945; d_r *= 0.945;
									if (d_r < 1e-8)
									{
										npx = p0_x; npy = p0_y;
										break;
									}
									npx = p0_x + xd; npy = p0_y + yd;
									energy_d = check_energy_untangling(vv_size, local_e, new_e, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec);
								}
							}
						}
					}
				}
			}
		}
	}

	new_p_x[i] = npx; new_p_y[i] = npy;

	min_area[i] = find_negative_area(vv_size, npx, npy, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y);
}

double parameterization_interface::find_negative_area(const int& vv_size, const double& npx, const double& npy,
	const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
	const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y)
{
	double l1x, l1y, l2x, l2y, a; double min_a = 0;
	for (unsigned i = 0; i < vv_size; ++i)
	{
		l1x = npx - p2_vec_x[i];
		l1y = npy - p2_vec_y[i];
		l2x = p1_vec_x[i] - npx;
		l2y = p1_vec_y[i] - npy;
		a = (-l2x * l1y + l2y*l1x);
		if (a < min_a)
		{
			min_a = a;
		}
	}
	return min_a;
}

//false : energy increase, true : energy decrease
bool parameterization_interface::check_energy_untangling(const int& vv_size,
	const double& old_e, double& new_e,
	const double& npx, const double& npy,
	const std::vector<double>& fh_area_vec, const std::vector<double>& inv_fh_area_vec,
	const std::vector<double>& l0_n_vec,
	const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
	const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y,
	const std::vector<double>& omega1_vec, const std::vector<double>& omega2_vec)
{
	new_e = 0.0;
	double l1x, l1y, l2x, l2y, a, ia;
	double l1_n, l2_n; double mips_e, area_e, k;
	for (unsigned i = 0; i < vv_size; ++i)
	{
		l1x = npx - p2_vec_x[i];
		l1y = npy - p2_vec_y[i];
		l2x = p1_vec_x[i] - npx;
		l2y = p1_vec_y[i] - npy;
		a = (-l2x * l1y + l2y*l1x);
		/*if (a > 0)
		{
			//ia = 2.0 / (a + std::sqrt(a*a + un_eps));
			ia = 1.0 / a;
		}
		else
		{
			//ia = 2.0*(std::sqrt(a*a + un_eps) - a) / un_eps;
			ia = 2.0*(std::sqrt(a*inv_un_eps*a*inv_un_eps + inv_un_eps) - a* inv_un_eps);
		}*/
		ia = 2.0 / (a + std::sqrt(a*a + un_eps));
		l1_n = l1x*l1x + l1y*l1y;
		l2_n = l2x*l2x + l2y*l2y;

		mips_e = (l0_n_vec[i] + omega1_vec[i] * l1_n + omega2_vec[i] * l2_n)*ia;
		area_e = fh_area_vec[i] * ia + a * inv_fh_area_vec[i];
		k = mips_e*(1 - c_v_ratio) + area_e*c_v_ratio;
		new_e += k;
		if (new_e > old_e) return false;
	}
	return true;
}

#include "fmath.hpp"
void parameterization_interface::AMIPS_one_V(int i, int omp_id)
{
	std::vector<double>& l0_n_vec = l0_n_vec_omp[omp_id];
	std::vector<double>& p1_vec_x = p1_vec_x_omp[omp_id]; std::vector<double>& p1_vec_y = p1_vec_y_omp[omp_id];
	std::vector<double>& p2_vec_x = p2_vec_x_omp[omp_id]; std::vector<double>& p2_vec_y = p2_vec_y_omp[omp_id];
	std::vector<double>& exp_vec = exp_vec_omp[omp_id];
	double alpha = c_v_ratio* amips_s; double beta = amips_s - alpha;
	double p0_x = new_p_x[i]; double p0_y = new_p_y[i];
	double gx = 0.0; double gy = 0.0;
	double local_EXP_MIPS_e = 0.0; double min_r = 1e30;
	int vv_size = vertex_v1[i].size();
	std::vector<double>& fh_area_vec = face_area[i]; std::vector<double>& inv_face_area_vec = inv_face_area[i];
	std::vector<double>& omega0_vec = omega0[i]; std::vector<double>& omega1_vec = omega1[i]; std::vector<double>& omega2_vec = omega2[i];
	std::vector<int>& v_v1 = vertex_v1[i]; std::vector<int>& v_v2 = vertex_v2[i];
	for (unsigned j = 0; j < vv_size; ++j)
	{
		int vv1 = v_v1[j]; int vv2 = v_v2[j];
		double p1_x = new_p_x[vv1]; double p1_y = new_p_y[vv1];
		double p2_x = new_p_x[vv2]; double p2_y = new_p_y[vv2];
		p1_vec_x[j] = p1_x; p1_vec_y[j] = p1_y;
		p2_vec_x[j] = p2_x; p2_vec_y[j] = p2_y;
		double omega0_ = omega0_vec[j]; double omega1_ = omega1_vec[j]; double omega2_ = omega2_vec[j];

		double l1x = p0_x - p2_x; double l1y = p0_y - p2_y; double l1_n = l1x*l1x + l1y*l1y;
		double l2x = p1_x - p0_x; double l2y = p1_y - p0_y; double l2_n = l2x*l2x + l2y*l2y;
		double l0x = p2_x - p1_x; double l0y = p2_y - p1_y; double l0_n = l0x*l0x + l0y*l0y;
		double a = (-l2x * l1y + l2y*l1x); double ia = 1.0 / a;

		l0_n_vec[j] = omega0_*l0_n;
		double fa = fh_area_vec[j]; double ifa = inv_face_area_vec[j];
		double mips_e = (l0_n_vec[j] + omega1_*l1_n + omega2_*l2_n) * ia;
		double area_e = (fa * ia + a*ifa);

		double k = 0.5*(beta*mips_e + alpha * area_e);
		if (k > 60)
		{
			k = 60;
		}
		double e = std::exp(k);
		//double e = fmath::expd(k);
		local_EXP_MIPS_e += e;
		double mips_gx = (2.0*(omega1_*l1x - omega2_*l2x) + mips_e*l0y)*ia;
		double mips_gy = (2.0*(omega1_*l1y - omega2_*l2y) - mips_e*l0x)*ia;

		double area_gx = -(a*a - fa*fa)*l0y * (ifa*ia*ia);
		double area_gy = (a*a - fa*fa)*l0x * (ifa*ia*ia);

		gx += e *(beta*mips_gx + alpha * area_gx)*0.5;
		gy += e *(beta*mips_gy + alpha * area_gy)*0.5;

		if (l2_n < min_r) { min_r = l2_n; }
	}

	min_r = std::sqrt(min_r) * vertex_radius_ratio[i];

	double xd = -gx; double yd = -gy;
	double d_r = sqrt(xd*xd + yd*yd);
	if (d_r > min_r)
	{
		xd = xd * min_r / d_r;
		yd = yd * min_r / d_r;
		d_r = min_r;
	}
	double npx = p0_x + xd; double npy = p0_y + yd;
	while (!local_check_negative_area(vv_size, npx, npy, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y))
	{
		xd *= 0.8; yd *= 0.8; d_r *= 0.8;
		if (d_r < 1e-8)
		{
			npx = p0_x; npy = p0_y;
			break;
		}
		npx = p0_x + xd; npy = p0_y + yd;
	}

	double new_e = 0.0;

	double* p_exp_vec = exp_vec.data();
	bool energy_d = check_energy_AMIPS(vv_size, local_EXP_MIPS_e, new_e, alpha, beta, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec, p_exp_vec);
	if (!energy_d)
	{
		while (!energy_d)
		{
			xd *= 0.2; yd *= 0.2; d_r *= 0.2;
			if (d_r < 1e-8)
			{
				npx = p0_x; npy = p0_y;
				break;
			}
			npx = p0_x + xd; npy = p0_y + yd;
			energy_d = check_energy_AMIPS(vv_size, local_EXP_MIPS_e, new_e, alpha, beta, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec, p_exp_vec);
		}

		if (energy_d)
		{
			energy_d = false;
			xd *= 5; yd *= 5; d_r *= 5;
			while (!energy_d)
			{
				xd *= 0.5; yd *= 0.5; d_r *= 0.5;
				if (d_r < 1e-8)
				{
					npx = p0_x; npy = p0_y;
					break;
				}
				npx = p0_x + xd; npy = p0_y + yd;
				energy_d = check_energy_AMIPS(vv_size, local_EXP_MIPS_e, new_e, alpha, beta, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec, p_exp_vec);
			}

			if (energy_d)
			{
				energy_d = false;
				xd *= 2; yd *= 2; d_r *= 2;
				while (!energy_d)
				{
					xd *= 0.75; yd *= 0.75; d_r *= 0.75;
					if (d_r < 1e-8)
					{
						npx = p0_x; npy = p0_y;
						break;
					}
					npx = p0_x + xd; npy = p0_y + yd;
					energy_d = check_energy_AMIPS(vv_size, local_EXP_MIPS_e, new_e, alpha, beta, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec, p_exp_vec);
				}

				if (energy_d)
				{
					energy_d = false;
					xd /= 0.75; yd /= 0.75; d_r /= 0.75;
					while (!energy_d)
					{
						xd *= 0.945; yd *= 0.945; d_r *= 0.945;
						if (d_r < 1e-8)
						{
							npx = p0_x; npy = p0_y;
							break;
						}
						npx = p0_x + xd; npy = p0_y + yd;
						energy_d = check_energy_AMIPS(vv_size, local_EXP_MIPS_e, new_e, alpha, beta, npx, npy, fh_area_vec, inv_face_area_vec, l0_n_vec, p1_vec_x, p1_vec_y, p2_vec_x, p2_vec_y, omega1_vec, omega2_vec, p_exp_vec);
					}
				}
			}
		}
	}

	new_p_x[i] = npx; new_p_y[i] = npy;

	if (energy_d)
	{
		if (vertex_radius_ratio[i] > 0.07)
		{
			if (std::abs(local_EXP_MIPS_e - new_e) < 1e-2 * local_EXP_MIPS_e)
			{
				vertex_radius_ratio[i] = 0.5;
			}

			if (std::abs(local_EXP_MIPS_e - new_e) < 1e-4 * local_EXP_MIPS_e)
			{
				vertex_radius_ratio[i] = 0.2;
			}

			if (std::abs(local_EXP_MIPS_e - new_e) < 1e-5 * local_EXP_MIPS_e)
			{
				vertex_radius_ratio[i] = 0.1;
			}

			if (std::abs(local_EXP_MIPS_e - new_e) < 1e-6 * local_EXP_MIPS_e)
			{
				vertex_radius_ratio[i] = 0.06;
			}
		}
	}
}

bool parameterization_interface::local_check_negative_area(const int& vv_size,
	const double& npx, const double& npy,
	const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
	const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y)
{
	double l1x, l1y, l2x, l2y, a;
	for (unsigned i = 0; i < vv_size; ++i)
	{
		l1x = npx - p2_vec_x[i];
		l1y = npy - p2_vec_y[i];
		l2x = p1_vec_x[i] - npx;
		l2y = p1_vec_y[i] - npy;
		a = (-l2x * l1y + l2y*l1x);
		if (a < 1e-16)
		{
			return false;
		}
	}
	return true;
}

bool parameterization_interface::check_energy_AMIPS(const int& vv_size,
	const double& old_e, double& new_e,
	const double& alpha, const double& beta,
	const double& npx, const double& npy,
	const std::vector<double>& fh_area_vec, const std::vector<double>& inv_fh_area_vec,
	const std::vector<double>& l0_n_vec,
	const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
	const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y,
	const std::vector<double>& omega1_vec, const std::vector<double>& omega2_vec,
	double* exp_vec)
{
	double l1x, l1y, l2x, l2y, a, ia;
	double l1_n, l2_n; double mips_e, area_e, k;
	for (unsigned i = 0; i < vv_size; ++i)
	{
		l1x = npx - p2_vec_x[i];
		l1y = npy - p2_vec_y[i];
		l2x = p1_vec_x[i] - npx;
		l2y = p1_vec_y[i] - npy;
		a = (-l2x * l1y + l2y*l1x);
		ia = 1.0 / a;
		l1_n = l1x*l1x + l1y*l1y;
		l2_n = l2x*l2x + l2y*l2y;

		mips_e = (l0_n_vec[i] + omega1_vec[i] * l1_n + omega2_vec[i] * l2_n);
		area_e = (fh_area_vec[i] + a*a * inv_fh_area_vec[i]);
		k = (beta * mips_e + alpha * area_e)*0.5*ia;

		if (k > 60) { k = 60; }
		exp_vec[i] = k;
	}
	//fmath::expd_v(exp_vec, vv_size);
	new_e = 0.0;
	for (int i = 0; i < vv_size; ++i)
	{
		exp_vec[i] = std::exp(exp_vec[i]);
		new_e += exp_vec[i];
	}
	if (new_e > old_e) return false;

	return true;
}
