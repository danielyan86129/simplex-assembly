#ifndef ASSEMBLE_SIMPLEX_H
#define ASSEMBLE_SIMPLEX_H

#include "mesh_definition.h"
#include <Eigen/Sparse>
#include <Eigen/Dense>

class assemble_triangle_interface
{
public:
	assemble_triangle_interface();
	~assemble_triangle_interface();
	void reset();
	/*
	filename_uv: initial mapping result
	filename_de: handles setting
	filename_frame: initial frames
	filename_rotation: rotation on each halfedge
	*/
	//==============================================================================================
	//planar deformation, conformal
	void optimize_affine_transformation(Mesh* mesh_, const char* filename_uv, const char* filename_de, int max_iter_ = 50);
	//==============================================================================================
	//planar parameterization, isometric
	void optimize_integrable_vector_field(Mesh* mesh_, const char* filename_uv, int max_iter_ = 100);
	void optimize_integrable_vector_field(Mesh* mesh_, int max_iter_ = 100); //use default iniital mapping
	//==============================================================================================
	//global seamless parameterization, isometric, Em = || A - A_0 ||_F^2
	void optimize_ivf_gsp(Mesh* mesh_, const char* filename_frame, const char* filename_rotation, int max_iter_ = 50);
	//==============================================================================================
	//planar parameterization(speedup), conformal, Em = LSCM
	void optimize_integrable_vector_field_speedup_seq(Mesh* mesh_, const char* filename_uv, int max_iter_ = 50);
	//==============================================================================================

	void load_initial_uv(Mesh* mesh_, const char* filename);
	void save_result_uv(Mesh* mesh_, const char* filename);
	void save_gsp_UV(Mesh* mesh_, const char* filename);
	void save_halfedge_uv(Mesh* mesh_, const char* filename);

private:
	
	void load_handles_setting(Mesh* mesh_, const char* filename);
	void load_frame(const char* filename, int nf);
	void load_rotation(const char* filename, int nhe);
	void save_affine_transformation(Mesh* mesh_, const char* filename);

	std::vector<OpenMesh::Vec3d> U_f;
	std::vector<OpenMesh::Vec3d> V_f;

	std::vector<OpenMesh::Vec3d> e1_f;
	std::vector<OpenMesh::Vec3d> e2_f;
	std::vector<OpenMesh::Vec3d> b_f;
	std::vector<OpenMesh::Vec4d> A_f; //a1, a2, b1, b2

	Eigen::SparseMatrix<double> CTC;
	Eigen::SparseMatrix<double> CT;
	Eigen::Matrix<double, Eigen::Dynamic, 1> CB;
	double CBTCB;

	Eigen::SparseMatrix<double> hessian;
	Eigen::Matrix<double, Eigen::Dynamic, 1> gradient;
	Eigen::Matrix<double, Eigen::Dynamic, 1> prevSolution;
	Eigen::Matrix<double, Eigen::Dynamic, 1> solution;
	Eigen::Matrix<double, Eigen::Dynamic, 1> step;
	Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, Eigen::Upper> lltSolver;
	double lambda; double min_lambda; double max_lambda;
	double ivf_alpha; double min_ivf_alpha; double max_ivf_alpha;
	double step_size; double min_step_size; double max_step_size;
	double current_ivf_energy; double current_em_energy;
	double current_dis_energy; double pre_current_dis_energy;
	int energy_method; double amips_s; int max_iter; int energy_type;
	std::vector<int> flipped_tri; std::vector<int> is_flipped; int flip_count;
	std::vector<double> src_face_area; double mu; double bound_k; bool is_bounded;

	void initialize_one_matrix(Eigen::Matrix2d& A, bool is_conformal);
	void update_ivf_alpha();

	double compute_energy_derivative_modified_hessian(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal = false);
	double compute_only_energy(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal = false);
	void initilize_vector_field(Mesh* mesh_, bool is_conformal = false); bool prepare_ok;
	void assign_new_vector_field(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x);
	void reconstruct_UV(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x);
	bool optimize_IVF(Mesh* mesh_,bool is_conformal = false);
	bool optimize_IVF2(Mesh* mesh_, bool is_conformal = false);

	void initilize_affine_trans_no_translation(Mesh* mesh_, bool is_conformal = false);
	double compute_energy_derivative_AT_no_translation(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal = false, bool update_hessian = true);
	double compute_only_energy_AT_no_translation(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal = false);
	void optimize_AT_no_translation(Mesh* mesh_, bool is_conformal = false); //affine transformation
	void reconstruct_UV_no_translation(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x);
	std::vector<int> face_translation; std::vector<int> edge_translation;

	//global seamless para(gsp)
	std::vector<double> edge_rotation;
	std::vector<int> edge_cut_flag;
	std::vector<double> face_uv;
	bool initilize_vector_field_gsp(Mesh* mesh_);
	void reconstruct_UV_gsp(Mesh* mesh_);
	void compute_distortion_gsp(Mesh* mesh_);

	//sequential speedup
	void initilize_vector_field_UV(Mesh* Mesh_);
	void select_one_block(Mesh* mesh_, int seed_face, int k_ring); //k_ring >= 1
	bool check_one_block(); //k_ring >= 1
	std::vector<int> block_tri; int block_num; std::vector<int> map_tri;
	bool initilize_vector_field_block(Mesh* mesh_);
	void assign_new_vector_field_block(Mesh* mesh_);
	void check_flip_by_A();
	void reconstruct_UV(Mesh* mesh_);
};

#endif