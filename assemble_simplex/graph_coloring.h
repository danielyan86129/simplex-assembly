#include <iostream>
#include <string>
#include <fstream>
#include "time.h"
#include "mesh_definition.h"

#include <boost/graph/adjacency_list.hpp>
#include <boost/property_map/shared_array_property_map.hpp> //this should be included from smallest_last_ordering.hpp
#include <boost/graph/smallest_last_ordering.hpp>
#include <boost/graph/sequential_vertex_coloring.hpp>

using namespace boost;

void graph_coloring(int N, std::vector<OpenMesh::Vec2i>& graph_color_edge, std::vector< std::vector<int> >& v_same_color);
