#include "assemble_simplex.h"
#include <time.h>
#include "omp.h"

assemble_triangle_interface::assemble_triangle_interface()
{
	prepare_ok = false;
	reset();
}

assemble_triangle_interface::~assemble_triangle_interface()
{

}

void assemble_triangle_interface::reset()
{
	prepare_ok = false;
	U_f.clear(); V_f.clear();
	e1_f.clear(); e2_f.clear(); A_f.clear();

	lambda = 1e-1; min_lambda = 1e-5; max_lambda = 1e16;
	ivf_alpha = 1e5; min_ivf_alpha = 1e3; max_ivf_alpha = 1e16;
	step_size = 1.0; min_step_size = 1.0e-16; max_step_size = 1.0;
	energy_method = 0;//MIPS
	amips_s = 2.0; max_iter = 1000; energy_type = 0; mu = 1e3;
	is_bounded = false; bound_k = 1;

	face_uv.clear();
	edge_rotation.clear(); edge_cut_flag.clear();
}

void assemble_triangle_interface::load_initial_uv(Mesh* mesh_, const char* filename)
{
	FILE* f_uv = fopen(filename, "r");
	int n_uv = mesh_->n_vertices(); char buf[4096];
	OpenMesh::Vec3d np(0, 0, 0);
	char u[128]; char v[128]; char w[128]; int v_count = 0;
	while (!feof(f_uv))
	{
		fgets(buf, 4096, f_uv);
		if (v_count < n_uv)
		{
			sscanf(buf, "%s %s %s", u, v, w);
			np[0] = atof(u); np[1] = atof(v); np[2] = 0.0;
			Mesh::VertexHandle vh = mesh_->vertex_handle(v_count);
			mesh_->data(vh).set_New_Pos(np);
			++v_count;
		}
	}
	fclose(f_uv);
}

void assemble_triangle_interface::save_result_uv(Mesh* mesh_, const char* filename)
{
	double min_con = 1e30; double min_iso = 1e30; double min_vol = 1e30;
	double max_con = 0; double max_iso = 0; double max_vol = 0;
	double avg_con = 0; double avg_iso = 0; double avg_vol = 0;
	int flipped_count = 0;
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0);
		n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);

		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix
		Eigen::Matrix2d XY, UV, A;
		XY << l01, x2, 0, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse();
		if (A.determinant() < 0)
		{
			++flipped_count;
		}
		else
		{
			Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
			Eigen::Vector2d s_v = svd.singularValues();
			double temp_error = s_v(0) / s_v(1);
			avg_con += temp_error;
			if (temp_error < min_con) min_con = temp_error;
			if (temp_error > max_con) max_con = temp_error;

			temp_error = s_v(0) > 1.0 / s_v(1) ? s_v(0) : 1.0 / s_v(1);
			avg_iso += temp_error;
			if (temp_error < min_iso) min_iso = temp_error;
			if (temp_error > max_iso) max_iso = temp_error;

			double dJ = s_v(0)*s_v(1);
			temp_error = dJ > 1.0 / dJ ? dJ : 1.0 / dJ;
			avg_vol += temp_error;
			if (temp_error < min_vol) min_vol = temp_error;
			if (temp_error > max_vol) max_vol = temp_error;
		}
	}
	avg_iso /= mesh_->n_faces(); avg_con /= mesh_->n_faces(); avg_vol /= mesh_->n_faces();
	printf("--------------------------------------------------------------\n");
	printf("Flipped Triangles: %d; Max/Min/Avg\n", flipped_count);
	printf("Isometric: %f/%f/%f\n", max_iso, min_iso, avg_iso);
	printf("Conformal: %f/%f/%f\n", max_con, min_con, avg_con);
	printf("Volumetric: %f/%f/%f\n", max_vol, min_vol, avg_vol);

	//FILE* f_uv = fopen(filename, "w");
	//for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	//{
	//	OpenMesh::Vec3d& np = mesh_->data(v_it).get_New_Pos();
	//	fprintf(f_uv, "%20.19f %20.19f\n", np[0], np[1]); //just 2D uv
	//}
	//fclose(f_uv);

	FILE* f_uv = fopen(filename, "w");
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		OpenMesh::Vec3d& np = mesh_->data(v_it).get_New_Pos();
		fprintf(f_uv, "v %20.19f %20.19f 0.0\n", np[0], np[1]); //just 2D uv
	}
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		fprintf(f_uv, "f");
		Mesh::FaceVertexIter fv_it = mesh_->fv_iter(f_it);
		fprintf(f_uv, " %d", fv_it->idx() + 1);
		++fv_it;
		fprintf(f_uv, " %d", fv_it->idx() + 1);
		++fv_it;
		fprintf(f_uv, " %d\n", fv_it->idx() + 1);
	}
	fclose(f_uv);
}

void assemble_triangle_interface::optimize_integrable_vector_field(Mesh* mesh_, const char* filename_uv, int max_iter_)
{
	load_initial_uv(mesh_, filename_uv);

	energy_method = 1; amips_s = 1; max_iter = max_iter_; energy_type = 0;

	initilize_vector_field(mesh_, false);
	
	/*compute_only_energy(mesh_, solution);
	update_ivf_alpha();*/

	long start_t = clock();

	optimize_IVF(mesh_, false);

	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	printf("Optimization Time: %f s\n", t);

	assign_new_vector_field(mesh_, solution);
	reconstruct_UV(mesh_, solution);

	std::string uv(filename_uv);
	uv.append("_result.uv");
	save_result_uv(mesh_, uv.c_str());
}

void assemble_triangle_interface::optimize_integrable_vector_field(Mesh* mesh_, int max_iter_)
{
	energy_method = 1; amips_s = 1; max_iter = max_iter_; energy_type = 0;
	bool is_conformal = false;

	initilize_vector_field(mesh_, is_conformal);
	long start_t = clock();
	
	optimize_IVF(mesh_, is_conformal);
	//optimize_IVF2(mesh_, is_conformal);
	assign_new_vector_field(mesh_, solution);
	reconstruct_UV(mesh_, solution);

	/*while (flip_count > 0)
	{
		initilize_vector_field(mesh_, is_conformal);
		optimize_IVF(mesh_, is_conformal);
		assign_new_vector_field(mesh_, solution);
		reconstruct_UV(mesh_, solution);
		if (flip_count == 0) break;
	}*/
	
	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	printf("Optimization Time: %f s\n", t);
}


void assemble_triangle_interface::initilize_vector_field(Mesh* mesh_, bool is_conformal)
{
	//if (prepare_ok) return;

	unsigned nf = mesh_->n_faces(); 
	U_f.resize(nf); V_f.resize(nf); e1_f.resize(nf); e2_f.resize(nf); A_f.resize(nf);
	std::vector<Eigen::Triplet<double> > f_triplets; int var_count = 4 * nf;
	solution.resize(var_count); //flip_count = 0
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		/*Mesh::FaceVertexIter fv_it = mesh_->fv_iter(f_it);
		OpenMesh::Vec3d p0 = mesh_->point(fv_it); Mesh::VertexHandle v0 = fv_it.handle();
		++fv_it; OpenMesh::Vec3d p1 = mesh_->point(fv_it); Mesh::VertexHandle v1 = fv_it.handle();
		++fv_it; OpenMesh::Vec3d p2 = mesh_->point(fv_it); Mesh::VertexHandle v2 = fv_it.handle();*/

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0); n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);
		
		int face_id = f_it.handle().idx();
		e1_f[face_id] = e1; e2_f[face_id] = e2;

		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix
		double A_00 = (q1[0] - q0[0]) / l01; double A_10 = -(q1[0] - q0[0])*x2 / (y2*l01)+ (q2[0] - q0[0]) / y2;
		double A_01 = (q1[1] - q0[1]) / l01; double A_11 = -(q1[1] - q0[1])*x2 / (y2*l01)+ (q2[1] - q0[1]) / y2;

		Eigen::Matrix2d A; A << A_00, A_01, A_10, A_11;
		initialize_one_matrix(A, is_conformal);

		A_f[face_id] = OpenMesh::Vec4d(A_00, A_10, A_01, A_11);
		solution(4 * face_id + 0) = A(0,0); solution(4 * face_id + 1) = A(0,1); 
		solution(4 * face_id + 2) = A(1,0); solution(4 * face_id + 3) = A(1,1);

		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				f_triplets.push_back(Eigen::Triplet<double>(4*face_id + i, 4*face_id + j, 1.0));
			}
		}
	}
	
	//edge_face_index.clear(); edge_base_dot.clear();
	std::vector<Eigen::Triplet<double> > c_triplets; std::vector<Eigen::Triplet<double> > triplets;
	int constraint_count = 0; std::vector<double> b;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		if (mesh_->is_boundary(e_it)) continue;

		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		const OpenMesh::Vec3d& p0 = mesh_->point(mesh_->to_vertex_handle(heh1));
		const OpenMesh::Vec3d& p1 = mesh_->point(mesh_->to_vertex_handle(heh0));
		OpenMesh::Vec3d e = (p1 - p0).normalize();

		Mesh::FaceHandle fh0 = mesh_->face_handle(heh0); int f0 = fh0.idx();
		Mesh::FaceHandle fh1 = mesh_->face_handle(heh1); int f1 = fh1.idx();

		//one edge, two equation
		double ee1_0 = OpenMesh::dot(e, e1_f[f0]); double ee2_0 = OpenMesh::dot(e, e2_f[f0]);
		double ee1_1 = OpenMesh::dot(e, e1_f[f1]); double ee2_1 = OpenMesh::dot(e, e2_f[f1]);

		c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, 1.0));
		c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, 1.0));
		c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, 1.0));
		c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, 1.0));
		c_triplets.push_back(Eigen::Triplet<double>(constraint_count+1, 4 * f0 + 2, 1.0));
		c_triplets.push_back(Eigen::Triplet<double>(constraint_count+1, 4 * f0 + 3, 1.0));
		c_triplets.push_back(Eigen::Triplet<double>(constraint_count+1, 4 * f1 + 2, 1.0));
		c_triplets.push_back(Eigen::Triplet<double>(constraint_count+1, 4 * f1 + 3, 1.0));

		triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, ee1_0));
		triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, ee2_0));
		triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, -ee1_1));
		triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, -ee2_1));
		triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, ee1_0));
		triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, ee2_0));
		triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, -ee1_1));
		triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, -ee2_1));

		constraint_count += 2; b.push_back(0.0); b.push_back(0.0);
	}

	Eigen::SparseMatrix<double> C; C.resize(constraint_count, var_count);
	C.setFromTriplets(c_triplets.begin(), c_triplets.end());
	CT = C.transpose();
	CTC.resize(var_count, var_count);
	CTC = CT*C;
	Eigen::SparseMatrix<double> H;
	H.resize(var_count, var_count);
	H.setFromTriplets(f_triplets.begin(), f_triplets.end());

	hessian.resize(var_count, var_count);
	hessian = H + CTC;
	lltSolver.analyzePattern(hessian);

	C *= 0.0;
	C.setFromTriplets(triplets.begin(), triplets.end());
	CT = C.transpose();
	CTC = CT*C;

	CB.resize(constraint_count);
	for (int i = 0; i < constraint_count; ++i)
	{
		CB(i) = b[i];
	}
	CBTCB = CB.transpose() * CB;

	gradient.resize(var_count); prevSolution.resize(var_count); step.resize(var_count);

	prepare_ok = true;
}

double assemble_triangle_interface::compute_energy_derivative_modified_hessian(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal)
{
	current_ivf_energy = x.transpose()*CTC*x;
	current_ivf_energy -= 2.0*x.transpose()*CT*CB;
	current_ivf_energy = std::abs(current_ivf_energy + CBTCB);

	gradient = 2.0*ivf_alpha*(CTC*x - CT*CB);
	Eigen::Matrix4d H; 
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix4d> es;
	hessian *= 0; current_dis_energy = 0.0; double s = amips_s;
	int nf = x.size() / 4; double distortion_e;
	double g_a1, g_a2, g_b1, g_b2;
	double h_a1_a1, h_a1_a2, h_a1_b1, h_a1_b2, h_a2_a2, h_a2_b1, h_a2_b2, h_b1_b1, h_b1_b2, h_b2_b2;
	current_em_energy = 0.0; mu = 1e3;
	for (int face_id = 0; face_id < nf; ++face_id)
	{
		double a1 = x(4 * face_id + 0); double a2 = x(4 * face_id + 1);
		double b1 = x(4 * face_id + 2); double b2 = x(4 * face_id + 3);
		double det_J = a1*b2 - a2*b1;
		double mips_e = 0.5*(a1*a1 + a2*a2 + b1*b1 + b2*b2) / det_J;
		if (is_conformal)
		{
			distortion_e = mips_e;
		}
		else
		{
			double area_e = 0.5*(det_J + 1.0 / det_J);
			distortion_e = 0.5*(mips_e + area_e);
		}
		//AMIPS
		{
			double k = s*distortion_e;
			if (k > 60) k = 60;
			double exp_e = std::exp(k);
			current_dis_energy += exp_e;

			if (is_conformal)
			{
				double temp_f = a1*a1 + a2*a2 + b1*b1 + b2*b2;
				g_a1 = a1 / det_J - (b2*temp_f) / (2 * det_J*det_J);
				g_a2 = a2 / det_J + (b1*temp_f) / (2 * det_J*det_J);
				g_b1 = b1 / det_J + (a2*temp_f) / (2 * det_J*det_J);
				g_b2 = b2 / det_J - (a1*temp_f) / (2 * det_J*det_J);

				h_a1_a1 = 1 / det_J - (2 * a1*b2) / (det_J*det_J) + (b2*b2 * temp_f) / (det_J*det_J*det_J);
				h_a1_a2 = (a1*b1) / (det_J*det_J) - (a2*b2) / (det_J*det_J) - (b1*b2*temp_f) / (det_J*det_J*det_J);
				h_a1_b1 = (a1*a2) / (det_J*det_J) - (b1*b2) / (det_J*det_J) - (a2*b2*temp_f) / (det_J*det_J*det_J);
				h_a1_b2 = (a1*b2*temp_f) / (det_J*det_J*det_J) - b2*b2 / (det_J*det_J) - temp_f / (2 * (det_J*det_J)) - a1*a1 / (det_J*det_J);

				h_a2_a2 = 1 / det_J + (2 * a2*b1) / (det_J*det_J) + (b1*b1 * temp_f) / (det_J*det_J*det_J);
				h_a2_b1 = a2*a2 / (det_J*det_J) + b1*b1 / (det_J*det_J) + temp_f / (2 * (det_J*det_J)) + (a2*b1*temp_f) / (det_J*det_J*det_J);
				h_a2_b2 = (b1*b2) / (det_J*det_J) - (a1*a2) / (det_J*det_J) - (a1*b1*temp_f) / (det_J*det_J*det_J);

				h_b1_b1 = 1 / det_J + (2 * a2*b1) / (det_J*det_J) + (a2*a2 * temp_f) / (det_J*det_J*det_J);
				h_b1_b2 = (a2*b2) / (det_J*det_J) - (a1*b1) / (det_J*det_J) - (a1*a2*temp_f) / (det_J*det_J*det_J);
				h_b2_b2 = 1 / det_J - (2 * a1*b2) / (det_J*det_J) + (a1*a1 * temp_f) / (det_J*det_J*det_J);
			}
			else
			{
				g_a1 = b2 / 4.0 + a1 / (2.0 * det_J) - b2 / (4.0 * det_J*det_J) - (b2*mips_e) / (2.0*det_J);
				g_a2 = a2 / (2.0 * det_J) - b1 / 4.0 + b1 / (4.0 * det_J*det_J) + (b1*mips_e) / (2.0*det_J);
				g_b1 = a2 / (4.0 * det_J*det_J) - a2 / 4.0 + b1 / (2.0 * det_J) + (a2*mips_e) / (2.0*det_J);
				g_b2 = a1 / 4.0 - a1 / (4.0 * det_J*det_J) + b2 / (2.0 * det_J) - (a1*mips_e) / (2.0*det_J);

				h_a1_a1 = 1.0 / (2.0 * det_J) + b2*b2 / (2.0 * det_J*det_J*det_J) - (a1*b2) / (det_J*det_J) + (b2*b2 * mips_e) / (det_J*det_J);
				h_a1_a2 = (a1*b1) / (2.0 * det_J*det_J) - (a2*b2) / (2.0 * det_J*det_J) - (b1*b2) / (2.0 * det_J*det_J*det_J) - (b1*b2*mips_e) / (det_J*det_J);
				h_a1_b1 = (a1*a2) / (2.0 * det_J*det_J) - (a2*b2) / (2.0 * det_J*det_J*det_J) - (b1*b2) / (2.0 * det_J*det_J) - (a2*b2*mips_e) / (det_J*det_J);
				h_a1_b2 = (a1*b2) / (2.0 * det_J *det_J*det_J) - a1*a1 / (2.0 * det_J*det_J) - b2*b2 / (2.0 * det_J*det_J) - mips_e / (2.0 * det_J) - 1.0 / (4.0 * det_J*det_J) + (a1*b2*mips_e) / (det_J *det_J) + 1.0 / 4.0;

				h_a2_a2 = 1.0 / (2.0 * det_J) + b1 *b1 / (2.0 * det_J*det_J*det_J) + (a2*b1) / (det_J*det_J) + (b1*b1*mips_e) / (det_J*det_J);
				h_a2_b1 = 1.0 / (4.0 * det_J*det_J) + a2*a2 / (2.0 * det_J*det_J) + b1*b1 / (2.0 * det_J*det_J) + mips_e / (2.0 * det_J) + (a2*b1) / (2.0 * det_J *det_J*det_J) + (a2*b1*mips_e) / (det_J *det_J) - 1.0 / 4.0;
				h_a2_b2 = (b1*b2) / (2.0 * det_J*det_J) - (a1*b1) / (2.0 * det_J*det_J*det_J) - (a1*a2) / (2.0 * det_J*det_J) - (a1*b1*mips_e) / (det_J*det_J);

				h_b1_b1 = 1.0 / (2.0 * det_J) + a2*a2 / (2.0 * det_J*det_J*det_J) + (a2*b1) / (det_J*det_J) + (a2*a2 *mips_e) / (det_J*det_J);;
				h_b1_b2 = (a2*b2) / (2.0 * det_J*det_J) - (a1*b1) / (2.0 * det_J*det_J) - (a1*a2) / (2.0 * det_J*det_J*det_J) - (a1*a2*mips_e) / (det_J*det_J);
				h_b2_b2 = 1.0 / (2.0 * det_J) + a1*a1 / (2.0 * det_J*det_J*det_J) - (a1*b2) / (det_J*det_J) + (a1*a1 *mips_e) / (det_J*det_J);
			}

			gradient(4 * face_id + 0) += exp_e*s*g_a1;
			gradient(4 * face_id + 1) += exp_e*s*g_a2;
			gradient(4 * face_id + 2) += exp_e*s*g_b1;
			gradient(4 * face_id + 3) += exp_e*s*g_b2;

			H.coeffRef(0, 0) = exp_e*s*h_a1_a1 + exp_e*s*s*g_a1*g_a1;
			H.coeffRef(0, 1) = exp_e*s*h_a1_a2 + exp_e*s*s*g_a1*g_a2;
			H.coeffRef(0, 2) = exp_e*s*h_a1_b1 + exp_e*s*s*g_a1*g_b1;
			H.coeffRef(0, 3) = exp_e*s*h_a1_b2 + exp_e*s*s*g_a1*g_b2;

			H.coeffRef(1, 1) = exp_e*s*h_a2_a2 + exp_e*s*s*g_a2*g_a2;
			H.coeffRef(1, 2) = exp_e*s*h_a2_b1 + exp_e*s*s*g_a2*g_b1;
			H.coeffRef(1, 3) = exp_e*s*h_a2_b2 + exp_e*s*s*g_a2*g_b2;

			H.coeffRef(2, 2) = exp_e*s*h_b1_b1 + exp_e*s*s*g_b1*g_b1;
			H.coeffRef(2, 3) = exp_e*s*h_b1_b2 + exp_e*s*s*g_b1*g_b2;

			H.coeffRef(3, 3) = exp_e*s*h_b2_b2 + exp_e*s*s*g_b2*g_b2;

			H.coeffRef(1, 0) = H.coeffRef(0, 1);
			H.coeffRef(2, 0) = H.coeffRef(0, 2);
			H.coeffRef(3, 0) = H.coeffRef(0, 3);
			H.coeffRef(2, 1) = H.coeffRef(1, 2);
			H.coeffRef(3, 1) = H.coeffRef(1, 3);
			H.coeffRef(3, 2) = H.coeffRef(2, 3);

			es.compute(H, Eigen::EigenvaluesOnly);
			double min_ev = es.eigenvalues()(0);
			if (min_ev < 1e-16)
			{
				min_ev = std::abs(min_ev) + 1e-16;
			}
			else //positive eigenvalues
			{
				min_ev = 0;
			}

			//min_ev = 0;
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					if (i == j)
					{
						hessian.coeffRef(4 * face_id + i, 4 * face_id + j) = H.coeffRef(i, j) + min_ev;
					}
					else
					{
						hessian.coeffRef(4 * face_id + i, 4 * face_id + j) = H.coeffRef(i, j);
					}
				}
			}
		}

		if (energy_type == 1)//derivation from input field for global seamless parameterization
		{
			double derivation_e = (a1 - 1.0)*(a1 - 1.0) + a2*a2 + b1*b1 + (b2 - 1.0)*(b2 - 1.0);
			current_em_energy += derivation_e;
			gradient(4 * face_id + 0) += mu * 2.0 * (a1 - 1.0);
			gradient(4 * face_id + 1) += mu * 2.0 *a2;
			gradient(4 * face_id + 2) += mu * 2.0 *b1;
			gradient(4 * face_id + 3) += mu * 2.0 * (b2 - 1.0);

			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 0) += 2 * mu;
			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 1) += 2 * mu;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 2) += 2 * mu;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 3) += 2 * mu;
		}
		else if (energy_type == 2) //LSCM
		{
			double lacm_e = ((a1 - b2)*(a1 - b2) + (a2 + b1)*(a2 + b1))*src_face_area[block_tri[face_id]];
			current_em_energy += lacm_e; double g_coef = 2 * mu*src_face_area[block_tri[face_id]];
			gradient(4 * face_id + 0) += g_coef * (a1 - b2);
			gradient(4 * face_id + 1) += g_coef * (a2 + b1);
			gradient(4 * face_id + 2) += g_coef * (a2 + b1);
			gradient(4 * face_id + 3) += g_coef * (b2 - a1);

			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 0) += g_coef;
			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 3) -= g_coef;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 0) -= g_coef;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 3) += g_coef;

			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 1) += g_coef;
			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 2) += g_coef;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 1) += g_coef;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 2) += g_coef;
		}
		else if (energy_type == 3) //
		{
			OpenMesh::Vec4d& af = A_f[face_id];
			double derivation_e = (a1 - af[0])*(a1 - af[0]) + (a2 - af[1])*(a2 - af[1]) + (b1 - af[2]) * (b1 - af[2]) + (b2 - af[3])*(b2 - af[3]);
			current_em_energy += derivation_e;
			gradient(4 * face_id + 0) += mu * 2.0 *(a1 - af[0]);
			gradient(4 * face_id + 1) += mu * 2.0 *(a2 - af[1]);
			gradient(4 * face_id + 2) += mu * 2.0 *(b1 - af[2]);
			gradient(4 * face_id + 3) += mu * 2.0 *(b2 - af[3]);

			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 0) += 2 * mu;
			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 1) += 2 * mu;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 2) += 2 * mu;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 3) += 2 * mu;
		}
		else
		{
			//nothing
		}
	}

	//if (!is_ivf_below_th)
	{
		hessian += 2.0*ivf_alpha*CTC;
	}

	return ivf_alpha*current_ivf_energy + current_dis_energy + mu*current_em_energy;
}

double assemble_triangle_interface::compute_only_energy(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal)
{
	current_dis_energy = 0.0; double s = amips_s;
	int nf = x.size() / 4; double distortion_e;
	current_em_energy = 0.0; mu = 1e3;
	for (int face_id = 0; face_id < nf;++face_id)
	{
		double a1 = x(4 * face_id + 0); double a2 = x(4 * face_id + 1);
		double b1 = x(4 * face_id + 2); double b2 = x(4 * face_id + 3);

		double det_J = a1*b2 - a2*b1;
		if (det_J < 1e-8) // min_det_j = 1e-8
		{
			return std::numeric_limits<double>::infinity();
		}

		double mips_e = 0.5*(a1*a1 + a2*a2 + b1*b1 + b2*b2) / det_J;
		if (is_conformal)
		{
			distortion_e = mips_e;
		}
		else
		{
			double area_e = 0.5*(det_J + 1.0 / det_J);
			distortion_e = 0.5*(mips_e + area_e);
		}
		if (energy_method == 0)//MIPS
		{
			current_dis_energy += distortion_e;
		}
		else//AMIPS
		{
			double k = s*distortion_e;
			if (k > 60) k = 60;
			double exp_e = std::exp(k);
			current_dis_energy += exp_e;
		}

		if (energy_type == 1)//derivation from input field for global seamless parameterization
		{
			double derivation_e = (a1 - 1.0)*(a1 - 1.0) + a2*a2 + b1*b1 + (b2 - 1.0)*(b2 - 1.0);
			current_em_energy += derivation_e;
		}
		else if (energy_type == 2) //LSCM
		{
			double lacm_e = ((a1 - b2)*(a1 - b2) + (a2 + b1)*(a2 + b1))*src_face_area[block_tri[face_id]];
			current_em_energy += lacm_e;
		}
		else if (energy_type == 3)
		{
			OpenMesh::Vec4d& af = A_f[face_id];
			double derivation_e = (a1 - af[0])*(a1 - af[0]) + (a2 - af[1])*(a2 - af[1]) + (b1 - af[2]) * (b1 - af[2]) + (b2 - af[3])*(b2 - af[3]);
			current_em_energy += derivation_e;
		}
		else
		{
			//nothing
		}
	}

	current_ivf_energy = x.transpose()*CTC*x;
	current_ivf_energy -= 2.0*x.transpose()*CT*CB;
	current_ivf_energy = std::abs(current_ivf_energy + CBTCB);

	return ivf_alpha*current_ivf_energy + current_dis_energy + mu*current_em_energy;
}

void assemble_triangle_interface::update_ivf_alpha()
{
	double s_ivf_alpha = ivf_alpha;
	ivf_alpha = max_ivf_alpha;

	ivf_alpha = current_dis_energy*min_ivf_alpha / current_ivf_energy;
	if (ivf_alpha > max_ivf_alpha) ivf_alpha = max_ivf_alpha;
	if (ivf_alpha < min_ivf_alpha) ivf_alpha = min_ivf_alpha;
}

void assemble_triangle_interface::assign_new_vector_field(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x)
{
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		U_f[face_id] = x(4 * face_id + 0)*e1_f[face_id] + x(4 * face_id + 1)*e2_f[face_id];
		V_f[face_id] = x(4 * face_id + 2)*e1_f[face_id] + x(4 * face_id + 3)*e2_f[face_id];
		
		A_f[face_id][0] = solution(4 * face_id + 0); A_f[face_id][1] = solution(4 * face_id + 1);
		A_f[face_id][2] = solution(4 * face_id + 2); A_f[face_id][3] = solution(4 * face_id + 3);
	}
}

#include "SPARSE\Sparse_Matrix.h"
#include "SPARSE\Sparse_Solver.h"
void assemble_triangle_interface::reconstruct_UV(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x)
{
	unsigned nv = mesh_->n_vertices(); unsigned nf = mesh_->n_faces();

	//fix two points
	Mesh::FaceHandle fh = mesh_->face_handle(0);
	Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(fh); 
	Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it);
	mesh_->data(v0).set_New_Pos(OpenMesh::Vec3d(0.0, 0.0, 0.0)); mesh_->data(v0).set_new_pos_fixed(true);
	const OpenMesh::Vec3d& p0 = mesh_->point(v0);
	Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it);
	const OpenMesh::Vec3d& p1 = mesh_->point(v1);
	double x1 = OpenMesh::dot(p1 - p0, e1_f[0]); double y1 = OpenMesh::dot(p1 - p0, e2_f[0]);
	mesh_->data(v1).set_New_Pos(OpenMesh::Vec3d(x1*x(0) + y1*x(1), x1*x(2) + y1*x(3), 0.0)); mesh_->data(v1).set_new_pos_fixed(true);

	int vertex_count_ok = 0;
	std::vector<int> map_with_vertex_UV(nv, -1);
	for (unsigned i = 0; i < nv; ++i)
	{
		if (mesh_->data(mesh_->vertex_handle(i)).get_new_pos_fixed() )
		{
			continue;
		}
		else
		{
			map_with_vertex_UV[i] = vertex_count_ok;
			++vertex_count_ok;
		}
	}

	Sparse_Matrix A(2 * nf, vertex_count_ok, NOSYM, CCS, 2);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle vi = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& pi = mesh_->point(vi);
		double xi = 0; double yi = 0;
		Mesh::VertexHandle vj = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pj = mesh_->point(vj);
		double xj = OpenMesh::dot(pj - pi, e1_f[face_id]); double yj = OpenMesh::dot(pj - pi, e2_f[face_id]);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle vk = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& pk = mesh_->point(vk);
		double xk = OpenMesh::dot(pk - pi, e1_f[face_id]); double yk = OpenMesh::dot(pk - pi, e2_f[face_id]);
		double face_area = OpenMesh::cross(pj - pi, pk - pi).norm();
		double i_face_area = 1.0 / face_area; 
		//i
		if (mesh_->data(vi).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vi).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yj - yk)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xk - xj)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yj - yk)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xk - xj)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vi.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yj - yk));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xk - xj));
		}
		
		//j
		if (mesh_->data(vj).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vj).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yk - yi)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xi - xk)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yk - yi)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xi - xk)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vj.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yk - yi));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xi - xk));
		}
		//k
		if (mesh_->data(vk).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vk).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yi - yj)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xj - xi)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yi - yj)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xj - xi)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vk.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yi - yj));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xj - xi));
		}

		A.fill_rhs_entry(face_id * 2 + 0, 0, x(4 * face_id + 0));
		A.fill_rhs_entry(face_id * 2 + 1, 0, x(4 * face_id + 1));

		A.fill_rhs_entry(face_id * 2 + 0, 1, x(4 * face_id + 2));
		A.fill_rhs_entry(face_id * 2 + 1, 1, x(4 * face_id + 3));
	}

	Sparse_Matrix* B = TransposeTimesSelf(&A, CCS, SYM_LOWER, true);
	solve_by_CHOLMOD(B);
	const std::vector<double>& uv = B->get_solution();
	int vertex_id;
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		vertex_id = v_it.handle().idx();
		if (map_with_vertex_UV[vertex_id] >=0)
		{
			mesh_->data(v_it).set_New_Pos(OpenMesh::Vec3d(uv[map_with_vertex_UV[vertex_id]], uv[map_with_vertex_UV[vertex_id] + vertex_count_ok], 0.0));
		}
	}
	delete B;

	flip_count = 0;
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0);
		n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);

		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix
		Eigen::Matrix2d XY, UV, A;
		XY << l01, x2, 0, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse();
		A_f[face_id][0] = A(0, 0); A_f[face_id][1] = A(0, 1); A_f[face_id][2] = A(1, 0); A_f[face_id][3] = A(1, 1);
		if (A.determinant() < 0)
		{
			flip_count++;
		}
		else
		{
		}
	}
	//printf("Flipped Triangles: %d\n", flip_count);
}

bool assemble_triangle_interface::optimize_IVF(Mesh* mesh_, bool is_conformal)
{
	int iter_count = 0; step_size = 1.0; 
	min_ivf_alpha = 1e3; double pre_ivf_alpha = min_ivf_alpha;
	int optimization_stop = 0;
	//int var_num = 4 * mesh_->n_faces();
	int var_num = solution.size(); bool update_sucess = false;
	bool is_ivf_below_th = false; int is_ivf_below_th_count = 0; pre_current_dis_energy = 1; double pre_ivf_energy = 1.0;
	while (iter_count < max_iter && step_size > min_step_size)
	{
		double old_e = compute_energy_derivative_modified_hessian(mesh_, solution, is_conformal);
		lltSolver.factorize(hessian);
		int status = lltSolver.info();
		if (status == Eigen::Success)
		{  
			lambda = min_lambda;
		}
		else
		{
			int run = 0;
			while (status != Eigen::Success && lambda < max_lambda)
			{
				for (int i = 0; i < var_num; i++)
					hessian.coeffRef(i, i) += lambda;

				lltSolver.factorize(hessian);
				status = lltSolver.info();

				if (status != Eigen::Success)
				{
					if (lambda < max_lambda)
						lambda *= 10;
				}
				else
				{
					if (run == 0 && lambda > min_lambda)
					{
						lambda *= 0.1;
					}
				}
				run++;
			}
		}
		if (lambda < max_lambda)
		{
			// compute newton step direction
			step = lltSolver.solve(-gradient);
		}
		else
		{
			// gradient descent
			lambda *= 0.1;
			step = -gradient;
		}
		prevSolution = solution + step_size*step;  update_sucess = true;
		double new_e = compute_only_energy(mesh_, prevSolution, is_conformal);
		if (new_e > old_e)
		{
			while (new_e > old_e)
			{
				if (step_size < min_step_size)
				{
					update_sucess = false;
					break;
				}
				step_size *= 0.5;
				//compute_new_ctr_point(new_ctr_point_1, new_ctr_point_2);
				prevSolution = solution + step_size*step;
				new_e = compute_only_energy(mesh_, prevSolution, is_conformal);
			}
		}
		else
		{
			step_size *= 2.0;
		}
		++iter_count;
		printf("%d A:%e,L:%3.2e,S:%4.3e,I:%e,D:%e,%e\n", iter_count, ivf_alpha, lambda, step_size, current_ivf_energy, current_dis_energy, current_em_energy);

		if ((current_ivf_energy < 1e-12 || is_ivf_below_th) && !update_sucess) break;

		solution = prevSolution;

		if (current_ivf_energy < 1e-12 && !is_ivf_below_th)
		{
			is_ivf_below_th = true;
		}
		else if (current_ivf_energy < 1e-12 && is_ivf_below_th)
		{
			if (std::abs(current_dis_energy - pre_current_dis_energy) / pre_current_dis_energy < 1e-8)
			{
				break;
			}
			is_ivf_below_th_count++;
			if (is_ivf_below_th_count > 7) break;
		}
		else
		{
			double re_dis = std::abs(current_dis_energy - pre_current_dis_energy) / pre_current_dis_energy;
			double re_ivf = std::abs(current_ivf_energy - pre_ivf_energy) / pre_ivf_energy;
			if (re_ivf < 0.01)
			{
				++optimization_stop;
			}
			else
			{
				optimization_stop = 0;
			}
		}
		
		pre_current_dis_energy = current_dis_energy; pre_ivf_energy = current_ivf_energy;
		
		if (step_size < 1e-12 || lambda > 1e10)
		{
			step_size = 1.0;
			lambda = min_lambda;
			printf("--------------------------------------\n");
			printf("reinitilize...............\n");
			min_ivf_alpha = 1e0;
			pre_ivf_alpha = 1e3;
			//update_ivf_alpha();
		}
		else
		{
			if (optimization_stop > 1)
			{
				double temp_alpha = current_dis_energy * min_ivf_alpha / current_ivf_energy;
				if (temp_alpha > max_ivf_alpha)
				{
					//if (pre_ivf_alpha > 1) pre_ivf_alpha *= 0.1;
					if (max_ivf_alpha < 1e20) max_ivf_alpha *= 10;
				}
				else
				{
					if (pre_ivf_alpha < 1e7) pre_ivf_alpha *= 10;
					if (max_ivf_alpha > 1e16) max_ivf_alpha *= 0.1;
				}
				if (step_size < 1.0) step_size = 1.0;
				min_ivf_alpha = pre_ivf_alpha;
				optimization_stop = 0;
			}
			else
			{
				min_ivf_alpha = pre_ivf_alpha;
			}
		}

		update_ivf_alpha();
		if (is_ivf_below_th && ivf_alpha < 1e16)
		{
			ivf_alpha = 1e16;
		}
	}

	printf("Iteration Number : %d\n", iter_count);
	if (current_ivf_energy < 1e-12)
	{
		return true;
	}

	return false;
}

bool assemble_triangle_interface::optimize_IVF2(Mesh* mesh_, bool is_conformal)
{
	int iter_count = 0; step_size = 1.0;
	min_ivf_alpha = 1e3; double pre_ivf_alpha = min_ivf_alpha;
	int optimization_stop = 0;
	//int var_num = 4 * mesh_->n_faces();
	int var_num = solution.size(); bool update_sucess = false;
	bool is_ivf_below_th = false; int is_ivf_below_th_count = 0; pre_current_dis_energy = 1; double pre_ivf_energy = 1.0;
	for (int i = 0; i < 13; ++i)
	{
		iter_count = 0; step_size = 1;
		while (iter_count < max_iter && step_size > min_step_size)
		{
			double old_e = compute_energy_derivative_modified_hessian(mesh_, solution, is_conformal);
			lltSolver.factorize(hessian);
			int status = lltSolver.info();
			if (status == Eigen::Success)
			{
				lambda = min_lambda;
			}
			else
			{
				int run = 0;
				while (status != Eigen::Success && lambda < max_lambda)
				{
					for (int i = 0; i < var_num; i++)
						hessian.coeffRef(i, i) += lambda;

					lltSolver.factorize(hessian);
					status = lltSolver.info();

					if (status != Eigen::Success)
					{
						if (lambda < max_lambda)
							lambda *= 10;
					}
					else
					{
						if (run == 0 && lambda > min_lambda)
						{
							lambda *= 0.1;
						}
					}
					run++;
				}
			}
			if (lambda < max_lambda)
			{
				// compute newton step direction
				step = lltSolver.solve(-gradient);
			}
			else
			{
				// gradient descent
				lambda *= 0.1;
				step = -gradient;
			}
			prevSolution = solution + step_size*step;  update_sucess = true;
			double new_e = compute_only_energy(mesh_, prevSolution, is_conformal);
			if (new_e > old_e)
			{
				while (new_e > old_e)
				{
					if (step_size < min_step_size)
					{
						update_sucess = false;
						break;
					}
					step_size *= 0.5;
					//compute_new_ctr_point(new_ctr_point_1, new_ctr_point_2);
					prevSolution = solution + step_size*step;
					new_e = compute_only_energy(mesh_, prevSolution, is_conformal);
				}
			}
			else
			{
				step_size *= 2.0;
			}
			++iter_count;
			printf("%d %d A:%e,L:%3.2e,S:%4.3e,I:%e,D:%e,%e\n", i, iter_count, ivf_alpha, lambda, step_size, current_ivf_energy, current_dis_energy, std::abs(new_e - old_e) / old_e);

			solution = prevSolution;

			if (std::abs(new_e - old_e) / old_e < 1e-6)
			{
				break;
			}
		}

		ivf_alpha *= 10;
	}

	return true;
}

void assemble_triangle_interface::initialize_one_matrix(Eigen::Matrix2d& A, bool is_conformal)
{
	Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
	Eigen::Matrix2d U = svd.matrixU(); Eigen::Matrix2d V = svd.matrixV();
	Eigen::Matrix2d rot = V*U.transpose();
	Eigen::Vector2d sv = svd.singularValues();
	double det = rot.determinant();
	if (det < 0)
	{
		if (sv(0) < sv(1))
		{
			U(0, 0) = -U(0, 0); U(1, 0) = -U(1, 0);
			sv(0) = -sv(0);
		}
		else
		{
			U(0, 1) = -U(0, 1); U(1, 1) = -U(1, 1);
			sv(1) = -sv(1);
		}
		double temp_s = 1;
		if (is_conformal)
		{
			temp_s = 0.5*(sv(0)+sv(1)) + 1e-8;
		}
		A = V*U.transpose()*temp_s;
	}
	else
	{
		if (is_conformal)
		{
			double temp_s = 0.5*(std::abs(sv(0)) + std::abs(sv(1)));
			A = temp_s* rot;
		}
		else
		{
			A = rot;
		}
	}
}