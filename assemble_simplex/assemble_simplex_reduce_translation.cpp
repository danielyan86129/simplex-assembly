#include "assemble_simplex.h"
#include <queue>
#include <time.h>

void assemble_triangle_interface::load_handles_setting(Mesh* mesh_, const char* filename)
{
	FILE* f_dc = fopen(filename, "r");
	char buf[4096];
	OpenMesh::Vec3d np(0, 0, 0);
	char x[128]; char y[128]; char z[128]; char v_id[128];
	while (!feof(f_dc))
	{
		fgets(buf, 4096, f_dc);
		sscanf(buf, "%s %s %s %s", v_id, x, y, z);
		Mesh::VertexHandle vh = mesh_->vertex_handle(atoi(v_id));
		mesh_->data(vh).set_new_pos_fixed(true);
	}
	fclose(f_dc);
}

void assemble_triangle_interface::optimize_affine_transformation(Mesh* mesh_, const char* filename_uv, const char* filename_de, int max_iter_)
{
	load_initial_uv(mesh_, filename_uv);
	load_handles_setting(mesh_, filename_de);

	//change to what you want.
	bool is_conformal = true; is_bounded = false; //bound_k = 5 + std::sqrt(24);
	bound_k = 4 + std::sqrt(15);

	//eliminate translations variables
	initilize_affine_trans_no_translation(mesh_, is_conformal);

	energy_method = 1; amips_s = 1; max_iter = max_iter_;// energy_type = 3;
	energy_type = 0;
	compute_only_energy_AT_no_translation(mesh_, solution, is_conformal);
	update_ivf_alpha();

	long start_t = clock();

	optimize_AT_no_translation(mesh_, is_conformal);

	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;

	printf("Optimization Time: %f s\n", t);

	reconstruct_UV_no_translation(mesh_, solution);
	
	std::string uv(filename_uv);
	uv.append("_result.obj");
	save_result_uv(mesh_, uv.c_str());
}

void assemble_triangle_interface::initilize_affine_trans_no_translation(Mesh* mesh_, bool is_conformal)
{
	unsigned nf = mesh_->n_faces(); int handle_count = 0; std::vector<Mesh::VertexHandle> handles; 
	unsigned nv = mesh_->n_vertices(); 
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		if (mesh_->data(v_it).get_new_pos_fixed())
		{
			handles.push_back(v_it.handle());
			++handle_count;
		}
	}

	std::vector< std::vector<int> > handles_with_same_label;
	int handle_label = 0; std::vector<int> handles_visited(nv, -1);
	std::vector<int> one_same_label;
	for (int i = 0; i < handles.size(); ++i)
	{
		if (handles_visited[handles[i].idx()] >= 0) continue;

		handles_visited[handles[i].idx()] = handle_label;
		std::queue<Mesh::VertexHandle> Q; Q.push(handles[i]);
		one_same_label.clear();
		while (Q.size() > 0)
		{
			Mesh::VertexHandle seed_vh = Q.front(); Q.pop();
			one_same_label.push_back(seed_vh.idx());
			for (Mesh::VertexOHalfedgeIter voh_it = mesh_->voh_iter(seed_vh); voh_it; ++voh_it)
			{
				Mesh::VertexHandle vh = mesh_->to_vertex_handle(voh_it); int v_id = vh.idx();
				if (mesh_->data(vh).get_new_pos_fixed() && handles_visited[vh.idx()] == -1) //it is a handle and not visited
				{
					Q.push(vh); handles_visited[vh.idx()] = handle_label;
				}
			}
		}
		handles_with_same_label.push_back(one_same_label);
		++handle_label;
	}

	unsigned ne = mesh_->n_edges();
	std::vector<std::vector<int>> edges_; edges_.reserve(handle_label*handle_label);
	std::vector<std::vector<int>> dis_(handle_label);
	for (int i = 0; i < handle_label; ++i) dis_[i].resize(handle_label);

	for (int i = 0; i < handle_label; ++i) //for label i
	{
		std::vector<int>& one_same_label_v = handles_with_same_label[i];
		std::vector<int> distance(nv, ne * 10); std::vector<int> visited(nv, -1);
		std::vector<std::vector<int>> edges_for_v(nv);
		for (int j = 0; j < one_same_label_v.size(); ++j)
		{
			int v_id = one_same_label_v[j];
			distance[v_id] = 0; visited[v_id] = 1;

			for (Mesh::VertexOHalfedgeIter voh_it = mesh_->voh_iter(mesh_->vertex_handle(v_id)); voh_it; ++voh_it) //one-ring
			{
				int vv_id = mesh_->to_vertex_handle(voh_it).idx();
				if (handles_visited[vv_id] != i && distance[vv_id] == ne * 10) // different label
				{
					distance[vv_id] = 1;
					edges_for_v[vv_id].push_back(mesh_->edge_handle(voh_it).idx());
				}
			}
		}

		int count_label = 0;
		while (count_label < handle_label - 1)
		{
			//find smallest distance
			int min_v_id = -1; int min_dis = ne*10;
			for (int j = 0; j < nv; ++j)
			{
				if (visited[j] == -1 && distance[j] < min_dis)
				{
					min_dis = distance[j]; min_v_id = j;
				}
			}

			if (min_v_id < 0)
			{
				printf("%d %d\n", min_v_id, min_dis);
				break;
			}

			visited[min_v_id] = 1;
			//update from min
			if (handles_visited[min_v_id] < 0) //not handles
			{
				for (Mesh::VertexOHalfedgeIter voh_it = mesh_->voh_iter(mesh_->vertex_handle(min_v_id)); voh_it; ++voh_it) //one-ring
				{
					int vv_id = mesh_->to_vertex_handle(voh_it).idx();
					if (visited[vv_id] == -1 && distance[vv_id] > min_dis + 1) //bigger and not visited
					{
						if (handles_visited[vv_id] < 0) // not handles
						{
							distance[vv_id] = min_dis + 1;
							edges_for_v[vv_id] = edges_for_v[min_v_id];
							edges_for_v[vv_id].push_back(mesh_->edge_handle(voh_it).idx());

						}
						else //handles
						{
							int _label = handles_visited[vv_id];
							for (int j = 0; j < handles_with_same_label[_label].size(); ++j) //updating all vertices with same label
							{
								int _v_id = handles_with_same_label[_label][j];
								distance[_v_id] = min_dis + 1;
								edges_for_v[_v_id] = edges_for_v[min_v_id];
								edges_for_v[_v_id].push_back(mesh_->edge_handle(voh_it).idx());
							}
						}
					}
				}
			}
			else //handles
			{
				int min_label = handles_visited[min_v_id]; ++count_label;
				for (int j = 0; j < handles_with_same_label[min_label].size(); ++j)//update all vertices with same label
				{
					int min_vv_id = handles_with_same_label[min_label][j];
					visited[min_vv_id] = 1;
				}

				for (int j = 0; j < handles_with_same_label[min_label].size(); ++j)//update all vertices with same label
				{
					int min_vv_id = handles_with_same_label[min_label][j];
					for (Mesh::VertexOHalfedgeIter voh_it = mesh_->voh_iter(mesh_->vertex_handle(min_vv_id)); voh_it; ++voh_it) //one-ring
					{
						int vv_id = mesh_->to_vertex_handle(voh_it).idx();
						if (visited[vv_id] == -1 && distance[vv_id] > min_dis + 1) //bigger and not visited
						{
							if (handles_visited[vv_id] < 0) // not handles
							{
								distance[vv_id] = min_dis + 1;
								edges_for_v[vv_id] = edges_for_v[min_v_id];
								edges_for_v[vv_id].push_back(mesh_->edge_handle(voh_it).idx());

							}
							else //handles
							{
								int _label = handles_visited[vv_id];
								for (int j = 0; j < handles_with_same_label[_label].size(); ++j) //updating all vertices with same label
								{
									int _v_id = handles_with_same_label[_label][j];
									distance[_v_id] = min_dis + 1;
									edges_for_v[_v_id] = edges_for_v[min_v_id];
									edges_for_v[_v_id].push_back(mesh_->edge_handle(voh_it).idx());
								}
							}
						}
					}//end for voh_it
				}//end for j
			}
		}

		for (int j = 0; j < handle_label; ++j)
		{
			//if (j == i) continue;

			int v_id = handles_with_same_label[j][0];
			edges_.push_back(edges_for_v[v_id]); 
			dis_[i][j] = distance[v_id]; //dis_[j][i] = distance[v_id];
		}
	}

	//minimal support tree
	std::vector<OpenMesh::Vec2i> MST_edge; MST_edge.reserve(handle_label - 1);
	std::vector<int> MST_node; MST_node.reserve(handle_label); MST_node.push_back(0);
	std::vector<int> visited_node(handle_label, -1); visited_node[0] = 1;
	while (MST_node.size() != handle_label)
	{
		int min_dis = ne * 10; int min_node = -1; OpenMesh::Vec2i one_MST_edge;
		for (int i = 0; i < MST_node.size(); ++i) //for all nodes in tree
		{
			int node = MST_node[i]; 
			for (int j = 0; j < handle_label; ++j) //for all nodes not in tree
			{
				if (visited_node[j] == -1 && j != node && dis_[node][j] < min_dis)
				{
					min_dis = dis_[node][j]; min_node = j;
					one_MST_edge[0] = node; one_MST_edge[1] = j;
				}
			}
		}

		if (min_node < 0) break;

		visited_node[min_node] = 1;
		MST_node.push_back(min_node); MST_edge.push_back(one_MST_edge);
	}
	
	edge_translation.clear();
	for (int i = 0; i < MST_edge.size(); ++i)
	{
		int edge_id = MST_edge[i][0] * handle_label + MST_edge[i][1];
		for (int j = 0; j < edges_[edge_id].size(); ++j)
		{
			edge_translation.push_back(edges_[edge_id][j]);
		}
	}

	//determine translation variable
	face_translation.clear(); face_translation.resize(nf, -1);
	int translation_var_count = 0; int only_one_face = -1;
	if (handle_label == 1)
	{
		translation_var_count = 1;
		Mesh::VertexFaceIter vf_it = mesh_->vf_iter(handles[0]);
		face_translation[vf_it->idx()] = 0; only_one_face = vf_it->idx();
	}
	else
	{
		for (int i = 0; i < edge_translation.size(); ++i)
		{
			Mesh::HalfedgeHandle heh = mesh_->halfedge_handle(mesh_->edge_handle(edge_translation[i]), 0);
			Mesh::FaceHandle fh = mesh_->face_handle(heh);
			if (fh == Mesh::InvalidFaceHandle)
			{
				fh = mesh_->face_handle(mesh_->opposite_halfedge_handle(heh));
			}
			if (face_translation[fh.idx()] == -1)
			{
				face_translation[fh.idx()] = translation_var_count;
				++translation_var_count;
			}
		}
	}

	/* find the 1st part of hessian matrix */
	//int var_num = 4 * nf + 2 * handles_with_same_label.size();
	int var_num = 4 * nf + 2 * translation_var_count; srand((unsigned)time(NULL));
	printf("Variable Number : %d (%d)\n", var_num, translation_var_count);
	std::vector<Eigen::Triplet<double> > f_triplets; solution.resize(var_num); A_f.resize(nf);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		/// build mapping for each face: rest shape > new shape 
		// grab rest shape vertices
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);
		// old frame (rest shape)
		double x10 = p1[0] - p0[0]; double x20 = p2[0] - p0[0];
		double y10 = p1[1] - p0[1]; double y20 = p2[1] - p0[1];

		int face_id = f_it.handle().idx();
		
		// new shape vertices
		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix: new-frame*old-frame^-1
		double det_ = 1.0 / (x10*y20 - x20*y10);
		double A_00 = ((q1[0] - q0[0])*y20 - (q2[0] - q0[0])*y10)*det_; double A_01 = (-(q1[0] - q0[0])*x20 + (q2[0] - q0[0])*x10)*det_;
		double A_10 = ((q1[1] - q0[1])*y20 - (q2[1] - q0[1])*y10)*det_; double A_11 = (-(q1[1] - q0[1])*x20 + (q2[1] - q0[1])*x10)*det_;

		Eigen::Matrix2d A; A << A_00, A_01, A_10, A_11;
		initialize_one_matrix(A, is_conformal);

		// put A of cur face to proper location of the big column vector
		A_f[face_id] = OpenMesh::Vec4d(A_00, A_01, A_10, A_11);

		solution(4 * face_id + 0) = A(0, 0); solution(4 * face_id + 1) = A(0, 1);
		solution(4 * face_id + 2) = A(1, 0); solution(4 * face_id + 3) = A(1, 1);

		// this corresponds to hessian of E_C ( diagonal matrix of block of d^2xd^2 )
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				f_triplets.push_back(Eigen::Triplet<double>(4 * face_id + i, 4 * face_id + j, 1.0));
			}
		}
	}

	/* hessian of E_assembly */
	//unsigned ne = mesh_->n_edges();
	std::vector< int > is_edge_two_handles(ne, -1);
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		Mesh::VertexHandle vh0 = mesh_->to_vertex_handle(heh1);
		Mesh::VertexHandle vh1 = mesh_->to_vertex_handle(heh0);
		
		if (mesh_->data(vh0).get_new_pos_fixed() && mesh_->data(vh1).get_new_pos_fixed())
		{
			is_edge_two_handles[e_it.handle().idx()] = 1;
		}
	}

	std::vector<Eigen::Triplet<double> > c_triplets; std::vector<Eigen::Triplet<double> > triplets;
	int constraint_count = 0; std::vector<double> b;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		if (mesh_->is_boundary(e_it) && is_edge_two_handles[e_it.handle().idx()] == -1) continue;

		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		const OpenMesh::Vec3d& p0 = mesh_->point(mesh_->to_vertex_handle(heh1));
		const OpenMesh::Vec3d& p1 = mesh_->point(mesh_->to_vertex_handle(heh0));
		OpenMesh::Vec3d e = (p1 - p0);

		Mesh::FaceHandle fh0 = mesh_->face_handle(heh0); int f0 = fh0.idx();
		Mesh::FaceHandle fh1 = mesh_->face_handle(heh1); int f1 = fh1.idx();

		//one edge, two equation
		if (is_edge_two_handles[e_it.handle().idx()] == 1)
		{
			const OpenMesh::Vec3d& np0 = mesh_->data(mesh_->to_vertex_handle(heh1)).get_New_Pos();
			const OpenMesh::Vec3d& np1 = mesh_->data(mesh_->to_vertex_handle(heh0)).get_New_Pos();
			OpenMesh::Vec3d ne = (np1 - np0);
			if (f0 >= 0)
			{
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count+1, 4 * f0 + 2, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count+1, 4 * f0 + 3, 1.0));

				triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, e[0]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, e[1]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, e[0]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, e[1]));
				b.push_back(ne[0]); b.push_back(ne[1]);
				constraint_count += 2;
			}
			if (f1 >= 0)
			{
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, 1.0));

				triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, e[0]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, e[1]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, e[0]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, e[1]));
				b.push_back(ne[0]); b.push_back(ne[1]);
				constraint_count += 2;
			}

		}
		else //no boundary edge
		{
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, 1.0));

			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, e[0]));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, e[1]));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, -e[0]));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, -e[1]));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, e[0]));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, e[1]));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, -e[0]));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, -e[1]));

			b.push_back(0.0); b.push_back(0.0);
			constraint_count += 2;
		}
	}

	if (handle_label == 1)
	{
		int face_id = only_one_face;
		double A_00 = solution(4 * face_id + 0); double A_01 = solution(4 * face_id + 1);
		double A_10 = solution(4 * face_id + 2); double A_11 = solution(4 * face_id + 3);
		int tx = 4 * nf + 0; int ty = 4 * nf + 1;
		for (Mesh::FaceVertexIter fv_it = mesh_->fv_iter(mesh_->face_handle(face_id)); fv_it; ++fv_it)
		{
			if (mesh_->data(fv_it).get_new_pos_fixed())
			{
				const OpenMesh::Vec3d& sp2 = mesh_->point(fv_it);
				const OpenMesh::Vec3d& np2 = mesh_->data(fv_it).get_New_Pos();
				//x
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 0, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 1, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count, tx, 1.0));
				triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 0, sp2[0]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 1, sp2[1]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count, tx, 1.0));
				b.push_back(np2[0]);
				//y
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 2, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 3, 1.0));
				c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, ty, 1.0));
				triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 2, sp2[0]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 3, sp2[1]));
				triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, ty, 1.0));
				b.push_back(np2[1]);

				constraint_count += 2;

				OpenMesh::Vec3d nq(A_00*sp2[0] + A_01*sp2[1], A_10*sp2[0] + A_11*sp2[1], 0);
				solution(tx) = np2[0] - nq[0]; solution(ty) = np2[1] - nq[1];
			}
		}
	}
	else
	{
		std::vector<int> is_visited_vertex_t(nv, -1); std::vector<Mesh::VertexHandle> two_ends(2);
		int two_face[2]; int two_face_c = 0;
		for (int i = 0; i < edge_translation.size(); ++i)
		{
			Mesh::HalfedgeHandle heh = mesh_->halfedge_handle(mesh_->edge_handle(edge_translation[i]), 0);
			two_ends[0] = mesh_->from_vertex_handle(heh); two_ends[1] = mesh_->to_vertex_handle(heh);
			for (int j = 0; j < 2; ++j)
			{
				if (is_visited_vertex_t[two_ends[j].idx()] == 1) continue;

				is_visited_vertex_t[two_ends[j].idx()] = 1; two_face_c = 0;

				for (Mesh::VertexFaceIter vf_it = mesh_->vf_iter(two_ends[j]); vf_it; ++vf_it)
				{
					int face_id = vf_it->idx();
					if (face_translation[face_id] >= 0)
					{
						two_face[two_face_c] = face_id;
						++two_face_c;
					}
					//no handles, only need two face
					if (!mesh_->data(two_ends[j]).get_new_pos_fixed() && two_face_c == 2) break;
				}

				const OpenMesh::Vec3d& sp = mesh_->point(two_ends[j]);
				const OpenMesh::Vec3d& np = mesh_->data(two_ends[j]).get_New_Pos();
				if (mesh_->data(two_ends[j]).get_new_pos_fixed())//handles
				{
					for (int k = 0; k < two_face_c; ++k)
					{
						int face_id = two_face[k]; int face_translation_var_id = face_translation[face_id];
						double A_00 = solution(4 * face_id + 0); double A_01 = solution(4 * face_id + 1);
						double A_10 = solution(4 * face_id + 2); double A_11 = solution(4 * face_id + 3);
						OpenMesh::Vec3d nq(A_00*sp[0] + A_01*sp[1], A_10*sp[0] + A_11*sp[1], 0);
						int tx = 4 * nf + 2 * face_translation_var_id + 0; int ty = 4 * nf + 2 * face_translation_var_id + 1;
						solution(tx) = np[0] - nq[0]; solution(ty) = np[1] - nq[1];
						//x
						c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 0, 1.0));
						c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 1, 1.0));
						c_triplets.push_back(Eigen::Triplet<double>(constraint_count, tx, 1.0));
						triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 0, sp[0]));
						triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * face_id + 1, sp[1]));
						triplets.push_back(Eigen::Triplet<double>(constraint_count, tx, 1.0));
						b.push_back(np[0]);
						//y
						c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 2, 1.0));
						c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 3, 1.0));
						c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, ty, 1.0));
						triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 2, sp[0]));
						triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * face_id + 3, sp[1]));
						triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, ty, 1.0));
						b.push_back(np[1]);

						constraint_count += 2;
					}
				}
				else
				{
					if (two_face_c != 2) printf("Error : No handle with less than 2 face translation!\n");

					int f0 = two_face[0]; int f1 = two_face[1];
					int t0_x = 4 * nf + 2 * face_translation[f0] + 0; int t0_y = 4 * nf + 2 * face_translation[f0] + 1;
					int t1_x = 4 * nf + 2 * face_translation[f1] + 0; int t1_y = 4 * nf + 2 * face_translation[f1] + 1;

					double A_00 = solution(4 * f0 + 0); double A_01 = solution(4 * f0 + 1);
					double A_10 = solution(4 * f0 + 2); double A_11 = solution(4 * f0 + 3);
					OpenMesh::Vec3d nq(A_00*sp[0] + A_01*sp[1], A_10*sp[0] + A_11*sp[1], 0);
					solution(t0_x) = np[0] - nq[0]; solution(t0_y) = np[1] - nq[1];

					A_00 = solution(4 * f1 + 0); A_01 = solution(4 * f1 + 1);
					A_10 = solution(4 * f1 + 2); A_11 = solution(4 * f1 + 3);
					OpenMesh::Vec3d nq1(A_00*sp[0] + A_01*sp[1], A_10*sp[0] + A_11*sp[1], 0);
					solution(t1_x) = np[0] - nq1[0]; solution(t1_y) = np[1] - nq1[1];

					c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count, t0_x, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count, t1_x, 1.0));

					c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, t0_y, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, 1.0));
					c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, t1_y, 1.0));

					triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, sp[0]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, sp[1]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count, t0_x, 1.0));
					triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, -sp[0]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, -sp[1]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count, t1_x, -1.0));

					triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, sp[0]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, sp[1]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, t0_y, 1.0));
					triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, -sp[0]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, -sp[1]));
					triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, t1_y, -1.0));

					b.push_back(0.0); b.push_back(0.0);
					constraint_count += 2;
				}
			}
		}
	}

 	Eigen::SparseMatrix<double> C; C.resize(constraint_count, var_num);
	C.setFromTriplets(c_triplets.begin(), c_triplets.end());
	CT = C.transpose();
	CTC.resize(var_num, var_num);
	CTC = CT*C;
	Eigen::SparseMatrix<double> H;
	H.resize(var_num, var_num);
	H.setFromTriplets(f_triplets.begin(), f_triplets.end());

	hessian.resize(var_num, var_num);
	hessian = H + CTC;
	lltSolver.analyzePattern(hessian);

	C *= 0.0;
	C.setFromTriplets(triplets.begin(), triplets.end());
	CT = C.transpose();
	CTC = CT*C;
	CB.resize(constraint_count);
	for (int i = 0; i < constraint_count; ++i)
	{
		CB(i) = b[i];
	}
	CBTCB = CB.transpose() * CB;
	gradient.resize(var_num); prevSolution.resize(var_num); step.resize(var_num);
	prepare_ok = true;
}

double assemble_triangle_interface::compute_energy_derivative_AT_no_translation(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal, bool update_hessian)
{
	current_ivf_energy = x.transpose()*CTC*x;
	current_ivf_energy -= 2.0*x.transpose()*CT*CB;
	current_ivf_energy = std::abs(current_ivf_energy + CBTCB);

	gradient = 2.0*ivf_alpha*(CTC*x - CT*CB);
	//Eigen::Matrix4d H; 
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix4d> es;
	hessian *= 0; current_dis_energy = 0.0; double s = amips_s;  current_em_energy = 0.0; //mu = 1e3;
	int nf = mesh_->n_faces(); double distortion_e; //std::vector<Eigen::Matrix4d> fH(nf);
	double g_a1, g_a2, g_b1, g_b2; double K = (bound_k + 1 / bound_k) / 2; Eigen::Matrix4d H;
	double h_a1_a1, h_a1_a2, h_a1_b1, h_a1_b2, h_a2_a2, h_a2_b1, h_a2_b2, h_b1_b1, h_b1_b2, h_b2_b2;
	for (int face_id = 0; face_id < nf; ++face_id)
	{
		double a1 = x(4 * face_id + 0); double a2 = x(4 * face_id + 1);
		double b1 = x(4 * face_id + 2); double b2 = x(4 * face_id + 3);
		double det_J = a1*b2 - a2*b1;
		double mips_e = 0.5*(a1*a1 + a2*a2 + b1*b1 + b2*b2) / det_J;
		if (is_conformal)
		{
			distortion_e = mips_e;
		}
		else
		{
			double area_e = 0.5*(det_J + 1.0 / det_J);
			distortion_e = 0.5*(mips_e + area_e);
		}
		//AMIPS
		{
			if (is_conformal)
			{
				double temp_f = a1*a1 + a2*a2 + b1*b1 + b2*b2;
				g_a1 = a1 / det_J - (b2*temp_f) / (2 * det_J*det_J);
				g_a2 = a2 / det_J + (b1*temp_f) / (2 * det_J*det_J);
				g_b1 = b1 / det_J + (a2*temp_f) / (2 * det_J*det_J);
				g_b2 = b2 / det_J - (a1*temp_f) / (2 * det_J*det_J);

				h_a1_a1 = 1 / det_J - (2 * a1*b2) / (det_J*det_J) + (b2*b2 * temp_f) / (det_J*det_J*det_J);
				h_a1_a2 = (a1*b1) / (det_J*det_J) - (a2*b2) / (det_J*det_J) - (b1*b2*temp_f) / (det_J*det_J*det_J);
				h_a1_b1 = (a1*a2) / (det_J*det_J) - (b1*b2) / (det_J*det_J) - (a2*b2*temp_f) / (det_J*det_J*det_J);
				h_a1_b2 = (a1*b2*temp_f) / (det_J*det_J*det_J) - b2*b2 / (det_J*det_J) - temp_f / (2 * (det_J*det_J)) - a1*a1 / (det_J*det_J);

				h_a2_a2 = 1 / det_J + (2 * a2*b1) / (det_J*det_J) + (b1*b1 * temp_f) / (det_J*det_J*det_J);
				h_a2_b1 = a2*a2 / (det_J*det_J) + b1*b1 / (det_J*det_J) + temp_f / (2 * (det_J*det_J)) + (a2*b1*temp_f) / (det_J*det_J*det_J);
				h_a2_b2 = (b1*b2) / (det_J*det_J) - (a1*a2) / (det_J*det_J) - (a1*b1*temp_f) / (det_J*det_J*det_J);

				h_b1_b1 = 1 / det_J + (2 * a2*b1) / (det_J*det_J) + (a2*a2 * temp_f) / (det_J*det_J*det_J);
				h_b1_b2 = (a2*b2) / (det_J*det_J) - (a1*b1) / (det_J*det_J) - (a1*a2*temp_f) / (det_J*det_J*det_J);
				h_b2_b2 = 1 / det_J - (2 * a1*b2) / (det_J*det_J) + (a1*a1 * temp_f) / (det_J*det_J*det_J);
			}
			else
			{
				g_a1 = b2 / 4.0 + a1 / (2.0 * det_J) - b2 / (4.0 * det_J*det_J) - (b2*mips_e) / (2.0*det_J);
				g_a2 = a2 / (2.0 * det_J) - b1 / 4.0 + b1 / (4.0 * det_J*det_J) + (b1*mips_e) / (2.0*det_J);
				g_b1 = a2 / (4.0 * det_J*det_J) - a2 / 4.0 + b1 / (2.0 * det_J) + (a2*mips_e) / (2.0*det_J);
				g_b2 = a1 / 4.0 - a1 / (4.0 * det_J*det_J) + b2 / (2.0 * det_J) - (a1*mips_e) / (2.0*det_J);

				h_a1_a1 = 1.0 / (2.0 * det_J) + b2*b2 / (2.0 * det_J*det_J*det_J) - (a1*b2) / (det_J*det_J) + (b2*b2 * mips_e) / (det_J*det_J);
				h_a1_a2 = (a1*b1) / (2.0 * det_J*det_J) - (a2*b2) / (2.0 * det_J*det_J) - (b1*b2) / (2.0 * det_J*det_J*det_J) - (b1*b2*mips_e) / (det_J*det_J);
				h_a1_b1 = (a1*a2) / (2.0 * det_J*det_J) - (a2*b2) / (2.0 * det_J*det_J*det_J) - (b1*b2) / (2.0 * det_J*det_J) - (a2*b2*mips_e) / (det_J*det_J);
				h_a1_b2 = (a1*b2) / (2.0 * det_J *det_J*det_J) - a1*a1 / (2.0 * det_J*det_J) - b2*b2 / (2.0 * det_J*det_J) - mips_e / (2.0 * det_J) - 1.0 / (4.0 * det_J*det_J) + (a1*b2*mips_e) / (det_J *det_J) + 1.0 / 4.0;

				h_a2_a2 = 1.0 / (2.0 * det_J) + b1 *b1 / (2.0 * det_J*det_J*det_J) + (a2*b1) / (det_J*det_J) + (b1*b1*mips_e) / (det_J*det_J);
				h_a2_b1 = 1.0 / (4.0 * det_J*det_J) + a2*a2 / (2.0 * det_J*det_J) + b1*b1 / (2.0 * det_J*det_J) + mips_e / (2.0 * det_J) + (a2*b1) / (2.0 * det_J *det_J*det_J) + (a2*b1*mips_e) / (det_J *det_J) - 1.0 / 4.0;
				h_a2_b2 = (b1*b2) / (2.0 * det_J*det_J) - (a1*b1) / (2.0 * det_J*det_J*det_J) - (a1*a2) / (2.0 * det_J*det_J) - (a1*b1*mips_e) / (det_J*det_J);

				h_b1_b1 = 1.0 / (2.0 * det_J) + a2*a2 / (2.0 * det_J*det_J*det_J) + (a2*b1) / (det_J*det_J) + (a2*a2 *mips_e) / (det_J*det_J);;
				h_b1_b2 = (a2*b2) / (2.0 * det_J*det_J) - (a1*b1) / (2.0 * det_J*det_J) - (a1*a2) / (2.0 * det_J*det_J*det_J) - (a1*a2*mips_e) / (det_J*det_J);
				h_b2_b2 = 1.0 / (2.0 * det_J) + a1*a1 / (2.0 * det_J*det_J*det_J) - (a1*b2) / (det_J*det_J) + (a1*a1 *mips_e) / (det_J*det_J);
			}

			if (!is_bounded)
			{
					double k = s*distortion_e;
					if (k > 50) k = 50;
					double exp_e = std::exp(k);
					current_dis_energy += exp_e;

					gradient(4 * face_id + 0) += exp_e*s*g_a1;
					gradient(4 * face_id + 1) += exp_e*s*g_a2;
					gradient(4 * face_id + 2) += exp_e*s*g_b1;
					gradient(4 * face_id + 3) += exp_e*s*g_b2;

					H.coeffRef(0, 0) = exp_e*s*h_a1_a1 + exp_e*s*s*g_a1*g_a1;
					H.coeffRef(0, 1) = exp_e*s*h_a1_a2 + exp_e*s*s*g_a1*g_a2;
					H.coeffRef(0, 2) = exp_e*s*h_a1_b1 + exp_e*s*s*g_a1*g_b1;
					H.coeffRef(0, 3) = exp_e*s*h_a1_b2 + exp_e*s*s*g_a1*g_b2;

					H.coeffRef(1, 1) = exp_e*s*h_a2_a2 + exp_e*s*s*g_a2*g_a2;
					H.coeffRef(1, 2) = exp_e*s*h_a2_b1 + exp_e*s*s*g_a2*g_b1;
					H.coeffRef(1, 3) = exp_e*s*h_a2_b2 + exp_e*s*s*g_a2*g_b2;

					H.coeffRef(2, 2) = exp_e*s*h_b1_b1 + exp_e*s*s*g_b1*g_b1;
					H.coeffRef(2, 3) = exp_e*s*h_b1_b2 + exp_e*s*s*g_b1*g_b2;

					H.coeffRef(3, 3) = exp_e*s*h_b2_b2 + exp_e*s*s*g_b2*g_b2;
			}
			else if (is_bounded && is_conformal) //only support conformal distortion and AMIPS
			{
				//if (distortion_e > K * 0.6)
				{
					double k = s*distortion_e;
					if (k > 50) k = 50;
					double exp_e = std::exp(k); double inv_K_d = 1.0 / (K - distortion_e);
					current_dis_energy += exp_e *inv_K_d;
					double inv_K_d_2 = inv_K_d*inv_K_d;
					double inv_K_d_3 = inv_K_d_2 * inv_K_d;

					double exp_g_a1 = exp_e*s*g_a1;
					double exp_g_a2 = exp_e*s*g_a2;
					double exp_g_b1 = exp_e*s*g_b1;
					double exp_g_b2 = exp_e*s*g_b2;

					gradient(4 * face_id + 0) += exp_g_a1*inv_K_d + exp_e*inv_K_d_2*g_a1;
					gradient(4 * face_id + 1) += exp_g_a2*inv_K_d + exp_e*inv_K_d_2*g_a2;
					gradient(4 * face_id + 2) += exp_g_b1*inv_K_d + exp_e*inv_K_d_2*g_b1;
					gradient(4 * face_id + 3) += exp_g_b2*inv_K_d + exp_e*inv_K_d_2*g_b2;

					double exp_h_a1_a1 = exp_e*s*h_a1_a1 + exp_e*s*s*g_a1*g_a1;
					double exp_h_a1_a2 = exp_e*s*h_a1_a2 + exp_e*s*s*g_a1*g_a2;
					double exp_h_a1_b1 = exp_e*s*h_a1_b1 + exp_e*s*s*g_a1*g_b1;
					double exp_h_a1_b2 = exp_e*s*h_a1_b2 + exp_e*s*s*g_a1*g_b2;

					double exp_h_a2_a2 = exp_e*s*h_a2_a2 + exp_e*s*s*g_a2*g_a2;
					double exp_h_a2_b1 = exp_e*s*h_a2_b1 + exp_e*s*s*g_a2*g_b1;
					double exp_h_a2_b2 = exp_e*s*h_a2_b2 + exp_e*s*s*g_a2*g_b2;

					double exp_h_b1_b1 = exp_e*s*h_b1_b1 + exp_e*s*s*g_b1*g_b1;
					double exp_h_b1_b2 = exp_e*s*h_b1_b2 + exp_e*s*s*g_b1*g_b2;

					double exp_h_b2_b2 = exp_e*s*h_b2_b2 + exp_e*s*s*g_b2*g_b2;

					H.coeffRef(0, 0) = exp_h_a1_a1*inv_K_d + exp_g_a1*g_a1*inv_K_d_2 + exp_g_a1*g_a1*inv_K_d_2 + exp_e*h_a1_a1*inv_K_d_2 + 2 * exp_e*g_a1*g_a1*inv_K_d_3;
					H.coeffRef(0, 1) = exp_h_a1_a2*inv_K_d + exp_g_a1*g_a2*inv_K_d_2 + exp_g_a2*g_a1*inv_K_d_2 + exp_e*h_a1_a2*inv_K_d_2 + 2 * exp_e*g_a1*g_a2*inv_K_d_3;
					H.coeffRef(0, 2) = exp_h_a1_b1*inv_K_d + exp_g_a1*g_b1*inv_K_d_2 + exp_g_b1*g_a1*inv_K_d_2 + exp_e*h_a1_b1*inv_K_d_2 + 2 * exp_e*g_a1*g_b1*inv_K_d_3;
					H.coeffRef(0, 3) = exp_h_a1_b2*inv_K_d + exp_g_a1*g_b2*inv_K_d_2 + exp_g_b2*g_a1*inv_K_d_2 + exp_e*h_a1_b2*inv_K_d_2 + 2 * exp_e*g_a1*g_b2*inv_K_d_3;

					H.coeffRef(1, 1) = exp_h_a2_a2*inv_K_d + exp_g_a2*g_a2*inv_K_d_2 + exp_g_a2*g_a2*inv_K_d_2 + exp_e*h_a2_a2*inv_K_d_2 + 2 * exp_e*g_a2*g_a2*inv_K_d_3;
					H.coeffRef(1, 2) = exp_h_a2_b1*inv_K_d + exp_g_a2*g_b1*inv_K_d_2 + exp_g_b1*g_a2*inv_K_d_2 + exp_e*h_a2_b1*inv_K_d_2 + 2 * exp_e*g_a2*g_b1*inv_K_d_3;
					H.coeffRef(1, 3) = exp_h_a2_b2*inv_K_d + exp_g_a2*g_b2*inv_K_d_2 + exp_g_b2*g_a2*inv_K_d_2 + exp_e*h_a2_b2*inv_K_d_2 + 2 * exp_e*g_a2*g_b2*inv_K_d_3;

					H.coeffRef(2, 2) = exp_h_b1_b1*inv_K_d + exp_g_b1*g_b1*inv_K_d_2 + exp_g_b1*g_b1*inv_K_d_2 + exp_e*h_b1_b1*inv_K_d_2 + 2 * exp_e*g_b1*g_b1*inv_K_d_3;
					H.coeffRef(2, 3) = exp_h_b1_b2*inv_K_d + exp_g_b1*g_b2*inv_K_d_2 + exp_g_b2*g_b1*inv_K_d_2 + exp_e*h_b1_b2*inv_K_d_2 + 2 * exp_e*g_b1*g_b2*inv_K_d_3;
					H.coeffRef(3, 3) = exp_h_b2_b2*inv_K_d + exp_g_b2*g_b2*inv_K_d_2 + exp_g_b2*g_b2*inv_K_d_2 + exp_e*h_b2_b2*inv_K_d_2 + 2 * exp_e*g_b2*g_b2*inv_K_d_3;
				}
				/*else
				{
				H.setZero();
				H.coeffRef(0, 0) = 1e-8; H.coeffRef(1, 1) = 1e-8; H.coeffRef(2, 2) = 1e-8; H.coeffRef(3, 3) = 1e-8;
				}*/
			}
			H.coeffRef(1, 0) = H.coeffRef(0, 1);
			H.coeffRef(2, 0) = H.coeffRef(0, 2);
			H.coeffRef(3, 0) = H.coeffRef(0, 3);
			H.coeffRef(2, 1) = H.coeffRef(1, 2);
			H.coeffRef(3, 1) = H.coeffRef(1, 3);
			H.coeffRef(3, 2) = H.coeffRef(2, 3);

			es.compute(H, Eigen::EigenvaluesOnly);
			double min_ev = es.eigenvalues()(0);
			if (min_ev < 1e-8)
			{
				min_ev = std::abs(min_ev) + 1e-8;
			}
			else //positive eigenvalues
			{
				min_ev = 0;
			}

			//min_ev = 0;
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					if (i == j)
					{
						hessian.coeffRef(4 * face_id + i, 4 * face_id + j) = H.coeffRef(i, j) + min_ev;
					}
					else
					{
						hessian.coeffRef(4 * face_id + i, 4 * face_id + j) = H.coeffRef(i, j);
					}
				}
			}
		}

		if (energy_type == 3)
		{
			OpenMesh::Vec4d& af = A_f[face_id];
			double derivation_e = (a1 - af[0])*(a1 - af[0]) + (a2 - af[1])*(a2 - af[1]) + (b1 - af[2]) * (b1 - af[2]) + (b2 - af[3])*(b2 - af[3]);
			current_em_energy += derivation_e;

			gradient(4 * face_id + 0) += mu * 2.0 *(a1 - af[0]);
			gradient(4 * face_id + 1) += mu * 2.0 *(a2 - af[1]);
			gradient(4 * face_id + 2) += mu * 2.0 *(b1 - af[2]);
			gradient(4 * face_id + 3) += mu * 2.0 *(b2 - af[3]);

			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 0) += 2 * mu;
			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 1) += 2 * mu;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 2) += 2 * mu;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 3) += 2 * mu;
		}
	}

	//if (!is_ivf_below_th)
	{
		hessian += 2.0*ivf_alpha*CTC;
	}

	return ivf_alpha*current_ivf_energy + current_dis_energy + mu*current_em_energy;
}
double assemble_triangle_interface::compute_only_energy_AT_no_translation(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, bool is_conformal)
{
	current_dis_energy = 0.0; double s = amips_s; current_em_energy = 0.0; //mu = 1e3;
	//for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	int nf = mesh_->n_faces(); double distortion_e; double K = (bound_k + 1 / bound_k) / 2.0;
	for (int face_id = 0; face_id < nf; ++face_id)
	{
		//int face_id = f_it.handle().idx();
		double a1 = x(4 * face_id + 0); double a2 = x(4 * face_id + 1);
		double b1 = x(4 * face_id + 2); double b2 = x(4 * face_id + 3);

		double det_J = a1*b2 - a2*b1;
		if (det_J < 1e-8) // min_det_j = 1e-8
		{
			return std::numeric_limits<double>::infinity();
		}

		double mips_e = 0.5*(a1*a1 + a2*a2 + b1*b1 + b2*b2) / det_J;
		if (is_conformal)
		{
			distortion_e = mips_e;
		}
		else
		{
			double area_e = 0.5*(det_J + 1.0 / det_J);
			distortion_e = 0.5*(mips_e + area_e);
		}

		//AMIPS
		{
			
		}

		if (!is_bounded)
		{
			if (energy_method == 0)//MIPS
			{
				current_dis_energy += distortion_e;
			}
			else//AMIPS
			{
				double k = s*distortion_e;
				if (k > 60) k = 60;
				double exp_e = std::exp(k);
				current_dis_energy += exp_e;
			}
		}
		else if (is_bounded && is_conformal)
		{
			if (distortion_e > K - 1e-8)
			{
				current_dis_energy = std::numeric_limits<double>::infinity();
				return std::numeric_limits<double>::infinity();
			}

			//if (distortion_e > K * 0.6)
			{
				double k = s*distortion_e;
				if (k > 60) k = 60;
				double exp_e = std::exp(k);
				current_dis_energy += exp_e / (K - distortion_e);
			}
		}


		if (energy_type == 3)
		{
			OpenMesh::Vec4d& af = A_f[face_id];
			double derivation_e = (a1 - af[0])*(a1 - af[0]) + (a2 - af[1])*(a2 - af[1]) + (b1 - af[2]) * (b1 - af[2]) + (b2 - af[3])*(b2 - af[3]);
			current_em_energy += derivation_e;
		}
	}

	//current_ivf_energy = std::abs(x.transpose()*CTC*x);
	current_ivf_energy = x.transpose()*CTC*x;
	current_ivf_energy -= 2.0*x.transpose()*CT*CB;
	current_ivf_energy = std::abs(current_ivf_energy + CBTCB);

	return ivf_alpha*current_ivf_energy + current_dis_energy + mu*current_em_energy;
}

void assemble_triangle_interface::optimize_AT_no_translation(Mesh* mesh_, bool is_conformal)
{
	int iter_count = 0; step_size = 1.0; min_ivf_alpha = 1e3;  double pre_ivf_alpha = min_ivf_alpha;
	int var_num = solution.size(); bool is_ivf_below_th = false; pre_current_dis_energy = 1;
	int nf = mesh_->n_faces(); bool update_sucess = false; bool update_hessian = true;
	int optimization_stop = 0; double pre_ivf_energy = 1.0; double g_norm = 0.0;
	//FILE* f_E_assembly = fopen("data\\Fixed_Mapping\\Plane\\Random.txt", "w");
	while (iter_count < max_iter && step_size > min_step_size)
	{
		double old_e = compute_energy_derivative_AT_no_translation(mesh_, solution, is_conformal);

		lltSolver.factorize(hessian);
		int status = lltSolver.info();
		if (status == Eigen::Success)
		{
			lambda = min_lambda;
		}
		else
		{
			int run = 0;
			while (status != Eigen::Success && lambda < max_lambda)
			{
				for (int i = 0; i < 4 * nf; i++)
				{
					hessian.coeffRef(i, i) += lambda;
				}
				for (int i = 4 * nf; i < var_num; ++i)
				{
					hessian.coeffRef(i, i) += min_lambda;
				}

				lltSolver.factorize(hessian);
				status = lltSolver.info();

				if (status != Eigen::Success)
				{
					if (lambda < max_lambda)
					{
						lambda *= 10;
					}
				}
				else
				{
					if (run == 0 && lambda > min_lambda)
					{
						lambda *= 0.1;
					}
				}
				run++;
			}
		}
		if (lambda < max_lambda)
		{
			// compute newton step direction
			step = lltSolver.solve(-gradient);
		}
		else
		{
			// gradient descent
			lambda *= 0.1;
			step = -gradient;
		}

		if (!update_hessian)
		{
			step = -gradient;
		}

		//g_norm = gradient.norm(); printf("norm: %e\n", g_norm);
		prevSolution = solution + step_size*step; update_sucess = true;
		double new_e = compute_only_energy_AT_no_translation(mesh_, prevSolution, is_conformal);
		if (new_e > old_e)
		{
			while (new_e > old_e)
			//while (new_e == std::numeric_limits<double>::infinity())
			{
				if (step_size < min_step_size)
				{
					update_sucess = false;
					break;
				}
				step_size *= 0.5;
				prevSolution = solution + step_size*step;
				new_e = compute_only_energy_AT_no_translation(mesh_, prevSolution, is_conformal);
			}
		}
		else
		{
			step_size *= 2.0;
		}
		++iter_count;
		//printf("%d A:%e,L:%3.2e,S:%4.3e,I:%4.3e,D:%4.3e,EM:%4.3e\n", iter_count, ivf_alpha, lambda, step_size, current_ivf_energy, current_dis_energy,current_em_energy);

		if (current_ivf_energy < 1e-12 && !is_ivf_below_th)
		{
			is_ivf_below_th = true;
		}
		else if (current_ivf_energy < 1e-12 && is_ivf_below_th)
		{
			if (std::abs(current_dis_energy - pre_current_dis_energy) / pre_current_dis_energy < 1e-8)
			{
				break;
			}
		}
		else
		{
			double re_dis = std::abs(current_dis_energy - pre_current_dis_energy) / pre_current_dis_energy;
			double re_ivf = std::abs(current_ivf_energy - pre_ivf_energy) / pre_ivf_energy;
			if (re_ivf < 0.01)
			{
				++optimization_stop;
			}
			else
			{
				optimization_stop = 0;
			}
		}

		if (update_sucess)
		{
			solution = prevSolution;
			pre_current_dis_energy = current_dis_energy; pre_ivf_energy = current_ivf_energy;
			//update_ivf_alpha();
		}

		if (step_size < 1e-12 || lambda > 1e10)
		{
			step_size = 1.0;
			if (lambda > 1e10) lambda = 1e10;
			printf("--------------------------------------\n");
			printf("reinitilize...............\n");
			min_ivf_alpha = 1e0;
			pre_ivf_alpha = 1e3;
			//if (current_dis_energy < 1e30) update_ivf_alpha();
		}
		else
		{
			if (optimization_stop > 1)
			{
				double temp_alpha = current_dis_energy * min_ivf_alpha / current_ivf_energy;
				if (temp_alpha > max_ivf_alpha)
				{
					//if (pre_ivf_alpha > 1) pre_ivf_alpha *= 0.1;
					if (max_ivf_alpha < 1e20) max_ivf_alpha *= 10;
				}
				else
				{
					if (pre_ivf_alpha < 1e7) pre_ivf_alpha *= 10;
					if (max_ivf_alpha > 1e16) max_ivf_alpha *= 0.1;
				}
				if (step_size < 1.0) step_size = 1.0;
				min_ivf_alpha = pre_ivf_alpha;
				optimization_stop = 0;
			}
			else
			{
				min_ivf_alpha = pre_ivf_alpha;
			}
		}

		if (update_sucess)
		{
			update_ivf_alpha();
			if (is_ivf_below_th && ivf_alpha < 1e16)
			{
				ivf_alpha = 1e16;
			}
		}

	}
	//fclose(f_E_assembly);
	printf("Iteration count : %d\n", iter_count);
}

#include "SPARSE\Sparse_Matrix.h"
#include "SPARSE\Sparse_Solver.h"
void assemble_triangle_interface::reconstruct_UV_no_translation(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x)
{
	unsigned nv = mesh_->n_vertices(); unsigned nf = mesh_->n_faces();

	//distortion
	double max_con_d = 0; double avg_con_d = 0;
	double max_iso_d = 0; double avg_iso_d = 0; int max_iso_d_face = -1;
	double max_area_d = 0; double avg_area_d = 0;
	Eigen::Matrix2d T; flipped_tri.clear();
	for (int face_id = 0; face_id < nf; ++face_id)
	{
		T << x(4 * face_id + 0), x(4 * face_id + 1), x(4 * face_id + 2), x(4 * face_id + 3);
		double det = T.determinant();
		if (det < 0)
		{
			flipped_tri.push_back(face_id);
		}
		else
		{
			Eigen::JacobiSVD<Eigen::Matrix2d> svd(T, Eigen::ComputeFullU | Eigen::ComputeFullV);
			Eigen::Vector2d s_v = svd.singularValues();

			double temp_error = s_v(0) > 1.0 / s_v(1) ? s_v(0) : 1.0 / s_v(1);
			if (temp_error > max_iso_d)
			{
				max_iso_d = temp_error; max_iso_d_face = face_id;
			}
			avg_iso_d += temp_error;
			temp_error = s_v(0) / s_v(1);

			if (temp_error > max_con_d)
			{
				max_con_d = temp_error;
			}
			avg_con_d += temp_error;

			double dJ = s_v(0)*s_v(1);
			temp_error = dJ > 1.0 / dJ ? dJ : 1.0 / dJ;
			if (temp_error > max_area_d)
			{
				max_area_d = temp_error;
			}
			avg_area_d += temp_error;
		}
	}

	avg_iso_d /= nf;
	avg_con_d /= nf;
	avg_area_d /= nf;

	printf("-----------------------------------------------------------\n");
	printf("Flip Count : %d;\n", flipped_tri.size());
	printf("Iso_D: %f/%f\n", max_iso_d, avg_iso_d);
	printf("Con_D: %f/%f\n", max_con_d, avg_con_d);
	printf("AREAD: %f/%f\n", max_area_d, avg_area_d);
	printf("-----------------------------------------------------------\n");

	int vertex_count_ok = 0; std::vector<int> map_with_vertex_UV(nv, -1);
	for (unsigned i = 0; i < nv; ++i)
	{
		if (mesh_->data(mesh_->vertex_handle(i)).get_new_pos_fixed())
		{
			continue;
		}
		else
		{
			map_with_vertex_UV[i] = vertex_count_ok;
			++vertex_count_ok;
		}
	}

	Sparse_Matrix A(2 * nf, vertex_count_ok, NOSYM, CCS, 2);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle vi = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& pi = mesh_->point(vi);
		double xi = pi[0]; double yi = pi[1];
		Mesh::VertexHandle vj = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pj = mesh_->point(vj);
		double xj = pj[0]; double yj = pj[1];
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle vk = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& pk = mesh_->point(vk);
		double xk = pk[0]; double yk = pk[1];
		double face_area = OpenMesh::cross(pj - pi, pk - pi).norm();
		double i_face_area = 1.0 / face_area;
		//i
		if (mesh_->data(vi).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vi).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yj - yk)*np[0]); 
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xk - xj)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yj - yk)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xk - xj)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vi.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yj - yk));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xk - xj));
		}

		//j
		if (mesh_->data(vj).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vj).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yk - yi)*np[0]); 
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xi - xk)*np[0]); 
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yk - yi)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xi - xk)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vj.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yk - yi));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xi - xk));
		}
		//k
		if (mesh_->data(vk).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vk).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yi - yj)*np[0]); 
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xj - xi)*np[0]); 
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yi - yj)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xj - xi)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vk.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yi - yj));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xj - xi));
		}

		A.fill_rhs_entry(face_id * 2 + 0, 0, x(4 * face_id + 0)); 
		A.fill_rhs_entry(face_id * 2 + 1, 0, x(4 * face_id + 1)); 

		A.fill_rhs_entry(face_id * 2 + 0, 1, x(4 * face_id + 2));
		A.fill_rhs_entry(face_id * 2 + 1, 1, x(4 * face_id + 3));
	}

	Sparse_Matrix* B = TransposeTimesSelf(&A, CCS, SYM_LOWER, true);
	solve_by_CHOLMOD(B);
	std::vector<double>& uv = B->get_solution();

	int vertex_id;
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		vertex_id = v_it.handle().idx();
		if (map_with_vertex_UV[vertex_id] >= 0)
		{
			mesh_->data(v_it).set_New_Pos(OpenMesh::Vec3d(uv[map_with_vertex_UV[vertex_id]], uv[map_with_vertex_UV[vertex_id] + vertex_count_ok], 0.0));
		}
	}
	delete B;
}

