#ifndef PARAMETERIZATION_H
#define PARAMETERIZATION_H

#include "mesh_definition.h"
class parameterization_interface
{
public:
	parameterization_interface();
	~parameterization_interface();

	//==============================================================================================
	//LSCM and LABF parameterization
	void LABF(Mesh* mesh_);
	void compute_circle_UV(Mesh* mesh_);
	//==============================================================================================
	//using simple untangling technique to eliminate most inverted elements, no guarantee 
	void untangling(Mesh* mesh_, int max_iter_num = 15);
	//==============================================================================================
	//using AMIPS technique to optimize, the input without inverted elements
	void AMIPS(Mesh* mesh_, int max_iter_num = 100, bool is_conformal = true);

	void prepare_data(Mesh* mesh_);

private:
	bool prepare_ok;
	//LSCM and LABF
	//void LSCM(Mesh* mesh_);
	//void setup_two_constraint_LSCM(Mesh* mesh_);
	void find_all_UV_using_angle(std::vector<double>& angle, Mesh* mesh_);
	void find_next_boundary_vertex(Mesh* mesh_, Mesh::VertexHandle& vh, Mesh::VertexHandle& vh0);

	std::vector<double> new_p_x; std::vector<double> new_p_y;
	std::vector<std::vector<int>> vertex_v1; std::vector<std::vector<int>> vertex_v2;
	std::vector<std::vector<double>> omega0; std::vector<std::vector<double>> omega1; std::vector<std::vector<double>> omega2;
	std::vector<std::vector<double>> face_area;
	std::vector<std::vector<double>> inv_face_area;
	std::vector<double> vertex_radius_ratio;
	std::vector<std::vector<double>> exp_vec_omp;
	std::vector<std::vector<int>> vertex_face_id; std::vector<std::vector<int>> face_vertex_id;
	int max_vv_size;
	
	double un_eps; double inv_un_eps; double amips_s; double c_v_ratio;
	std::vector<double> min_area;

	std::vector<std::vector<double>> p1_vec_x_omp; std::vector<std::vector<double>> p1_vec_y_omp;
	std::vector<std::vector<double>> p2_vec_x_omp; std::vector<std::vector<double>> p2_vec_y_omp;
	std::vector<std::vector<double>> l0_n_vec_omp;

	//graph coloring
	std::vector<OpenMesh::Vec2i> graph_color_edge;
	std::vector< std::vector<int> > v_same_color;

	void Untangling_one_V(int i, int omp_id);
	double find_negative_area(const int& vv_size, const double& npx, const double& npy,
		const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
		const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y);

	bool check_energy_untangling(const int& vv_size,
		const double& old_e, double& new_e,
		const double& npx, const double& npy,
		const std::vector<double>& fh_area_vec, const std::vector<double>& inv_fh_area_vec,
		const std::vector<double>& l0_n_vec,
		const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
		const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y,
		const std::vector<double>& omega1_vec, const std::vector<double>& omega2_vec);
	void AMIPS_one_V(int i, int omp_id);
	bool local_check_negative_area(const int& vv_size, const double& npx, const double& npy,
		const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
		const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y);
	bool check_energy_AMIPS(const int& vv_size,
		const double& old_e, double& new_e,
		const double& alpha, const double& beta,
		const double& npx, const double& npy,
		const std::vector<double>& fh_area_vec, const std::vector<double>& inv_fh_area_vec,
		const std::vector<double>& l0_n_vec,
		const std::vector<double>& p1_vec_x, const std::vector<double>& p1_vec_y,
		const std::vector<double>& p2_vec_x, const std::vector<double>& p2_vec_y,
		const std::vector<double>& omega1_vec, const std::vector<double>& omega2_vec,
		double* exp_vec);
};

#endif