#include "mesh_definition.h"
#include "assemble_simplex.h"
#include "assemble_simplex_ivf_speedup.h"
#include "Parameterization.h"
#include <iostream>
using std::cout;
using std::endl;

void print_usage()
{
	printf("===========================================================================\n");
	printf("type = c, conformal planar parameterization(speedup) with infinite bound: \nassemble_simplex type input_mesh_file\n");
	printf("type = c, conformal planar parameterization(speedup) with finite bound: \nassemble_simplex type input_mesh_file K\n");
	printf("===========================================================================\n");
	printf("type = e, optimal bound for conformal planar parameterization(speedup): \nassemble_simplex type input_mesh_file\n");
	printf("===========================================================================\n");
	printf("type = i, isometric planar parameterization: \nassemble_simplex type input_mesh_file\n");
	printf("===========================================================================\n");
	printf("type = g, global seamless parameterization: \nassemble_simplex type input_mesh_file frame edge_rotation\n");
	printf("===========================================================================\n");
	printf("type = f, planar deformation: \nassemble_simplex type input_mesh_file initial_uv handles_setting max_iter\n");
	printf("===========================================================================\n");
}

int main(int argc, char** argv)
{
	if (argc < 3)
	{
		print_usage();
		return 0;
	}

	Mesh mesh_;
	cout << "Reading mesh..." << endl;
	OpenMesh::IO::read_mesh(mesh_, argv[2]);
	cout << "Done: reading mesh." << endl;
	//int type = atoi(argv[1]);
	assemble_triangle_interface ati;
	assemble_triangle_speedup_interface atsi;
	if (argv[1][0] == 'c')
	{
		if (argc < 3 || argc > 5) { print_usage(); return 0; }
		parameterization_interface para; para.prepare_data(&mesh_);
		para.LABF(&mesh_);
		atsi.optimize_integrable_vector_field_speedup(&mesh_, 50, 50);
		if (argc == 4)
		{
			double k = atof(argv[3]);
			printf("Bounded Distortion Mapping: %f\n", k);
			atsi.optimize_integrable_vector_field_speedup(&mesh_, 50, k, true);
		}
		//para.AMIPS(&mesh_, 100, true);
		std::string uv(argv[2]);
		uv.append("_result.obj");
		atsi.save_result_uv(&mesh_, uv.c_str());
	}
	else if (argv[1][0] == 'e')
	{
		if (argc != 3) { print_usage(); return 0; }
		parameterization_interface para; para.prepare_data(&mesh_);
		para.LABF(&mesh_);
		atsi.optimize_integrable_vector_field_speedup(&mesh_, 50, 50);
		//para.AMIPS(&mesh_, 100, true);
		
		printf("---------------------------------------------------------------\n");
		printf("Optimal Bound Mapping: %f\n");
		atsi.optimize_integrable_vector_field_optimal_bound(&mesh_, 50);
		std::string uv(argv[2]);
		uv.append("_eqc_result.obj");
		atsi.save_result_uv(&mesh_, uv.c_str());
	}
	else if (argv[1][0] == 'i')
	{
		if (argc != 3) { print_usage(); return 0; }
		parameterization_interface para; para.prepare_data(&mesh_);
		para.compute_circle_UV(&mesh_);
		ati.optimize_integrable_vector_field(&mesh_, 50);
		//para.AMIPS(&mesh_, 50, false);
		std::string uv(argv[2]);
		uv.append("_result.obj");
		ati.save_result_uv(&mesh_, uv.c_str());
	}
	else if (argv[1][0] == 'g')
	{
		if (argc != 5) { print_usage(); return 0; }
		ati.optimize_ivf_gsp(&mesh_, argv[3], argv[4]);
		std::string uv(argv[2]);
		//uv.append("_result.obj");
		//ati.save_gsp_UV(&mesh_, uv.c_str());
		uv.append("_result.uv");
		ati.save_halfedge_uv(&mesh_, uv.c_str());
	}
	else if (argv[1][0] == 'f')
	{
		if (argc != 6) { print_usage(); return 0; }
		int max_iter = std::atoi(argv[5]);
		ati.optimize_affine_transformation(&mesh_, argv[3], argv[4], max_iter);
	}
	else
	{
		printf("Unsupported!!!!!!!!!!!!!!!\n");
	}

	return 0;
}