#include "graph_coloring.h"

void graph_coloring(int N, std::vector<OpenMesh::Vec2i>& graph_color_edge, std::vector< std::vector<int> >& v_same_color)
{
	//clock_t tt = clock();

	typedef adjacency_list<listS, vecS, undirectedS> Graph;
	typedef graph_traits<Graph>::vertex_descriptor vertex_descriptor;
	typedef graph_traits<Graph>::vertices_size_type vertices_size_type;
	typedef property_map<Graph, vertex_index_t>::const_type vertex_index_map;

	Graph g;
	//std::ifstream infile("graph.txt");
	//std::ofstream outfile("coloring.txt");
	//int N, vv_size, ij;
	//infile >> N;
	for (int i = 0; i < N; ++i)
	{
		add_vertex(g);
	}
	int edge_size = graph_color_edge.size();
	for (int i = 0; i < edge_size; i++)
	{
		add_edge(graph_color_edge[i][0], graph_color_edge[i][1], g);
	}

	boost::vector_property_map<vertex_descriptor> order;

	smallest_last_vertex_ordering(g, order);

	// Test with the normal order
	std::vector<vertices_size_type> color_vec(num_vertices(g));
	iterator_property_map<vertices_size_type*, vertex_index_map>
		color(&color_vec.front(), get(vertex_index, g));
	//vertices_size_type num_colors = sequential_vertex_coloring(g, color);
	vertices_size_type num_colors = sequential_vertex_coloring(g, order, color);

	//printf("\n===========\tFinished in %d ms!\n\n", clock() - tt);

	//std::cout << num_colors << std::endl;

	v_same_color.clear(); v_same_color.resize(num_colors);
	for (int i = 0; i < N; ++i)
	{
		v_same_color[color_vec[i]].push_back(i);// v_color[i] = color_vec[i] + 1;
	}
	graph_color_edge.clear();
}
