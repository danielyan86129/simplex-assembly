#ifndef ASSEMBLE_SIMPLEX_SPEEDUP_H
#define ASSEMBLE_SIMPLEX_SPEEDUP_H

#include "mesh_definition.h"
#include <Eigen/Sparse>
#include <Eigen/Dense>

class assemble_triangle_speedup_interface
{
public:
	assemble_triangle_speedup_interface();
	~assemble_triangle_speedup_interface();
	void reset();

	//==============================================================================================
	//planar parameterization(speedup), conformal
	void optimize_integrable_vector_field_speedup(Mesh* mesh_, int max_iter_ = 100, double bound_k_ = 10, bool is_bounded_ = false);
	//==============================================================================================
	void optimize_integrable_vector_field_optimal_bound(Mesh* mesh_, int max_iter_ = 100);
	//==============================================================================================
	void load_initial_uv(Mesh* mesh_, const char* filename);
	void load_another_uv_clac_distance(Mesh* mesh_, const char* filename);
	void save_result_uv(Mesh* mesh_, const char* filename);

private:
	std::vector<OpenMesh::Vec3d> e1_f;
	std::vector<OpenMesh::Vec3d> e2_f;
	std::vector<OpenMesh::Vec3d> b_f;
	std::vector<OpenMesh::Vec4d> A_f; //a1, a2, b1, b2
	std::vector<OpenMesh::Vec4d> A_f_0; //a1, a2, b1, b2
	std::vector<OpenMesh::Vec3d> edge_vec;

	std::vector<Eigen::SparseMatrix<double>> CTC_omp;
	std::vector<Eigen::SparseMatrix<double>> C_omp;
	std::vector<Eigen::SparseMatrix<double>> CT_omp;
	std::vector<Eigen::Matrix<double, Eigen::Dynamic, 1>> CB_omp;
	std::vector<double> CBTCB_omp;

	std::vector<Eigen::SparseMatrix<double>> hessian_omp;
	std::vector<Eigen::Matrix<double, Eigen::Dynamic, 1>> gradient_omp;
	std::vector<Eigen::Matrix<double, Eigen::Dynamic, 1>> prevSolution_omp;
	std::vector<Eigen::Matrix<double, Eigen::Dynamic, 1>> solution_omp;
	std::vector<Eigen::Matrix<double, Eigen::Dynamic, 1>> step_omp;
	Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, Eigen::Upper>* lltSolver_omp;

	std::vector<double> lambda_omp; double min_lambda; double max_lambda;
	std::vector<double> ivf_alpha_omp; std::vector<double> min_ivf_alpha_omp; double max_ivf_alpha;
	std::vector<double> step_size_omp; double min_step_size; double max_step_size;
	std::vector<double> current_ivf_energy_omp; std::vector<double> current_em_energy_omp;
	std::vector<double> current_dis_energy_omp; std::vector<double> pre_current_dis_energy_omp;
	int energy_method; double amips_s; double max_iter; int energy_type;
	std::vector<int> flipped_tri; int flipped_count; std::vector<int> is_flipped;
	std::vector<double> src_face_area; bool is_conformal;
	double bound_k; bool is_bounded; double mu; bool need_reinitialize; 
	double max_dis_k; int max_dis_id;

	void scale_by_average_edge_length(Mesh* mesh_);

	void initialize_one_matrix(Eigen::Matrix2d& A);
	void update_ivf_alpha(int omp_id);

	void initilize_vector_field_UV_0(Mesh* mesh_);
	void initilize_vector_field_UV(Mesh* mesh_);
	void select_block(Mesh* mesh_, int k_ring); //k_ring >= 1
	void select_one_block2(Mesh* mesh_, int seed_face, int k_ring, 
		std::vector<int>& block_tri_, int& block_num_, std::vector<OpenMesh::Vec6i>& block_edge_, std::vector<int>& boundary_tri, bool add_boundary_inverted = false); //k_ring >= 1
	
	void check_flip_by_A();
	void check_max_d_by_A();

	double compute_energy_derivative_modified_hessian(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x,
		int block_id, int omp_id);
	double compute_only_energy(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, 
		int block_id, int omp_id);
	bool optimize_IVF(Mesh* mesh_, int block_id, int omp_id);
	void reconstruct_UV(Mesh* mesh_);
	void reconstruct_UV_block(Mesh* mesh_);

	std::vector<std::vector<int>> all_block_tri;
	std::vector<int> all_block_num; int max_block_num;
	std::vector<std::vector<OpenMesh::Vec6i>> all_block_edge;
	std::vector<OpenMesh::Vec2i> graph_color_edge;
	std::vector< std::vector<int> > v_same_color;
	void optimize_ivf_one_block(Mesh* mesh_, int block_id,int omp_id);
	bool check_one_block(int block_id);
	bool initilize_vector_field_block(Mesh* mesh_, int block_id, int omp_id);
	bool reinitilize_vector_field_block(Mesh* mesh_, Eigen::Matrix<double, Eigen::Dynamic, 1>& x, int block_id);
	void assign_new_vector_field_block(Mesh* mesh_, int block_id, int omp_id);
};
#endif