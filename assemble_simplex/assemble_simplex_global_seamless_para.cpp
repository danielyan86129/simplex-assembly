#include "assemble_simplex.h"
#include <time.h>

void assemble_triangle_interface::load_frame(const char* filename, int nf)
{
	FILE* f_nor = fopen(filename, "r"); char buf[4096];
	e1_f.clear(); e1_f.resize(nf); e2_f.clear(); e2_f.resize(nf);
	U_f.clear(); U_f.resize(nf); V_f.clear(); V_f.resize(nf); A_f.resize(nf, OpenMesh::Vec4d(1, 0, 0, 1));
	char u[128]; char v[128]; char w[128]; int f_count = 0; char u1[128]; char v1[128]; char w1[128]; char fid[128];
	fgets(buf, 4096, f_nor);
	while (!feof(f_nor))
	{
		fgets(buf, 4096, f_nor);
		if (f_count < nf)
		{
			sscanf(buf, "%s %s %s %s %s %s %s", fid, u, v, w, u1, v1, w1);
			e1_f[f_count] = OpenMesh::Vec3d(atof(u), atof(v), atof(w)); e1_f[f_count].normalize();
			U_f[f_count] = e1_f[f_count];
			e2_f[f_count] = OpenMesh::Vec3d(atof(u1), atof(v1), atof(w1)); e2_f[f_count].normalize();
			V_f[f_count] = e2_f[f_count];
			++f_count;
		}
	}
	fclose(f_nor);
}

void assemble_triangle_interface::load_rotation(const char* filename, int nhe)
{
	FILE* f_rot = fopen(filename, "r"); char buf[4096];
	edge_rotation.clear(); edge_rotation.resize(nhe, 0.0);
	edge_cut_flag.clear(); edge_cut_flag.resize(nhe, 0);
	char r[128]; char sf[128]; int he_count = 0;
	while (!feof(f_rot))
	{
		fgets(buf, 4096, f_rot);
		if (he_count < nhe)
		{
			sscanf(buf, "%s %s", r, sf);
			edge_rotation[he_count] = atof(r);
			edge_cut_flag[he_count] = atoi(sf);
			++he_count;
		}
	}
	fclose(f_rot);
}

void assemble_triangle_interface::save_affine_transformation(Mesh* mesh_, const char* filename)
{
	FILE* f_affine = fopen(filename, "w");
	fprintf(f_affine, "4 %d\n", U_f.size());
	for (int i = 0; i < U_f.size(); ++i)
	{
		fprintf(f_affine, "%d %20.19f %20.19f %20.19f %20.19f %20.19f %20.19f\n", i, U_f[i][0], U_f[i][1], U_f[i][2], V_f[i][0], V_f[i][1], V_f[i][2]);
	}
	fclose(f_affine);
}

void assemble_triangle_interface::save_gsp_UV(Mesh* mesh_, const char* filename)
{
	FILE* f_obj = fopen(filename, "w");
	
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		const OpenMesh::Vec3d& p = mesh_->point(v_it);
		fprintf(f_obj, "v %20.19f %20.19f %20.19f\n",p[0],p[1],p[2]);
	}
	unsigned nf = mesh_->n_faces();
	for (int i = 0; i < nf; ++i)
	{
		fprintf(f_obj, "vt %20.19f %20.19f\n", face_uv[6 * i + 0], face_uv[6 * i + 1]);
		fprintf(f_obj, "vt %20.19f %20.19f\n", face_uv[6 * i + 2], face_uv[6 * i + 3]);
		fprintf(f_obj, "vt %20.19f %20.19f\n", face_uv[6 * i + 4], face_uv[6 * i + 5]);
	}
	Mesh::FaceIter f_it; int face_id;
	Mesh::FaceHalfedgeIter fhe_it;
	for (f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		fprintf(f_obj, "f"); face_id = f_it.handle().idx();
		fhe_it = mesh_->fh_iter(f_it); 
		int uv_id = mesh_->data(fhe_it).get_face_he_var() + 3 * face_id;
		int v_id = mesh_->to_vertex_handle(fhe_it).idx();
		fprintf(f_obj, " %d/%d", v_id + 1, uv_id + 1);
		++fhe_it;
		uv_id = mesh_->data(fhe_it).get_face_he_var() + 3 * face_id;
		v_id = mesh_->to_vertex_handle(fhe_it).idx();
		fprintf(f_obj, " %d/%d", v_id + 1, uv_id + 1);
		++fhe_it;
		uv_id = mesh_->data(fhe_it).get_face_he_var() + 3 * face_id;
		v_id = mesh_->to_vertex_handle(fhe_it).idx();
		fprintf(f_obj, " %d/%d", v_id + 1, uv_id + 1);
		fprintf(f_obj, "\n");
	}
	fclose(f_obj);
}

void assemble_triangle_interface::save_halfedge_uv(Mesh* mesh_, const char* filename)
{
	FILE* f_uv = fopen(filename, "w");
	for (Mesh::HalfedgeIter he_it = mesh_->halfedges_begin(); he_it != mesh_->halfedges_end(); ++he_it)
	{
		Mesh::FaceHandle fh = mesh_->face_handle(he_it);
		if (fh != Mesh::InvalidFaceHandle)
		{
			int face_id = fh.idx();
			double u = face_uv[face_id * 6 + 2 * mesh_->data(he_it).get_face_he_var() + 0];
			double v = face_uv[face_id * 6 + 2 * mesh_->data(he_it).get_face_he_var() + 1];
			fprintf(f_uv, "%d %20.19f %20.19f\n", he_it.handle().idx(), u, v);
		}
	}
	fclose(f_uv);
}

void assemble_triangle_interface::optimize_ivf_gsp(Mesh* mesh_, const char* filename_frame, const char* filename_rotation, int max_iter_)
{
	load_frame( filename_frame, mesh_->n_faces());
	load_rotation( filename_rotation, mesh_->n_halfedges());

	initilize_vector_field_gsp(mesh_);
	energy_method = 1; amips_s = 1; max_iter = max_iter_; energy_type = 1;
	long start_t = clock();

	optimize_IVF(mesh_);

	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	printf("Optimize IVF(GSP) Time: %f s\n", t);
	prepare_ok = false;

	assign_new_vector_field(mesh_, solution);

	//not save
	reconstruct_UV_gsp(mesh_);

	//only save frame
	std::string frame_(filename_frame);
	frame_.append("_result.frame");
	save_affine_transformation(mesh_, frame_.c_str());
}

bool assemble_triangle_interface::initilize_vector_field_gsp(Mesh* mesh_)
{
	unsigned nf = mesh_->n_faces(); b_f.clear(); b_f.resize(nf);
	if (e1_f.size() != nf || edge_rotation.size() != mesh_->n_halfedges()) return false;
	std::vector<Eigen::Triplet<double> > f_triplets; solution.resize(4 * nf);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		solution(4 * face_id + 0) = 1.0; solution(4 * face_id + 1) = 0.0;
		solution(4 * face_id + 2) = 0.0; solution(4 * face_id + 3) = 1.0;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				f_triplets.push_back(Eigen::Triplet<double>(4 * face_id + i, 4 * face_id + j, 1.0));
			}
		}
		Mesh::FaceVertexIter fv_it = mesh_->fv_iter(f_it);
		b_f[face_id] = mesh_->point(fv_it);
	}
	std::vector<Eigen::Triplet<double> > c_triplets; std::vector<Eigen::Triplet<double> > triplets;
	int constraint_count = 0;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		if (mesh_->is_boundary(e_it)) continue;

		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		const OpenMesh::Vec3d& p0 = mesh_->point(mesh_->to_vertex_handle(heh1));
		const OpenMesh::Vec3d& p1 = mesh_->point(mesh_->to_vertex_handle(heh0));
		OpenMesh::Vec3d e = (p1 - p0).normalize();

		Mesh::FaceHandle fh0 = mesh_->face_handle(heh0); int f0 = fh0.idx();
		Mesh::FaceHandle fh1 = mesh_->face_handle(heh1); int f1 = fh1.idx();

		double ee1_0 = OpenMesh::dot(e, e1_f[f0]); double ee2_0 = OpenMesh::dot(e, e2_f[f0]);
		double ee1_1 = OpenMesh::dot(e, e1_f[f1]); double ee2_1 = OpenMesh::dot(e, e2_f[f1]);

		int heh0_id = heh0.idx();
		if (std::abs(edge_rotation[heh0_id]) < 1e-6) // edge_rotation = 0;
		{
			//one edge, two equation
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, 1.0));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, 1.0));

			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, -ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, -ee2_1));

			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, -ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, -ee2_1));
		}
		else
		{
			double R0 = std::cos(M_PI_2 * edge_rotation[heh0_id]); double R1 = -std::sin(M_PI_2 * edge_rotation[heh0_id]);
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, R0*ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, R0*ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 2, R1*ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 3, R1*ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, -ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, -ee2_1));

			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 0, -R1*ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 1, -R1*ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, R0*ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, R0*ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, -ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, -ee2_1));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 1, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f0 + 3, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * f1 + 1, 1.0));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 1, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f0 + 3, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * f1 + 3, 1.0));
		}

		constraint_count += 2;
	}
	Eigen::SparseMatrix<double> C; C.resize(constraint_count, 4 * nf);
	C.setFromTriplets(c_triplets.begin(), c_triplets.end());
	CT = C.transpose();
	CTC.resize(4 * nf, 4 * nf);
	CTC = CT*C;
	Eigen::SparseMatrix<double> H;
	H.resize(4 * nf, 4 * nf);
	H.setFromTriplets(f_triplets.begin(), f_triplets.end());

	hessian.resize(4 * nf, 4 * nf);
	hessian = H + CTC;
	lltSolver.analyzePattern(hessian);
	C *= 0.0;
	C.setFromTriplets(triplets.begin(), triplets.end());
	CT = C.transpose();
	CTC = CT*C;
	CB.resize(constraint_count);
	for (int i = 0; i < constraint_count; ++i)
	{
		CB(i) = 0.0;
	}
	CBTCB = CB.transpose() * CB;

	gradient.resize(4 * nf); prevSolution.resize(4 * nf); step.resize(4 * nf);

	prepare_ok = true;
	return true;
}

#include "SPARSE\Sparse_Matrix.h"
#include "SPARSE\Sparse_Solver.h"
void assemble_triangle_interface::reconstruct_UV_gsp(Mesh* mesh_)
{
	double ivf_error = 0.0; double max_edge_ivf_error = 0.0; double avg_edge_ivf_error = 0.0; double edge_count = 0;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		if (mesh_->is_boundary(e_it)) continue;

		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		const OpenMesh::Vec3d& p0 = mesh_->point(mesh_->to_vertex_handle(heh1));
		const OpenMesh::Vec3d& p1 = mesh_->point(mesh_->to_vertex_handle(heh0));
		OpenMesh::Vec3d e = (p1 - p0);// .normalize();

		Mesh::FaceHandle fh0 = mesh_->face_handle(heh0); int f0 = fh0.idx();
		Mesh::FaceHandle fh1 = mesh_->face_handle(heh1); int f1 = fh1.idx();

		double ee1_0 = OpenMesh::dot(e, e1_f[f0]); double ee2_0 = OpenMesh::dot(e, e2_f[f0]);
		double ee1_1 = OpenMesh::dot(e, e1_f[f1]); double ee2_1 = OpenMesh::dot(e, e2_f[f1]);

		int heh0_id = heh0.idx();
		if (std::abs(edge_rotation[heh0_id]) < 1e-6) // edge_rotation = 0;
		{
			//one edge, two equation
			double err = A_f[f0][0] * ee1_0 + A_f[f0][1] * ee2_0 - A_f[f1][0] * ee1_1 - A_f[f1][1] * ee2_1;
			ivf_error += err*err; double temp_err = err*err;
			
			err = A_f[f0][2] * ee1_0 + A_f[f0][3] * ee2_0 - A_f[f1][2] * ee1_1 - A_f[f1][3] * ee2_1;
			ivf_error += err*err; temp_err += err*err;
			
			if (temp_err > max_edge_ivf_error) max_edge_ivf_error = temp_err;
			avg_edge_ivf_error += temp_err; edge_count += 1.0;
		}
		else
		{
			double R0 = std::cos(M_PI_2 * edge_rotation[heh0_id]); double R1 = -std::sin(M_PI_2 * edge_rotation[heh0_id]);
			double err = A_f[f0][0] * R0*ee1_0 + A_f[f0][1] * R0*ee2_0 + A_f[f0][2] * R1*ee1_0 + A_f[f0][3] * R1*ee2_0 - A_f[f1][0] * ee1_1 - A_f[f1][1] * ee2_1;
			ivf_error += err*err; double temp_err = err*err;
			err = -A_f[f0][0] * R1*ee1_0 - A_f[f0][1] * R1*ee2_0 + A_f[f0][2] * R0*ee1_0 + A_f[f0][3] * R0*ee2_0 - A_f[f1][2] * ee1_1 - A_f[f1][3] * ee2_1;
			ivf_error += err*err; temp_err += err*err;

			if (temp_err > max_edge_ivf_error) max_edge_ivf_error = temp_err;
			avg_edge_ivf_error += temp_err; edge_count += 1.0;
		}
	}
	avg_edge_ivf_error /= edge_count;
	//printf("IVF error : %e, %e/%e\n", ivf_error, max_edge_ivf_error, avg_edge_ivf_error);
	
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		mesh_->data(fhe_it).set_face_he_var(-1);
		++fhe_it;
		mesh_->data(fhe_it).set_face_he_var(-1);
		++fhe_it;
		mesh_->data(fhe_it).set_face_he_var(-1);
	}

	//variable
	int var_count = 0;
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		int edge_count = 0; std::vector<Mesh::HalfedgeHandle> edge_count_he;
		Mesh::VertexIHalfedgeIter vih_it = mesh_->vih_iter(v_it); 
		Mesh::HalfedgeHandle s_he = vih_it; Mesh::HalfedgeHandle e_he = vih_it;
		do 
		{
			int he_id = s_he.idx();
			if (edge_cut_flag[he_id] == 1 && !mesh_->is_boundary(s_he))
			{
				++edge_count;
				edge_count_he.push_back(s_he);
			}
			s_he = mesh_->opposite_halfedge_handle(mesh_->next_halfedge_handle(s_he));
		} while (e_he != s_he);

		if (edge_count < 2) // 0, 1
		{
			for (Mesh::VertexIHalfedgeIter vih_it = mesh_->vih_iter(v_it); vih_it; ++vih_it)
			{
				if (mesh_->data(vih_it).get_face_he_var() >= 0)
				{
					printf("Error %d\n", vih_it.handle().idx());
				}
				mesh_->data(vih_it).set_face_he_var(var_count);
			}
			++var_count;
		}
		else
		{
			for (int i = 0; i < edge_count ; ++i)
			{
				s_he = edge_count_he[i];
				while (s_he != edge_count_he[(i + 1) % edge_count])
				{
					if (mesh_->data(s_he).get_face_he_var() >= 0)
					{
						printf("Error %d\n", s_he.idx());
					}
					mesh_->data(s_he).set_face_he_var(var_count);
					s_he = mesh_->opposite_halfedge_handle(mesh_->next_halfedge_handle(s_he));
				}
				++var_count;
			}
		}
	}

	std::vector<int> v_fixed_flag(var_count, -1);
	std::vector<OpenMesh::Vec3d> final_uv(var_count);
	unsigned nf = mesh_->n_faces();
	//fix two points of face 0
	Mesh::FaceHandle fh = mesh_->face_handle(0);
	Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(fh);
	Mesh::VertexHandle v0 = mesh_->to_vertex_handle(fhe_it);
	const OpenMesh::Vec3d& p0 = mesh_->point(v0);
	int var_0 = mesh_->data(fhe_it).get_face_he_var(); v_fixed_flag[var_0] = 1;
	final_uv[var_0] = OpenMesh::Vec3d(0.0, 0.0, 0.0);
	
	++fhe_it;
	Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it);
	int var_1 = mesh_->data(fhe_it).get_face_he_var(); v_fixed_flag[var_1] = 1;
	const OpenMesh::Vec3d& p1 = mesh_->point(v1);
	double x1 = OpenMesh::dot(p1 - p0, e1_f[0]); double y1 = OpenMesh::dot(p1 - p0, e2_f[0]);
	final_uv[var_1] = OpenMesh::Vec3d(x1*A_f[0][0] + y1*A_f[0][1], x1*A_f[0][2] + y1*A_f[0][3], 0.0);

	int vertex_count_ok = 0;
	std::vector<int> map_with_vertex_UV(var_count, -1);
	for (unsigned i = 0; i < var_count; ++i)
	{
		if (v_fixed_flag[i] == 1)
		{
			continue;
		}
		else
		{
			map_with_vertex_UV[i] = vertex_count_ok;
			++vertex_count_ok;
		}
	}

	Sparse_Matrix A(2 * nf, vertex_count_ok, NOSYM, CCS, 2);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle vi = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pi = mesh_->point(vi);
		int vi_id = mesh_->data(fhe_it).get_face_he_var();
		double xi = 0; double yi = 0;

		++fhe_it;
		Mesh::VertexHandle vj = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pj = mesh_->point(vj);
		int vj_id = mesh_->data(fhe_it).get_face_he_var();
		double xj = OpenMesh::dot(pj - pi, e1_f[face_id]); double yj = OpenMesh::dot(pj - pi, e2_f[face_id]);

		++fhe_it;
		Mesh::VertexHandle vk = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pk = mesh_->point(vk);
		int vk_id = mesh_->data(fhe_it).get_face_he_var();
		double xk = OpenMesh::dot(pk - pi, e1_f[face_id]); double yk = OpenMesh::dot(pk - pi, e2_f[face_id]);

		double face_area = OpenMesh::cross(pj - pi, pk - pi).norm();
		double i_face_area = 1.0 / face_area;
		//i
		if (v_fixed_flag[vi_id] == 1)
		{
			OpenMesh::Vec3d np = final_uv[vi_id];
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yj - yk)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xk - xj)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yj - yk)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xk - xj)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vi_id];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yj - yk));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xk - xj));
		}

		//j
		if (v_fixed_flag[vj_id] == 1)
		{
			OpenMesh::Vec3d np = final_uv[vj_id];
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yk - yi)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xi - xk)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yk - yi)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xi - xk)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vj_id];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yk - yi));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xi - xk));
		}
		//k
		if (v_fixed_flag[vk_id] == 1)
		{
			OpenMesh::Vec3d np = final_uv[vk_id];
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yi - yj)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xj - xi)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yi - yj)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xj - xi)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vk_id];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yi - yj));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xj - xi));
		}

		A.fill_rhs_entry(face_id * 2 + 0, 0, A_f[face_id][0]);
		A.fill_rhs_entry(face_id * 2 + 1, 0, A_f[face_id][1]);

		A.fill_rhs_entry(face_id * 2 + 0, 1, A_f[face_id][2]);
		A.fill_rhs_entry(face_id * 2 + 1, 1, A_f[face_id][3]);
	}

	Sparse_Matrix* B = TransposeTimesSelf(&A, CCS, SYM_LOWER, true);
	solve_by_CHOLMOD(B);
	std::vector<double> uv = B->get_solution();
	for (unsigned i = 0; i < var_count; ++i)
	{
		int var_id = map_with_vertex_UV[i];
		if (var_id >= 0)
		{
			final_uv[i] = OpenMesh::Vec3d(uv[var_id], uv[var_id + vertex_count_ok], 0.0);
		}
	}
	delete B;

	face_uv.resize(6 * nf);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		face_uv[6 * face_id + 0] = final_uv[mesh_->data(fhe_it).get_face_he_var()][0];
		face_uv[6 * face_id + 1] = final_uv[mesh_->data(fhe_it).get_face_he_var()][1];
		++fhe_it;
		face_uv[6 * face_id + 2] = final_uv[mesh_->data(fhe_it).get_face_he_var()][0];
		face_uv[6 * face_id + 3] = final_uv[mesh_->data(fhe_it).get_face_he_var()][1];
		++fhe_it;
		face_uv[6 * face_id + 4] = final_uv[mesh_->data(fhe_it).get_face_he_var()][0];
		face_uv[6 * face_id + 5] = final_uv[mesh_->data(fhe_it).get_face_he_var()][1];
	}

	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		mesh_->data(fhe_it).set_face_he_var(0);
		++fhe_it;
		mesh_->data(fhe_it).set_face_he_var(1);
		++fhe_it;
		mesh_->data(fhe_it).set_face_he_var(2);
	}
	compute_distortion_gsp(mesh_);

	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		U_f[face_id] = A_f[face_id][0] * e1_f[face_id] + A_f[face_id][1] * e2_f[face_id];
		V_f[face_id] = A_f[face_id][2] * e1_f[face_id] + A_f[face_id][3] * e2_f[face_id];
	}
}

void assemble_triangle_interface::compute_distortion_gsp(Mesh* mesh_)
{
	flipped_tri.clear();
	std::vector<double> isometric_d(mesh_->n_faces(), -1);
	std::vector<double> conformal_d(mesh_->n_faces(), -1);
	std::vector<double> area_d(mesh_->n_faces(), -1);
	double max_con_d = 0; double avg_con_d = 0; double min_con_d = 1e30;
	double max_iso_d = 0; double avg_iso_d = 0; double min_iso_d = 1e30;
	double max_area_d = 0; double avg_area_d = 0; double min_area_d = 1e30;
	A_f.clear(); A_f.resize(mesh_->n_faces(), OpenMesh::Vec4d(1, 0, 0, 1));
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle vi = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pi = mesh_->point(vi);
		double Ui = face_uv[6 * face_id + 2 * mesh_->data(fhe_it).get_face_he_var() + 0];
		double Vi = face_uv[6 * face_id + 2 * mesh_->data(fhe_it).get_face_he_var() + 1];
		double xi = 0; double yi = 0;

		++fhe_it;
		Mesh::VertexHandle vj = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pj = mesh_->point(vj);
		double Uj = face_uv[6 * face_id + 2 * mesh_->data(fhe_it).get_face_he_var() + 0];
		double Vj = face_uv[6 * face_id + 2 * mesh_->data(fhe_it).get_face_he_var() + 1];
		double xj = OpenMesh::dot(pj - pi, e1_f[face_id]); double yj = OpenMesh::dot(pj - pi, e2_f[face_id]);

		++fhe_it;
		Mesh::VertexHandle vk = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pk = mesh_->point(vk);
		double Uk = face_uv[6 * face_id + 2 * mesh_->data(fhe_it).get_face_he_var() + 0];
		double Vk = face_uv[6 * face_id + 2 * mesh_->data(fhe_it).get_face_he_var() + 1];
		double xk = OpenMesh::dot(pk - pi, e1_f[face_id]); double yk = OpenMesh::dot(pk - pi, e2_f[face_id]);

		//from xy to uv
		Eigen::Matrix2d XY, UV, A;
		XY << xj, xk, yj, yk;
		UV << Uj - Ui, Uk - Ui, Vj - Vi, Vk - Vi;
		A = UV*XY.inverse();
		A_f[face_id][0] = A(0, 0); A_f[face_id][1] = A(0, 1); A_f[face_id][2] = A(1, 0); A_f[face_id][3] = A(1, 1);
		double det = A.determinant();
		if (det < 0)
		{
			flipped_tri.push_back(face_id);
		}
		else
		{
			Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
			Eigen::Matrix2d U = svd.matrixU(); Eigen::Matrix2d V = svd.matrixV();
			Eigen::Vector2d s_v = svd.singularValues();

			double temp_error = s_v(0) > 1.0 / s_v(1) ? s_v(0) : 1.0 / s_v(1);
			isometric_d[face_id] = temp_error;
			if (temp_error > max_iso_d)
			{
				max_iso_d = temp_error;
			}
			if (temp_error < min_iso_d)
			{
				min_iso_d = temp_error;
			}
			avg_iso_d += temp_error;

			temp_error = s_v(0) / s_v(1);
			conformal_d[face_id] = temp_error;
			if (temp_error > max_con_d)
			{
				max_con_d = temp_error;
			}
			if (temp_error < min_con_d)
			{
				min_con_d = temp_error;
			}
			avg_con_d += temp_error;

			double dJ = s_v(0)*s_v(1);
			temp_error = dJ > 1.0 / dJ ? dJ : 1.0 / dJ;
			area_d[face_id] = temp_error;
			if (temp_error > max_area_d)
			{
				max_area_d = temp_error;
			}
			if (temp_error < min_area_d)
			{
				min_area_d = temp_error;
			}
			avg_area_d += temp_error;
		}
	}

	avg_iso_d /= mesh_->n_faces();
	avg_con_d /= mesh_->n_faces();
	avg_area_d /= mesh_->n_faces();

	double std_iso = 0.0; double std_con = 0.0; double std_area = 0.0; int nf_count = 0;
	for (int i = 0; i < isometric_d.size(); ++i)
	{
		if (isometric_d[i] > 0)
		{
			std_iso += (isometric_d[i] - avg_iso_d)*(isometric_d[i] - avg_iso_d);
			++nf_count;
		}

		if (conformal_d[i] > 0)
		{
			std_con += (conformal_d[i] - avg_con_d)*(conformal_d[i] - avg_con_d);
		}

		if (area_d[i] > 0)
		{
			std_area += (area_d[i] - avg_area_d)*(area_d[i] - avg_area_d);
		}
	}
	std_iso = std::sqrt(std_iso / (nf_count - 1));
	std_con = std::sqrt(std_con / (nf_count - 1));
	std_area = std::sqrt(std_area / (nf_count - 1));

	printf("-----------------------------------------------------------\n");
	printf("Flip Count : %d;\n", flipped_tri.size());
	printf("Iso_D: %f/%f/%f/%f\n", max_iso_d, min_iso_d, avg_iso_d, std_iso);
	printf("Con_D: %f/%f/%f/%f\n", max_con_d, min_con_d, avg_con_d, std_con);
	printf("AREAD: %f/%f/%f/%f\n", max_area_d, min_area_d, avg_area_d, std_area);
	printf("-----------------------------------------------------------\n");
}
