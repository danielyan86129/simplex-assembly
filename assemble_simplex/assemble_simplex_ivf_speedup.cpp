#include "assemble_simplex_ivf_speedup.h"

assemble_triangle_speedup_interface::assemble_triangle_speedup_interface()
{
	lltSolver_omp = NULL;
	reset();
}

assemble_triangle_speedup_interface::~assemble_triangle_speedup_interface()
{
	if (lltSolver_omp)
		delete [] lltSolver_omp;
}

void assemble_triangle_speedup_interface::reset()
{
	e1_f.clear(); e2_f.clear(); A_f.clear();

	int omp_max_t = omp_get_max_threads();

	lambda_omp.clear(); lambda_omp.resize(omp_max_t, 1e-1); 
	min_lambda = 1e-5; max_lambda= 1e16;
	ivf_alpha_omp.clear(); ivf_alpha_omp.resize(omp_max_t ,1e5);
	min_ivf_alpha_omp.clear(); min_ivf_alpha_omp.resize(omp_max_t, 1e3); max_ivf_alpha = 1e16;
	step_size_omp.clear(); step_size_omp.resize(omp_max_t, 1.0);
	min_step_size = 1.0e-16; max_step_size = 1.0;
	current_ivf_energy_omp.resize(omp_max_t); current_em_energy_omp.resize(omp_max_t);
	current_dis_energy_omp.resize(omp_max_t); pre_current_dis_energy_omp.resize(omp_max_t);

	amips_s = 2.0; max_iter = 50; energy_type = 0; is_conformal = true; energy_method = 1; mu = 1e3;

	hessian_omp.resize(omp_max_t); gradient_omp.resize(omp_max_t); prevSolution_omp.resize(omp_max_t);
	solution_omp.resize(omp_max_t); step_omp.resize(omp_max_t); //lltSolver_omp.resize(omp_max_t);
	CTC_omp.resize(omp_max_t); C_omp.resize(omp_max_t); CT_omp.resize(omp_max_t); 
	CB_omp.resize(omp_max_t); CBTCB_omp.resize(omp_max_t);
	lltSolver_omp = new Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, Eigen::Upper>[omp_max_t];
}

void assemble_triangle_speedup_interface::load_initial_uv(Mesh* mesh_, const char* filename)
{
	FILE* f_uv = fopen(filename, "r");
	int n_uv = mesh_->n_vertices(); char buf[4096];
	OpenMesh::Vec3d np(0, 0, 0);
	char u[128]; char v[128]; char w[128]; int v_count = 0;
	while (!feof(f_uv))
	{
		fgets(buf, 4096, f_uv);
		if (v_count < n_uv)
		{
			sscanf(buf, "%s %s %s", u, v, w);
			np[0] = atof(u); np[1] = atof(v); np[2] = 0.0;
			Mesh::VertexHandle vh = mesh_->vertex_handle(v_count);
			mesh_->data(vh).set_New_Pos(np);
			++v_count;
		}
	}
	fclose(f_uv);

	initilize_vector_field_UV_0(mesh_);

}

void assemble_triangle_speedup_interface::scale_by_average_edge_length(Mesh* mesh_)
{
	double s = 0.0;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		Mesh::HalfedgeHandle heh = mesh_->halfedge_handle(e_it, 0);
		Mesh::VertexHandle vh0 = mesh_->from_vertex_handle(heh);
		Mesh::VertexHandle vh1 = mesh_->to_vertex_handle(heh);

		OpenMesh::Vec3d p0 = mesh_->point(vh0); OpenMesh::Vec3d p0_ = mesh_->data(vh0).get_New_Pos();
		OpenMesh::Vec3d p1 = mesh_->point(vh1); OpenMesh::Vec3d p1_ = mesh_->data(vh1).get_New_Pos();

		s += (p0 - p1).norm() / (p0_ - p1_).norm();
	}
	s /= mesh_->n_edges();
	printf("Scale : %e\n", s);
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		OpenMesh::Vec3d& p = mesh_->data(v_it).get_New_Pos();
		p *= s;
		mesh_->data(v_it).set_New_Pos(p);
	}
}

void assemble_triangle_speedup_interface::load_another_uv_clac_distance(Mesh* mesh_, const char* filename)
{
	initilize_vector_field_UV(mesh_);
	FILE* f_uv = fopen(filename, "r");
	int n_uv = mesh_->n_vertices(); char buf[4096];
	std::vector<OpenMesh::Vec3d> new_pos(n_uv);
	OpenMesh::Vec3d np(0, 0, 0);
	char u[128]; char v[128]; char w[128]; int v_count = 0;
	while (!feof(f_uv))
	{
		fgets(buf, 4096, f_uv);
		if (v_count < n_uv)
		{
			sscanf(buf, "%s %s %s", u, v, w);
			np[0] = atof(u); np[1] = atof(v); np[2] = 0.0;
			new_pos[v_count] = np;
			++v_count;
		}
	}
	fclose(f_uv);

	//
	double distance = 0.0; double distance2 = 0.0;
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0); src_face_area[face_id] = n.norm() * 0.5;
		n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);

		e1_f[face_id] = e1; e2_f[face_id] = e2;

		const OpenMesh::Vec3d& q0 = new_pos[v0.idx()];
		const OpenMesh::Vec3d& q1 = new_pos[v1.idx()];
		const OpenMesh::Vec3d& q2 = new_pos[v2.idx()];

		//affine matrix
		Eigen::Matrix2d XY, UV, A, B;
		XY << l01, x2, 0, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse(); B << A_f[face_id][0], A_f[face_id][1], A_f[face_id][2], A_f[face_id][3];
		distance += src_face_area[face_id] * (A - B).squaredNorm();
		distance2 += (A - B).squaredNorm();
	}
	printf("Mapping Distance : %e(with area); %e(withoout area)\n", distance, distance2);
}

void assemble_triangle_speedup_interface::initilize_vector_field_UV_0(Mesh* mesh_)
{
	unsigned nf = mesh_->n_faces(); int flipped_count1 = 0;
	e1_f.resize(nf); e2_f.resize(nf); A_f_0.resize(nf); src_face_area.resize(nf, 0);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0); src_face_area[face_id] = n.norm() * 0.5;
		n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);

		e1_f[face_id] = e1; e2_f[face_id] = e2;

		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix
		Eigen::Matrix2d XY, UV, A;
		XY << l01, x2, 0, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse();
		A_f_0[face_id][0] = A(0, 0); A_f_0[face_id][1] = A(0, 1); A_f_0[face_id][2] = A(1, 0); A_f_0[face_id][3] = A(1, 1);
		if (A.determinant() < 0)
		{
			++flipped_count1;
		}
	}
	//printf("Flipped Triangles: %d\n", flipped_count1);
}

void assemble_triangle_speedup_interface::initilize_vector_field_UV(Mesh* mesh_)
{
	unsigned nf = mesh_->n_faces(); flipped_tri.clear(); //high_distortion_tri.clear();
	is_flipped.clear(); is_flipped.resize(nf, -1);
	A_f.resize(nf); e1_f.resize(nf); e2_f.resize(nf); src_face_area.resize(nf, 0);
	max_dis_k = 0; max_dis_id = -1;
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0); src_face_area[face_id] = n.norm() * 0.5;
		n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);
		e1_f[face_id] = e1; e2_f[face_id] = e2;

		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix
		Eigen::Matrix2d XY, UV, A;
		XY << l01, x2, 0, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse();
	
		A_f[face_id][0] = A(0, 0); A_f[face_id][1] = A(0, 1); A_f[face_id][2] = A(1, 0); A_f[face_id][3] = A(1, 1);
		if (A.determinant() < 0)
		{
			flipped_tri.push_back(face_id);
			is_flipped[face_id] = 1;
		}
		else
		{
			if (bound_k > 1)
			{
				Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
				Eigen::Vector2d s_v = svd.singularValues();
				if (is_conformal)
				{
					double temp_error = s_v(0) / s_v(1);
					if (temp_error > max_dis_k)
					{
						max_dis_k = temp_error; max_dis_id = face_id;
					}
					if (temp_error > bound_k)
					{
						flipped_tri.push_back(face_id);
						is_flipped[face_id] = 1;
					}
				}
				else
				{
					double temp_error = s_v(0) > 1.0 / s_v(1) ? s_v(0) : 1.0 / s_v(1);
					if (temp_error > bound_k)
					{
						flipped_tri.push_back(face_id);
						is_flipped[face_id] = 1;
					}
				}
			}
		}
	}
	flipped_count = flipped_tri.size();
	//printf("Flipped Triangles: %d; Max D %f\n", flipped_count, max_dis_k);

	edge_vec.clear(); edge_vec.resize(mesh_->n_edges());
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		const OpenMesh::Vec3d& p0 = mesh_->point(mesh_->to_vertex_handle(heh1));
		const OpenMesh::Vec3d& p1 = mesh_->point(mesh_->to_vertex_handle(heh0));
		edge_vec[e_it->idx()] = (p1 - p0).normalize();
	}
}

void assemble_triangle_speedup_interface::save_result_uv(Mesh* mesh_, const char* filename)
{
	double min_con = 1e30; double min_iso = 1e30; double min_vol = 1e30;
	double max_con = 0; double max_iso = 0; double max_vol = 0;
	double avg_con = 0; double avg_iso = 0; double avg_vol = 0;
	int flipped_count = 0;
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0);
		n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);

		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix
		Eigen::Matrix2d XY, UV, A;
		XY << l01, x2, 0, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse();
		if (A.determinant() < 0)
		{
			++flipped_count;
		}
		else
		{
			Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
			Eigen::Vector2d s_v = svd.singularValues();
			double temp_error = s_v(0) / s_v(1);
			avg_con += temp_error;
			if (temp_error < min_con) min_con = temp_error;
			if (temp_error > max_con) max_con = temp_error;

			temp_error = s_v(0) > 1.0 / s_v(1) ? s_v(0) : 1.0 / s_v(1);
			avg_iso += temp_error;
			if (temp_error < min_iso) min_iso = temp_error;
			if (temp_error > max_iso) max_iso = temp_error;

			double dJ = s_v(0)*s_v(1);
			temp_error = dJ > 1.0 / dJ ? dJ : 1.0 / dJ;
			avg_vol += temp_error;
			if (temp_error < min_vol) min_vol = temp_error;
			if (temp_error > max_vol) max_vol = temp_error;
		}
	}
	avg_iso /= mesh_->n_faces(); avg_con /= mesh_->n_faces(); avg_vol /= mesh_->n_faces();
	printf("--------------------------------------------------------------\n");
	printf("Flipped Triangles: %d; Max/Min/Avg\n", flipped_count);
	printf("Isometric: %f/%f/%f\n", max_iso, min_iso, avg_iso);
	printf("Conformal: %f/%f/%f\n", max_con, min_con, avg_con);
	printf("Volumetric: %f/%f/%f\n", max_vol, min_vol, avg_vol);

	//FILE* f_uv = fopen(filename, "w");
	//for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	//{
	//	OpenMesh::Vec3d& np = mesh_->data(v_it).get_New_Pos();
	//	fprintf(f_uv, "%20.19f %20.19f\n", np[0], np[1]); //just 2D uv
	//}
	//fclose(f_uv);

	FILE* f_uv = fopen(filename, "w");
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		OpenMesh::Vec3d& np = mesh_->data(v_it).get_New_Pos();
		fprintf(f_uv, "v %20.19f %20.19f 0.0\n", np[0], np[1]); //just 2D uv
	}
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		fprintf(f_uv, "f");
		Mesh::FaceVertexIter fv_it = mesh_->fv_iter(f_it);
		fprintf(f_uv, " %d", fv_it->idx() + 1);
		++fv_it;
		fprintf(f_uv, " %d", fv_it->idx() + 1);
		++fv_it;
		fprintf(f_uv, " %d\n", fv_it->idx() + 1);
	}
	fclose(f_uv);
}

void assemble_triangle_speedup_interface::update_ivf_alpha(int omp_id)
{
	double s_ivf_alpha = ivf_alpha_omp[omp_id];

	ivf_alpha_omp[omp_id] = current_dis_energy_omp[omp_id] * min_ivf_alpha_omp[omp_id] / current_ivf_energy_omp[omp_id];
	if (ivf_alpha_omp[omp_id] > max_ivf_alpha) ivf_alpha_omp[omp_id] = max_ivf_alpha;
	if (ivf_alpha_omp[omp_id] < min_ivf_alpha_omp[omp_id]) ivf_alpha_omp[omp_id] = min_ivf_alpha_omp[omp_id];
}

void assemble_triangle_speedup_interface::initialize_one_matrix(Eigen::Matrix2d& A)
{
	Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
	Eigen::Matrix2d U = svd.matrixU(); Eigen::Matrix2d V = svd.matrixV();
	Eigen::Matrix2d rot = V*U.transpose();
	Eigen::Vector2d sv = svd.singularValues();
	double det = rot.determinant();
	if (det < 0)
	{
		if (sv(0) < sv(1))
		{
			U(0, 0) = -U(0, 0); U(1, 0) = -U(1, 0);
			sv(0) = -sv(0);
		}
		else
		{
			U(0, 1) = -U(0, 1); U(1, 1) = -U(1, 1);
			sv(1) = -sv(1);
		}
		double temp_s = 1;
		if (is_conformal)
		{
			temp_s = 0.5*(sv(0) + sv(1)) + 1e-8;
		}
		A = V*U.transpose()*temp_s;
	}
	else
	{
		if (is_conformal)
		{
			double temp_s = 0.5*(std::abs(sv(0)) + std::abs(sv(1)));
			A = temp_s* V*U.transpose();
		}
		else
		{
			A = rot;
		}
	}
}

void assemble_triangle_speedup_interface::select_block(Mesh* mesh_, int k_ring)
{
	int flipped_size = flipped_tri.size();
	all_block_tri.clear(); all_block_edge.clear(); all_block_num.clear();
	all_block_tri.resize(flipped_size); all_block_edge.resize(flipped_size); all_block_num.resize(flipped_size, 0);
	//select block and build connectivity
	graph_color_edge.clear();
	int nf = mesh_->n_faces(); std::vector<int> boundary_tri;
	std::vector<std::vector<int>> face_block_id(nf);
	std::vector<int> connect_v(flipped_size, -1); 
	max_block_num = 0; int max_block_edge_num = 0;
	bool add_b_i = k_ring > 8 ? false : true;
	for (int i = 0; i < flipped_size; ++i)
	{
		std::vector<int>& block_i = all_block_tri[i]; block_i.reserve(nf);
		int& num_i = all_block_num[i];
		std::vector<OpenMesh::Vec6i>& edge_i = all_block_edge[i];

		boundary_tri.clear();
		select_one_block2(mesh_, flipped_tri[i], k_ring, block_i, num_i, edge_i, boundary_tri, add_b_i);

		for (int j = 0; j < block_i.size(); ++j)
		{
			int face_id = block_i[j];
			int face_block_id_size = face_block_id[face_id].size();
			for (int k = 0; k < face_block_id_size; ++k)
			{
				connect_v[face_block_id[face_id][k]] = 1;
			}
			face_block_id[face_id].push_back(i);
		}

		for (int j = 0; j < boundary_tri.size(); ++j)
		{
			int face_id = boundary_tri[j];
			int face_block_id_size = face_block_id[face_id].size();
			for (int k = 0; k < face_block_id_size; ++k)
			{
				connect_v[face_block_id[face_id][k]] = 1;
			}
		}

		for (int j = 0; j < i; ++j)
		{
			if (connect_v[j] == 1)
			{
				graph_color_edge.push_back(OpenMesh::Vec2i(j, i));
				connect_v[j] == -1;
			}
		}

		if (num_i > max_block_num) max_block_num = num_i;
		if (edge_i.size() > max_block_edge_num) max_block_edge_num = edge_i.size();
	}

	int omp_max_t = omp_get_max_threads();
	int max_constraint_count = 2 * max_block_edge_num;
	//printf("%d %d\n", max_block_num, max_constraint_count);
	for (int i = 0; i < omp_max_t; ++i)
	{
		solution_omp[i].resize(4 * max_block_num);
		C_omp[i].resize(max_constraint_count, 4 * max_block_num);
		CT_omp[i].resize(4 * max_block_num, max_constraint_count);
		CTC_omp[i].resize(4 * max_block_num, 4 * max_block_num);
		hessian_omp[i].resize(4 * max_block_num, 4 * max_block_num);
		CB_omp[i].resize(max_constraint_count);
		gradient_omp[i].resize(4 * max_block_num);
		prevSolution_omp[i].resize(4 * max_block_num); 
		step_omp[i].resize(4 * max_block_num);
	}
}

void assemble_triangle_speedup_interface::select_one_block2(Mesh* mesh_, int seed_face, int k_ring,
	std::vector<int>& block_tri_, int& block_num_, std::vector<OpenMesh::Vec6i>& block_edge_, std::vector<int>& boundary_tri, bool add_boundary_inverted)
{
	int nf = mesh_->n_faces();
	std::vector<int> map_tri(nf, -1);
	block_tri_.clear(); block_tri_.reserve(nf); block_tri_.push_back(seed_face);
	int start_id = 0; map_tri[seed_face] = 0; int current_tri_size = -1; block_num_ = 1;
	for (int i = 0; i < k_ring; ++i)
	{
		current_tri_size = block_tri_.size();
		for (int j = start_id; j < current_tri_size; ++j)
		{
			for (Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(mesh_->face_handle(block_tri_[j])); fhe_it; ++fhe_it)
			{
				int edge_id = mesh_->edge_handle(fhe_it).idx();
				Mesh::FaceHandle fh = mesh_->face_handle(mesh_->opposite_halfedge_handle(fhe_it));
				int face_id = fh.idx();

				if (face_id != -1 && map_tri[face_id] == -1)
				{
					block_tri_.push_back(face_id); map_tri[face_id] = block_num_; ++block_num_;
				}
			}
		}
		start_id = current_tri_size;
	}

	//check boundary cell with fold-over
	if (add_boundary_inverted)
	{
		while (1)
		{
			int add_count = 0;
			for (int j = 0; j < flipped_tri.size(); ++j)
			{
				if (map_tri[flipped_tri[j]] == -1) //in the block
				{
					for (Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(mesh_->face_handle(flipped_tri[j])); fhe_it; ++fhe_it)
					{
						Mesh::FaceHandle fh = mesh_->face_handle(mesh_->opposite_halfedge_handle(fhe_it));
						int face_id = fh.idx();
						if (face_id != -1 && map_tri[face_id] > -1)
						{
							block_tri_.push_back(flipped_tri[j]);
							map_tri[flipped_tri[j]] = block_num_; ++block_num_;
							++add_count;
							break;
						}
					}
				}
			}
			if (add_count == 0) break;
		}
	}

	int ne = mesh_->n_edges();
	std::vector<int> visited_edge(ne, -1); std::vector<int> visited_face(nf, -1);
	block_edge_.clear(); block_edge_.reserve(block_num_ * 3);
	for (int i = 0; i < block_num_; ++i)
	{
		Mesh::FaceHandle fh = mesh_->face_handle(block_tri_[i]);
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(fh);
		for (; fhe_it; ++fhe_it)
		{
			Mesh::EdgeHandle eh = mesh_->edge_handle(fhe_it);
			Mesh::FaceHandle opp_fh = mesh_->face_handle(mesh_->opposite_halfedge_handle(fhe_it));
			if (visited_edge[eh.idx()] == -1 && opp_fh.idx() != -1)
			{
				block_edge_.push_back(OpenMesh::Vec6i(eh.idx(), block_tri_[i], opp_fh.idx(),map_tri[block_tri_[i]], map_tri[opp_fh.idx()], -1));
				visited_edge[eh.idx()] = 1;

				if (map_tri[opp_fh.idx()] < 0 && visited_face[opp_fh.idx()] == -1)
				{
					boundary_tri.push_back(opp_fh.idx()); visited_face[opp_fh.idx()] = 1;
				}
			}
		}
	}

}

//no flipped : true
bool assemble_triangle_speedup_interface::check_one_block(int block_id)
{
	std::vector<int>& one_block_tri = all_block_tri[block_id];
	for (int i = 0; i < one_block_tri.size(); ++i)
	{
		if (is_flipped[one_block_tri[i]] == 1)
		{
			return false;
		}
	}
	return true;
}

bool assemble_triangle_speedup_interface::initilize_vector_field_block(Mesh* mesh_, int block_id, int omp_id)
{
	unsigned nf = mesh_->n_faces();
	if (e1_f.size() != nf) return false;
	std::vector<int>& block_tri = all_block_tri[block_id];
	Eigen::Matrix<double, Eigen::Dynamic, 1>& solution = solution_omp[omp_id]; solution.setZero();
	Eigen::SparseMatrix<double>& hessian = hessian_omp[omp_id]; hessian *= 0;
	for (int i = 0; i < block_tri.size(); ++i)
	{
		int face_id = block_tri[i];
		Eigen::Matrix2d A; A << A_f[face_id][0], A_f[face_id][1], A_f[face_id][2], A_f[face_id][3];
		initialize_one_matrix(A);

		solution(4 * i + 0) = A(0, 0); solution(4 * i + 1) = A(0, 1);
		solution(4 * i + 2) = A(1, 0); solution(4 * i + 3) = A(1, 1);

		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				hessian.coeffRef(4 * i + j, 4 * i + k) = 1.0;
			}
		}
	}

	Eigen::SparseMatrix<double>& C = C_omp[omp_id]; Eigen::Matrix<double, Eigen::Dynamic, 1>& CB = CB_omp[omp_id];
	C.setZero(); CB.setZero();
	int constraint_count = 0; //std::vector<double> b;
	std::vector<OpenMesh::Vec6i>& block_edge = all_block_edge[block_id];
	for (int i = 0; i < block_edge.size(); ++i)
	{
		int edge_id = block_edge[i][0];
		int f0 = block_edge[i][1]; int f1 = block_edge[i][2];
		int new_f0 = block_edge[i][3]; int new_f1 = block_edge[i][4];

		OpenMesh::Vec3d& e = edge_vec[edge_id];
		double ee1_0 = OpenMesh::dot(e, e1_f[f0]); double ee2_0 = OpenMesh::dot(e, e2_f[f0]);
		double ee1_1 = OpenMesh::dot(e, e1_f[f1]); double ee2_1 = OpenMesh::dot(e, e2_f[f1]);

		if (new_f0 < 0 && new_f1 >= 0)
		{
			C.coeffRef(constraint_count, 4 * new_f1 + 0) = ee1_1;
			C.coeffRef(constraint_count, 4 * new_f1 + 1) = ee2_1;
			CB[constraint_count] = ee1_0*A_f[f0][0] + ee2_0*A_f[f0][1];

			C.coeffRef(constraint_count + 1, 4 * new_f1 + 2) = ee1_1;
			C.coeffRef(constraint_count + 1, 4 * new_f1 + 3) = ee2_1;
			CB[constraint_count + 1] = ee1_0*A_f[f0][2] + ee2_0*A_f[f0][3];
		}
		else if (new_f1 < 0 && new_f0 >= 0)
		{
			C.coeffRef(constraint_count, 4 * new_f0 + 0) = ee1_0;
			C.coeffRef(constraint_count, 4 * new_f0 + 1) = ee2_0;
			CB[constraint_count] = ee1_1*A_f[f1][0] + ee2_1*A_f[f1][1];

			C.coeffRef(constraint_count + 1, 4 * new_f0 + 2) = ee1_0;
			C.coeffRef(constraint_count + 1, 4 * new_f0 + 3) = ee2_0;
			CB[constraint_count + 1] = ee1_1*A_f[f1][2] + ee2_1*A_f[f1][3];
		}
		else
		{
			C.coeffRef(constraint_count, 4 * new_f1 + 0) = -ee1_1;
			C.coeffRef(constraint_count, 4 * new_f1 + 1) = -ee2_1;
			C.coeffRef(constraint_count, 4 * new_f0 + 0) = ee1_0;
			C.coeffRef(constraint_count, 4 * new_f0 + 1) = ee2_0;
			CB[constraint_count] = 0;

			C.coeffRef(constraint_count + 1, 4 * new_f1 + 2) = -ee1_1;
			C.coeffRef(constraint_count + 1, 4 * new_f1 + 3) = -ee2_1;
			C.coeffRef(constraint_count + 1, 4 * new_f0 + 2) = ee1_0;
			C.coeffRef(constraint_count + 1, 4 * new_f0 + 3) = ee2_0;
			CB[constraint_count + 1] = 0;
		}
		constraint_count += 2;
	}
	Eigen::SparseMatrix<double>& CT = CT_omp[omp_id];
	Eigen::SparseMatrix<double>& CTC = CTC_omp[omp_id];
	CT = C.transpose();
	CTC = CT*C;
	CBTCB_omp[omp_id] = CB.transpose() * CB;
	int var_num = 4 * all_block_num[block_id];
	hessian += CTC;
	Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, Eigen::Upper>& lltSolver = lltSolver_omp[omp_id];
	lltSolver.analyzePattern(hessian.topLeftCorner(var_num, var_num));

	return true;
}

bool assemble_triangle_speedup_interface::reinitilize_vector_field_block(Mesh* mesh_, Eigen::Matrix<double, Eigen::Dynamic, 1>& x, int block_id)
{
	int nf = all_block_num[block_id];
	for (int i = 0; i < nf; ++i)
	{
		Eigen::Matrix2d A; A << x(4 * i + 0), x(4 * i + 1), x(4 * i + 2), x(4 * i + 3);
		initialize_one_matrix(A);
		x(4 * i + 0) = A(0, 0); x(4 * i + 1) = A(0, 1);
		x(4 * i + 2) = A(1, 0); x(4 * i + 3) = A(1, 1);
	}

	return true;
}


void assemble_triangle_speedup_interface::assign_new_vector_field_block(Mesh* mesh_, int block_id, int omp_id)
{
	std::vector<int>& block_tri = all_block_tri[block_id];
	int block_num = all_block_num[block_id];
	Eigen::Matrix<double, Eigen::Dynamic, 1>& solution = solution_omp[omp_id];
	for (int i = 0; i < block_num; ++i)
	{
		int face_id = block_tri[i];
		
		A_f[face_id][0] = solution(4 * i + 0); A_f[face_id][1] = solution(4 * i + 1);
		A_f[face_id][2] = solution(4 * i + 2); A_f[face_id][3] = solution(4 * i + 3);

		double det = A_f[face_id][0] * A_f[face_id][3] - A_f[face_id][1] * A_f[face_id][2];
		if (bound_k > 1)
		{
			double temp = A_f[face_id][0] * A_f[face_id][0] + A_f[face_id][1] * A_f[face_id][1]
				+ A_f[face_id][2] * A_f[face_id][2] + A_f[face_id][3] * A_f[face_id][3];
			temp /= det;
			if (is_flipped[face_id] == 1 && det > 0 && temp < bound_k + 1 / bound_k)
			{
				--flipped_count;
			}
			else if (is_flipped[face_id] == -1 && (det <= 0 || temp > bound_k + 1 / bound_k))
			{
				++flipped_count;
			}

			if (det <= 0 || temp > bound_k + 1 / bound_k)
			{
				is_flipped[face_id] = 1;
			}
			else
			{
				is_flipped[face_id] = -1;
			}
		}
		else
		{
			if (is_flipped[face_id] == 1 && det > 0)
			{
				--flipped_count;
			}
			else if (is_flipped[face_id] == -1 && det <= 0 )
			{
				++flipped_count;
			}

			if (det <= 0 )
			{
				is_flipped[face_id] = 1;
			}
			else
			{
				is_flipped[face_id] = -1;
			}
		}
		
	}
}

double assemble_triangle_speedup_interface::compute_energy_derivative_modified_hessian(Mesh* mesh_, 
	const Eigen::Matrix<double, Eigen::Dynamic, 1>& x,int block_id, int omp_id)
{
	Eigen::SparseMatrix<double>& CT = CT_omp[omp_id];
	Eigen::SparseMatrix<double>& CTC = CTC_omp[omp_id];
	Eigen::Matrix<double, Eigen::Dynamic, 1>& CB = CB_omp[omp_id];
	current_ivf_energy_omp[omp_id] = x.transpose()*CTC*x;
	current_ivf_energy_omp[omp_id] -= 2.0*x.transpose()*CT*CB;
	current_ivf_energy_omp[omp_id] = std::abs(current_ivf_energy_omp[omp_id] + CBTCB_omp[omp_id]);

	Eigen::Matrix<double, Eigen::Dynamic, 1>& gradient = gradient_omp[omp_id];
	gradient = 2.0*ivf_alpha_omp[omp_id] * (CTC*x - CT*CB);
	Eigen::Matrix4d H;
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix4d> es;
	Eigen::SparseMatrix<double>& hessian = hessian_omp[omp_id]; hessian *= 0; 
	current_dis_energy_omp[omp_id] = 0.0; double s = amips_s;
	int nf = all_block_num[block_id]; double distortion_e;
	std::vector<int>& block_tri = all_block_tri[block_id];
	double g_a1, g_a2, g_b1, g_b2;
	double h_a1_a1, h_a1_a2, h_a1_b1, h_a1_b2, h_a2_a2, h_a2_b1, h_a2_b2, h_b1_b1, h_b1_b2, h_b2_b2;
	current_em_energy_omp[omp_id] = 0.0; double K = (bound_k + 1 / bound_k) / 2;
	for (int face_id = 0; face_id < nf; ++face_id)
	{
		double a1 = x(4 * face_id + 0); double a2 = x(4 * face_id + 1);
		double b1 = x(4 * face_id + 2); double b2 = x(4 * face_id + 3);
		double det_J = a1*b2 - a2*b1;
		double mips_e = 0.5*(a1*a1 + a2*a2 + b1*b1 + b2*b2) / det_J;
		if (is_conformal)
		{
			distortion_e = mips_e;
		}
		else
		{
			double area_e = 0.5*(det_J + 1.0 / det_J);
			distortion_e = 0.5*(mips_e + area_e);
		}
		//AMIPS
		{
			if (is_conformal)
			{
				double temp_f = a1*a1 + a2*a2 + b1*b1 + b2*b2;
				g_a1 = a1 / det_J - (b2*temp_f) / (2 * det_J*det_J);
				g_a2 = a2 / det_J + (b1*temp_f) / (2 * det_J*det_J);
				g_b1 = b1 / det_J + (a2*temp_f) / (2 * det_J*det_J);
				g_b2 = b2 / det_J - (a1*temp_f) / (2 * det_J*det_J);

				h_a1_a1 = 1 / det_J - (2 * a1*b2) / (det_J*det_J) + (b2*b2 * temp_f) / (det_J*det_J*det_J);
				h_a1_a2 = (a1*b1) / (det_J*det_J) - (a2*b2) / (det_J*det_J) - (b1*b2*temp_f) / (det_J*det_J*det_J);
				h_a1_b1 = (a1*a2) / (det_J*det_J) - (b1*b2) / (det_J*det_J) - (a2*b2*temp_f) / (det_J*det_J*det_J);
				h_a1_b2 = (a1*b2*temp_f) / (det_J*det_J*det_J) - b2*b2 / (det_J*det_J) - temp_f / (2 * (det_J*det_J)) - a1*a1 / (det_J*det_J);

				h_a2_a2 = 1 / det_J + (2 * a2*b1) / (det_J*det_J) + (b1*b1 * temp_f) / (det_J*det_J*det_J);
				h_a2_b1 = a2*a2 / (det_J*det_J) + b1*b1 / (det_J*det_J) + temp_f / (2 * (det_J*det_J)) + (a2*b1*temp_f) / (det_J*det_J*det_J);
				h_a2_b2 = (b1*b2) / (det_J*det_J) - (a1*a2) / (det_J*det_J) - (a1*b1*temp_f) / (det_J*det_J*det_J);

				h_b1_b1 = 1 / det_J + (2 * a2*b1) / (det_J*det_J) + (a2*a2 * temp_f) / (det_J*det_J*det_J);
				h_b1_b2 = (a2*b2) / (det_J*det_J) - (a1*b1) / (det_J*det_J) - (a1*a2*temp_f) / (det_J*det_J*det_J);
				h_b2_b2 = 1 / det_J - (2 * a1*b2) / (det_J*det_J) + (a1*a1 * temp_f) / (det_J*det_J*det_J);
			}
			else
			{
				g_a1 = b2 / 4.0 + a1 / (2.0 * det_J) - b2 / (4.0 * det_J*det_J) - (b2*mips_e) / (2.0*det_J);
				g_a2 = a2 / (2.0 * det_J) - b1 / 4.0 + b1 / (4.0 * det_J*det_J) + (b1*mips_e) / (2.0*det_J);
				g_b1 = a2 / (4.0 * det_J*det_J) - a2 / 4.0 + b1 / (2.0 * det_J) + (a2*mips_e) / (2.0*det_J);
				g_b2 = a1 / 4.0 - a1 / (4.0 * det_J*det_J) + b2 / (2.0 * det_J) - (a1*mips_e) / (2.0*det_J);

				h_a1_a1 = 1.0 / (2.0 * det_J) + b2*b2 / (2.0 * det_J*det_J*det_J) - (a1*b2) / (det_J*det_J) + (b2*b2 * mips_e) / (det_J*det_J);
				h_a1_a2 = (a1*b1) / (2.0 * det_J*det_J) - (a2*b2) / (2.0 * det_J*det_J) - (b1*b2) / (2.0 * det_J*det_J*det_J) - (b1*b2*mips_e) / (det_J*det_J);
				h_a1_b1 = (a1*a2) / (2.0 * det_J*det_J) - (a2*b2) / (2.0 * det_J*det_J*det_J) - (b1*b2) / (2.0 * det_J*det_J) - (a2*b2*mips_e) / (det_J*det_J);
				h_a1_b2 = (a1*b2) / (2.0 * det_J *det_J*det_J) - a1*a1 / (2.0 * det_J*det_J) - b2*b2 / (2.0 * det_J*det_J) - mips_e / (2.0 * det_J) - 1.0 / (4.0 * det_J*det_J) + (a1*b2*mips_e) / (det_J *det_J) + 1.0 / 4.0;

				h_a2_a2 = 1.0 / (2.0 * det_J) + b1 *b1 / (2.0 * det_J*det_J*det_J) + (a2*b1) / (det_J*det_J) + (b1*b1*mips_e) / (det_J*det_J);
				h_a2_b1 = 1.0 / (4.0 * det_J*det_J) + a2*a2 / (2.0 * det_J*det_J) + b1*b1 / (2.0 * det_J*det_J) + mips_e / (2.0 * det_J) + (a2*b1) / (2.0 * det_J *det_J*det_J) + (a2*b1*mips_e) / (det_J *det_J) - 1.0 / 4.0;
				h_a2_b2 = (b1*b2) / (2.0 * det_J*det_J) - (a1*b1) / (2.0 * det_J*det_J*det_J) - (a1*a2) / (2.0 * det_J*det_J) - (a1*b1*mips_e) / (det_J*det_J);

				h_b1_b1 = 1.0 / (2.0 * det_J) + a2*a2 / (2.0 * det_J*det_J*det_J) + (a2*b1) / (det_J*det_J) + (a2*a2 *mips_e) / (det_J*det_J);;
				h_b1_b2 = (a2*b2) / (2.0 * det_J*det_J) - (a1*b1) / (2.0 * det_J*det_J) - (a1*a2) / (2.0 * det_J*det_J*det_J) - (a1*a2*mips_e) / (det_J*det_J);
				h_b2_b2 = 1.0 / (2.0 * det_J) + a1*a1 / (2.0 * det_J*det_J*det_J) - (a1*b2) / (det_J*det_J) + (a1*a1 *mips_e) / (det_J*det_J);
			}
			if (!is_bounded)
			{
				if (energy_method == 0)//MIPS
				{
					current_dis_energy_omp[omp_id] += distortion_e;

					gradient(4 * face_id + 0) += g_a1;
					gradient(4 * face_id + 1) += g_a2;
					gradient(4 * face_id + 2) += g_b1;
					gradient(4 * face_id + 3) += g_b2;

					//Eigen::Matrix4d& H = fH[face_id];
					H.coeffRef(0, 0) = h_a1_a1;
					H.coeffRef(0, 1) = h_a1_a2;
					H.coeffRef(0, 2) = h_a1_b1;
					H.coeffRef(0, 3) = h_a1_b2;

					H.coeffRef(1, 1) = h_a2_a2;
					H.coeffRef(1, 2) = h_a2_b1;
					H.coeffRef(1, 3) = h_a2_b2;

					H.coeffRef(2, 2) = h_b1_b1;
					H.coeffRef(2, 3) = h_b1_b2;

					H.coeffRef(3, 3) = h_b2_b2;
				}
				else
				{
					double k = s*distortion_e;
					if (k > 128) k = 128;
					double exp_e = std::exp(k);
					current_dis_energy_omp[omp_id] += exp_e;

					gradient(4 * face_id + 0) += exp_e*s*g_a1;
					gradient(4 * face_id + 1) += exp_e*s*g_a2;
					gradient(4 * face_id + 2) += exp_e*s*g_b1;
					gradient(4 * face_id + 3) += exp_e*s*g_b2;

					//Eigen::Matrix4d& H = fH[face_id];
					H.coeffRef(0, 0) = exp_e*s*h_a1_a1 + exp_e*s*s*g_a1*g_a1;
					H.coeffRef(0, 1) = exp_e*s*h_a1_a2 + exp_e*s*s*g_a1*g_a2;
					H.coeffRef(0, 2) = exp_e*s*h_a1_b1 + exp_e*s*s*g_a1*g_b1;
					H.coeffRef(0, 3) = exp_e*s*h_a1_b2 + exp_e*s*s*g_a1*g_b2;

					H.coeffRef(1, 1) = exp_e*s*h_a2_a2 + exp_e*s*s*g_a2*g_a2;
					H.coeffRef(1, 2) = exp_e*s*h_a2_b1 + exp_e*s*s*g_a2*g_b1;
					H.coeffRef(1, 3) = exp_e*s*h_a2_b2 + exp_e*s*s*g_a2*g_b2;

					H.coeffRef(2, 2) = exp_e*s*h_b1_b1 + exp_e*s*s*g_b1*g_b1;
					H.coeffRef(2, 3) = exp_e*s*h_b1_b2 + exp_e*s*s*g_b1*g_b2;

					H.coeffRef(3, 3) = exp_e*s*h_b2_b2 + exp_e*s*s*g_b2*g_b2;
				}
			}
			else if (is_bounded && is_conformal) //only support conformal distortion and AMIPS
			{
				//if (distortion_e > K * 0.6)
				{
					double k = s*distortion_e;
					if (k > 50) k = 50;
					double exp_e = std::exp(k); double inv_K_d = 1.0 / (K - distortion_e);
					current_dis_energy_omp[omp_id] += exp_e *inv_K_d;
					double inv_K_d_2 = inv_K_d*inv_K_d;
					double inv_K_d_3 = inv_K_d_2 * inv_K_d;

					double exp_g_a1 = exp_e*s*g_a1;
					double exp_g_a2 = exp_e*s*g_a2;
					double exp_g_b1 = exp_e*s*g_b1;
					double exp_g_b2 = exp_e*s*g_b2;

					gradient(4 * face_id + 0) += exp_g_a1*inv_K_d + exp_e*inv_K_d_2*g_a1;
					gradient(4 * face_id + 1) += exp_g_a2*inv_K_d + exp_e*inv_K_d_2*g_a2;
					gradient(4 * face_id + 2) += exp_g_b1*inv_K_d + exp_e*inv_K_d_2*g_b1;
					gradient(4 * face_id + 3) += exp_g_b2*inv_K_d + exp_e*inv_K_d_2*g_b2;

					double exp_h_a1_a1 = exp_e*s*h_a1_a1 + exp_e*s*s*g_a1*g_a1;
					double exp_h_a1_a2 = exp_e*s*h_a1_a2 + exp_e*s*s*g_a1*g_a2;
					double exp_h_a1_b1 = exp_e*s*h_a1_b1 + exp_e*s*s*g_a1*g_b1;
					double exp_h_a1_b2 = exp_e*s*h_a1_b2 + exp_e*s*s*g_a1*g_b2;

					double exp_h_a2_a2 = exp_e*s*h_a2_a2 + exp_e*s*s*g_a2*g_a2;
					double exp_h_a2_b1 = exp_e*s*h_a2_b1 + exp_e*s*s*g_a2*g_b1;
					double exp_h_a2_b2 = exp_e*s*h_a2_b2 + exp_e*s*s*g_a2*g_b2;

					double exp_h_b1_b1 = exp_e*s*h_b1_b1 + exp_e*s*s*g_b1*g_b1;
					double exp_h_b1_b2 = exp_e*s*h_b1_b2 + exp_e*s*s*g_b1*g_b2;

					double exp_h_b2_b2 = exp_e*s*h_b2_b2 + exp_e*s*s*g_b2*g_b2;

					H.coeffRef(0, 0) = exp_h_a1_a1*inv_K_d + exp_g_a1*g_a1*inv_K_d_2 + exp_g_a1*g_a1*inv_K_d_2 + exp_e*h_a1_a1*inv_K_d_2 + 2 * exp_e*g_a1*g_a1*inv_K_d_3;
					H.coeffRef(0, 1) = exp_h_a1_a2*inv_K_d + exp_g_a1*g_a2*inv_K_d_2 + exp_g_a2*g_a1*inv_K_d_2 + exp_e*h_a1_a2*inv_K_d_2 + 2 * exp_e*g_a1*g_a2*inv_K_d_3;
					H.coeffRef(0, 2) = exp_h_a1_b1*inv_K_d + exp_g_a1*g_b1*inv_K_d_2 + exp_g_b1*g_a1*inv_K_d_2 + exp_e*h_a1_b1*inv_K_d_2 + 2 * exp_e*g_a1*g_b1*inv_K_d_3;
					H.coeffRef(0, 3) = exp_h_a1_b2*inv_K_d + exp_g_a1*g_b2*inv_K_d_2 + exp_g_b2*g_a1*inv_K_d_2 + exp_e*h_a1_b2*inv_K_d_2 + 2 * exp_e*g_a1*g_b2*inv_K_d_3;

					H.coeffRef(1, 1) = exp_h_a2_a2*inv_K_d + exp_g_a2*g_a2*inv_K_d_2 + exp_g_a2*g_a2*inv_K_d_2 + exp_e*h_a2_a2*inv_K_d_2 + 2 * exp_e*g_a2*g_a2*inv_K_d_3;
					H.coeffRef(1, 2) = exp_h_a2_b1*inv_K_d + exp_g_a2*g_b1*inv_K_d_2 + exp_g_b1*g_a2*inv_K_d_2 + exp_e*h_a2_b1*inv_K_d_2 + 2 * exp_e*g_a2*g_b1*inv_K_d_3;
					H.coeffRef(1, 3) = exp_h_a2_b2*inv_K_d + exp_g_a2*g_b2*inv_K_d_2 + exp_g_b2*g_a2*inv_K_d_2 + exp_e*h_a2_b2*inv_K_d_2 + 2 * exp_e*g_a2*g_b2*inv_K_d_3;

					H.coeffRef(2, 2) = exp_h_b1_b1*inv_K_d + exp_g_b1*g_b1*inv_K_d_2 + exp_g_b1*g_b1*inv_K_d_2 + exp_e*h_b1_b1*inv_K_d_2 + 2 * exp_e*g_b1*g_b1*inv_K_d_3;
					H.coeffRef(2, 3) = exp_h_b1_b2*inv_K_d + exp_g_b1*g_b2*inv_K_d_2 + exp_g_b2*g_b1*inv_K_d_2 + exp_e*h_b1_b2*inv_K_d_2 + 2 * exp_e*g_b1*g_b2*inv_K_d_3;
					H.coeffRef(3, 3) = exp_h_b2_b2*inv_K_d + exp_g_b2*g_b2*inv_K_d_2 + exp_g_b2*g_b2*inv_K_d_2 + exp_e*h_b2_b2*inv_K_d_2 + 2 * exp_e*g_b2*g_b2*inv_K_d_3;
				}
			}

			H.coeffRef(1, 0) = H.coeffRef(0, 1);
			H.coeffRef(2, 0) = H.coeffRef(0, 2);
			H.coeffRef(3, 0) = H.coeffRef(0, 3);
			H.coeffRef(2, 1) = H.coeffRef(1, 2);
			H.coeffRef(3, 1) = H.coeffRef(1, 3);
			H.coeffRef(3, 2) = H.coeffRef(2, 3);

			es.compute(H, Eigen::EigenvaluesOnly);
			double min_ev = es.eigenvalues()(0);
			if (min_ev < 1e-16)
			{
				min_ev = std::abs(min_ev) + 1e-16;
			}
			else //positive eigenvalues
			{
				min_ev = 0;
			}

			//min_ev = 0;
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					if (i == j)
					{
						hessian.coeffRef(4 * face_id + i, 4 * face_id + j) = H.coeffRef(i, j) + min_ev;
					}
					else
					{
						hessian.coeffRef(4 * face_id + i, 4 * face_id + j) = H.coeffRef(i, j);
					}
				}
			}
		}
		
		if (energy_type == 0)//LSCM
		{
			double lacm_e = ((a1 - b2)*(a1 - b2) + (a2 + b1)*(a2 + b1))*src_face_area[block_tri[face_id]];
			current_em_energy_omp[omp_id] += lacm_e; double g_coef = 2 * mu*src_face_area[block_tri[face_id]];
			gradient(4 * face_id + 0) += g_coef * (a1 - b2);
			gradient(4 * face_id + 1) += g_coef * (a2 + b1);
			gradient(4 * face_id + 2) += g_coef * (a2 + b1);
			gradient(4 * face_id + 3) += g_coef * (b2 - a1);

			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 0) += g_coef;
			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 3) -= g_coef;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 0) -= g_coef;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 3) += g_coef;

			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 1) += g_coef;
			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 2) += g_coef;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 1) += g_coef;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 2) += g_coef;
		}
		else if (energy_type == 1) //distance from initial mapping
		{
			OpenMesh::Vec4d& A_i = A_f_0[block_tri[face_id]];
			double dis_e = ((a1 - A_i[0])*(a1 - A_i[0]) + (a2 - A_i[1])*(a2 - A_i[1]) + (b1 - A_i[2])*(b1 - A_i[2]) + (b2 - A_i[3])*(b2 - A_i[3]));// *src_face_area[block_tri[face_id]];
			double g_coef = 2 * mu;// *src_face_area[block_tri[face_id]];
			current_em_energy_omp[omp_id] += dis_e;
			gradient(4 * face_id + 0) += g_coef * (a1 - A_i[0]);
			gradient(4 * face_id + 1) += g_coef * (a2 - A_i[1]);
			gradient(4 * face_id + 2) += g_coef * (b1 - A_i[2]);
			gradient(4 * face_id + 3) += g_coef * (b2 - A_i[3]);

			hessian.coeffRef(4 * face_id + 0, 4 * face_id + 0) += g_coef;
			hessian.coeffRef(4 * face_id + 3, 4 * face_id + 3) += g_coef;
			hessian.coeffRef(4 * face_id + 1, 4 * face_id + 1) += g_coef;
			hessian.coeffRef(4 * face_id + 2, 4 * face_id + 2) += g_coef;
		}
		
	}

	hessian += 2.0*ivf_alpha_omp[omp_id]*CTC;

	return ivf_alpha_omp[omp_id]*current_ivf_energy_omp[omp_id] + current_dis_energy_omp[omp_id]+ mu*current_em_energy_omp[omp_id];
}

double assemble_triangle_speedup_interface::compute_only_energy(Mesh* mesh_, const Eigen::Matrix<double, Eigen::Dynamic, 1>& x, int block_id, int omp_id)
{
	current_dis_energy_omp[omp_id] = 0.0; double s = amips_s;
	int nf = all_block_num[block_id]; double distortion_e;
	std::vector<int>& block_tri = all_block_tri[block_id];
	current_em_energy_omp[omp_id] = 0.0; double K = (bound_k + 1 / bound_k) / 2.0;
	for (int face_id = 0; face_id < nf; ++face_id)
	{
		double a1 = x(4 * face_id + 0); double a2 = x(4 * face_id + 1);
		double b1 = x(4 * face_id + 2); double b2 = x(4 * face_id + 3);

		double det_J = a1*b2 - a2*b1;
		if (det_J < 1e-8) // min_det_j = 1e-8
		{
			return std::numeric_limits<double>::infinity();
		}

		double mips_e = 0.5*(a1*a1 + a2*a2 + b1*b1 + b2*b2) / det_J;
		if (is_conformal)
		{
			distortion_e = mips_e;
		}
		else
		{
			double area_e = 0.5*(det_J + 1.0 / det_J);
			distortion_e = 0.5*(mips_e + area_e);
		}
		if (!is_bounded)
		{
			if (energy_method == 0)//MIPS
			{
				current_dis_energy_omp[omp_id] += distortion_e;
			}
			else//AMIPS
			{
				double k = s*distortion_e;
				if (k > 50) k = 50;
				double exp_e = std::exp(k);
				current_dis_energy_omp[omp_id] += exp_e;
			}
		}
		else if(is_bounded && is_conformal)
		{
			if (distortion_e > K - 1e-8)
			{
				current_dis_energy_omp[omp_id] = std::numeric_limits<double>::infinity();
				return std::numeric_limits<double>::infinity();
			}

			double k = s*distortion_e;
			if (k > 60) k = 60;
			double exp_e = std::exp(k);
			current_dis_energy_omp[omp_id] += exp_e / (K - distortion_e);
		}
		//LSCM
		if (energy_type == 0)
		{
			double lacm_e = ((a1 - b2)*(a1 - b2) + (a2 + b1)*(a2 + b1))*src_face_area[block_tri[face_id]];
			current_em_energy_omp[omp_id] += lacm_e;
		}
		else if (energy_type ==1)
		{
			OpenMesh::Vec4d& A_i = A_f_0[block_tri[face_id]];
			double dis_e = ((a1 - A_i[0])*(a1 - A_i[0]) + (a2 - A_i[1])*(a2 - A_i[1]) + (b1 - A_i[2])*(b1 - A_i[2]) + (b2 - A_i[3])*(b2 - A_i[3]));// *src_face_area[block_tri[face_id]];
			current_em_energy_omp[omp_id] += dis_e;
		}
	}

	Eigen::SparseMatrix<double>& CT = CT_omp[omp_id];
	Eigen::SparseMatrix<double>& CTC = CTC_omp[omp_id];
	Eigen::Matrix<double, Eigen::Dynamic, 1>& CB = CB_omp[omp_id];
	current_ivf_energy_omp[omp_id] = x.transpose()*CTC*x;
	current_ivf_energy_omp[omp_id] -= 2.0*x.transpose()*CT*CB;
	current_ivf_energy_omp[omp_id] = std::abs(current_ivf_energy_omp[omp_id] + CBTCB_omp[omp_id]);

	return ivf_alpha_omp[omp_id] * current_ivf_energy_omp[omp_id] + current_dis_energy_omp[omp_id] + mu*current_em_energy_omp[omp_id];
}


bool assemble_triangle_speedup_interface::optimize_IVF(Mesh* mesh_, int block_id, int omp_id)
{
	int iter_count = 0; step_size_omp[omp_id] = 1.0; min_ivf_alpha_omp[omp_id] = 1e3; ivf_alpha_omp[omp_id] = 1e5;
	int var_num = 4 * all_block_num[block_id]; bool update_sucess = false;
	bool is_ivf_below_th = false; pre_current_dis_energy_omp[omp_id] = 1; int is_ivf_below_th_count = 0;
	Eigen::Matrix<double, Eigen::Dynamic, 1>& solution = solution_omp[omp_id];
	Eigen::Matrix<double, Eigen::Dynamic, 1>& step = step_omp[omp_id]; step.setZero();
	Eigen::SparseMatrix<double>& hessian = hessian_omp[omp_id];
	Eigen::SimplicialLLT<Eigen::SparseMatrix<double>, Eigen::Upper>& lltSolver = lltSolver_omp[omp_id];
	int optimization_stop = 0; double pre_ivf_energy = 1.0; double pre_ivf_alpha = min_ivf_alpha_omp[omp_id];
	while (iter_count < max_iter && step_size_omp[omp_id] > min_step_size)
	{
		double old_e = compute_energy_derivative_modified_hessian(mesh_, solution, block_id, omp_id);
		lltSolver.factorize(hessian.topLeftCorner(var_num, var_num));
		int status = lltSolver.info();
		if (status == Eigen::Success)
		{
			lambda_omp[omp_id] = min_lambda;
		}
		else
		{
			int run = 0;
			while (status != Eigen::Success && lambda_omp[omp_id] < max_lambda)
			{
				for (int i = 0; i < var_num; i++)
					hessian.coeffRef(i, i) += lambda_omp[omp_id];

				lltSolver.factorize(hessian.topLeftCorner(var_num, var_num));
				status = lltSolver.info();
				if (status != Eigen::Success)
				{
					if (lambda_omp[omp_id] < max_lambda)
						lambda_omp[omp_id] *= 10;
				}
				else
				{
					if (run == 0 && lambda_omp[omp_id] > min_lambda)
					{
						lambda_omp[omp_id] *= 0.1;
					}
				}
				run++;
			}
		}
		if (lambda_omp[omp_id] < max_lambda)
		{
			// compute newton step direction
			step.topLeftCorner(var_num, 1) = lltSolver.solve(-gradient_omp[omp_id].topLeftCorner(var_num, 1));
		}
		else
		{
			// gradient descent
			lambda_omp[omp_id] *= 0.1;
			step = -gradient_omp[omp_id];
		}
		prevSolution_omp[omp_id] = solution + step_size_omp[omp_id] * step;  update_sucess = true;
		double new_e = compute_only_energy(mesh_, prevSolution_omp[omp_id], block_id, omp_id);
		if (new_e > old_e)
		{
			while (new_e > old_e)
			{
				if (step_size_omp[omp_id] < min_step_size)
				{
					update_sucess = false;
					break;
				}
				step_size_omp[omp_id] *= 0.5;
				prevSolution_omp[omp_id] = solution + step_size_omp[omp_id] * step;
				new_e = compute_only_energy(mesh_, prevSolution_omp[omp_id], block_id, omp_id);
			}
		}
		else
		{
			step_size_omp[omp_id] *= 2.0;
		}
		++iter_count;
		//printf("%d A:%3.2e,L:%3.2e,S:%4.3e,I:%e,D:%e,%e\n", iter_count, ivf_alpha_omp[omp_id], lambda_omp[omp_id], step_size_omp[omp_id], current_ivf_energy_omp[omp_id], current_dis_energy_omp[omp_id], current_em_energy_omp[omp_id]);

		if ((current_ivf_energy_omp[omp_id] < 1e-12 || is_ivf_below_th) && !update_sucess) break;

		solution = prevSolution_omp[omp_id];

		if (current_ivf_energy_omp[omp_id] < 1e-12 && !is_ivf_below_th)
		{
			is_ivf_below_th = true; is_ivf_below_th_count = 0;
		}
		else if (current_ivf_energy_omp[omp_id] < 1e-12 && is_ivf_below_th)
		{
			++is_ivf_below_th_count;
			if (std::abs(current_dis_energy_omp[omp_id] - pre_current_dis_energy_omp[omp_id]) / pre_current_dis_energy_omp[omp_id] < 1e-6 || is_ivf_below_th_count > 7)
			{
				break;
			}
		}
		else
		{
			double re_dis = std::abs(current_dis_energy_omp[omp_id] -pre_current_dis_energy_omp[omp_id]) / pre_current_dis_energy_omp[omp_id];
			double re_ivf = std::abs(current_ivf_energy_omp[omp_id] - pre_ivf_energy) / pre_ivf_energy;
			if (re_ivf < 0.01)
			{
				++optimization_stop;
			}
			else
			{
				optimization_stop = 0;
			}
		}

		pre_current_dis_energy_omp[omp_id] = current_dis_energy_omp[omp_id]; pre_ivf_energy = current_ivf_energy_omp[omp_id];
		
		if (step_size_omp[omp_id] < 1e-12 || lambda_omp[omp_id] > 1e10)
		{
			step_size_omp[omp_id] = 1.0;
			lambda_omp[omp_id] = min_lambda;
			min_ivf_alpha_omp[omp_id] = 1e0;
			pre_ivf_alpha = 1e3;
			//update_ivf_alpha(omp_id);
		}
		else
		{
			if (optimization_stop > 1)
			{
				double temp_alpha = current_dis_energy_omp[omp_id] * min_ivf_alpha_omp[omp_id] / current_ivf_energy_omp[omp_id];
				if (temp_alpha > max_ivf_alpha)
				{
					if (max_ivf_alpha < 1e20) max_ivf_alpha *= 10;
					else if (max_ivf_alpha == 1e20 && need_reinitialize)
					{
						reinitilize_vector_field_block(mesh_, solution, block_id);
						compute_only_energy(mesh_, solution, block_id, omp_id);
						max_ivf_alpha = 1e16;
					}
				}
				else
				{
					if (pre_ivf_alpha < 1e7) pre_ivf_alpha *= 10;
					if (max_ivf_alpha > 1e16) max_ivf_alpha *= 0.1;
				}
				if (step_size_omp[omp_id]  < 1.0) step_size_omp[omp_id] = 1.0;
				min_ivf_alpha_omp[omp_id] = pre_ivf_alpha;
				optimization_stop = 0;
			}
			else
			{
				min_ivf_alpha_omp[omp_id] = pre_ivf_alpha;
			}
		}

		update_ivf_alpha(omp_id);
		if (is_ivf_below_th && ivf_alpha_omp[omp_id] < 1e16)
		{
			ivf_alpha_omp[omp_id] = 1e16;
		}
		if (step_size_omp[omp_id] < min_step_size)
		{
			step_size_omp[omp_id] = 1e-10;
		}
	}

	//printf("%d %d Optimize IVF energy: %e \n", omp_id,block_id, current_ivf_energy_omp[omp_id]);
	
	if (iter_count == max_iter && current_ivf_energy_omp[omp_id] > 1e-12)
	{
		/*reconstruct_UV_block(mesh_);
		//reconstruct_UV(mesh_);
		//initilize_vector_field_UV(mesh_);
		printf("after local reconstruct:  ");
		check_flip_by_A();
		printf("----------------------------------------------------\n");*/
		return false;
	}

	assign_new_vector_field_block(mesh_, block_id, omp_id);
	//printf("iteration count : %d\n", iter_count);
	return true;
}

void assemble_triangle_speedup_interface::check_flip_by_A()
{
	flipped_tri.clear(); //int nf = is_flipped.size(); is_flipped.clear(); is_flipped.res
	for (int face_id = 0; face_id < A_f.size(); ++face_id)
	{
		double det = A_f[face_id][0] * A_f[face_id][3] - A_f[face_id][1] * A_f[face_id][2];
		if (det < 0)
		{
			flipped_tri.push_back(face_id);
			is_flipped[face_id] = 1;
		}
		else
		{
			if (bound_k > 1)
			{
				double temp = A_f[face_id][0] * A_f[face_id][0] + A_f[face_id][1] * A_f[face_id][1]
					+ A_f[face_id][2] * A_f[face_id][2] + A_f[face_id][3] * A_f[face_id][3];
				temp /= det;
				if (temp > bound_k + 1 / bound_k)
				{
					flipped_tri.push_back(face_id);
					is_flipped[face_id] = 1;
				}
				else
				{
					is_flipped[face_id] = -1;
				}
			}
			else
			{
				is_flipped[face_id] = -1;
			}
		}
	}
	flipped_count = flipped_tri.size();
	//printf("A Flipped : %d\n", flipped_tri.size());
}

void assemble_triangle_speedup_interface::check_max_d_by_A()
{
	max_dis_id = -1; max_dis_k = 0; std::vector<double> d_(A_f.size(), 0);
	for (int face_id = 0; face_id < A_f.size(); ++face_id)
	{
		double det = A_f[face_id][0] * A_f[face_id][3] - A_f[face_id][1] * A_f[face_id][2];
		double temp = A_f[face_id][0] * A_f[face_id][0] + A_f[face_id][1] * A_f[face_id][1]
			+ A_f[face_id][2] * A_f[face_id][2] + A_f[face_id][3] * A_f[face_id][3];
		temp /= det; d_[face_id] = temp;
		if (temp > max_dis_k)
		{
			max_dis_k = temp; max_dis_id = face_id;
		}
	}
	flipped_tri.clear(); is_flipped.clear(); is_flipped.resize(A_f.size(), -1);
	double d_th = max_dis_k - 1e-3;// -0.01*(max_dis_k - 2);
	for (int face_id = 0; face_id < A_f.size(); ++face_id)
	{
		if (d_[face_id] > d_th)
		{
			flipped_tri.push_back(max_dis_id);
			is_flipped[max_dis_id] = 1;
		}
	}
	
	max_dis_k = 0.5*(max_dis_k + std::sqrt(max_dis_k*max_dis_k - 4));
	bound_k = max_dis_k - 1e-3;
	flipped_count = flipped_tri.size();
}

#include "SPARSE\Sparse_Matrix.h"
#include "SPARSE\Sparse_Solver.h"
void assemble_triangle_speedup_interface::reconstruct_UV(Mesh* mesh_)
{
	unsigned nv = mesh_->n_vertices(); unsigned nf = mesh_->n_faces();
	//fix two points
	Mesh::FaceHandle fh = mesh_->face_handle(0);
	Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(fh);
	Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it);
	mesh_->data(v0).set_New_Pos(OpenMesh::Vec3d(0.0, 0.0, 0.0)); mesh_->data(v0).set_new_pos_fixed(true);
	const OpenMesh::Vec3d& p0 = mesh_->point(v0);
	Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it);
	const OpenMesh::Vec3d& p1 = mesh_->point(v1);
	double x1 = OpenMesh::dot(p1 - p0, e1_f[0]); double y1 = OpenMesh::dot(p1 - p0, e2_f[0]);
	mesh_->data(v1).set_New_Pos(OpenMesh::Vec3d(x1*A_f[0][0] + y1*A_f[0][1], x1*A_f[0][2] + y1*A_f[0][3], 0.0)); mesh_->data(v1).set_new_pos_fixed(true);

	int vertex_count_ok = 0;
	std::vector<int> map_with_vertex_UV(nv, -1);
	for (unsigned i = 0; i < nv; ++i)
	{
		if (mesh_->data(mesh_->vertex_handle(i)).get_new_pos_fixed())
		{
			continue;
		}
		else
		{
			map_with_vertex_UV[i] = vertex_count_ok;
			++vertex_count_ok;
		}
	}

	Sparse_Matrix A(2 * nf, vertex_count_ok, NOSYM, CCS, 2);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle vi = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& pi = mesh_->point(vi);
		double xi = 0; double yi = 0;
		Mesh::VertexHandle vj = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pj = mesh_->point(vj);
		double xj = OpenMesh::dot(pj - pi, e1_f[face_id]); double yj = OpenMesh::dot(pj - pi, e2_f[face_id]);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle vk = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& pk = mesh_->point(vk);
		double xk = OpenMesh::dot(pk - pi, e1_f[face_id]); double yk = OpenMesh::dot(pk - pi, e2_f[face_id]);
		double face_area = OpenMesh::cross(pj - pi, pk - pi).norm();
		double i_face_area = 1.0 / face_area;
		//i
		if (mesh_->data(vi).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vi).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yj - yk)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xk - xj)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yj - yk)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xk - xj)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vi.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yj - yk));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xk - xj));
		}

		//j
		if (mesh_->data(vj).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vj).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yk - yi)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xi - xk)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yk - yi)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xi - xk)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vj.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yk - yi));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xi - xk));
		}
		//k
		if (mesh_->data(vk).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vk).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yi - yj)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xj - xi)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yi - yj)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xj - xi)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vk.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yi - yj));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xj - xi));
		}

		A.fill_rhs_entry(face_id * 2 + 0, 0, A_f[face_id][0]);
		A.fill_rhs_entry(face_id * 2 + 1, 0, A_f[face_id][1]);

		A.fill_rhs_entry(face_id * 2 + 0, 1, A_f[face_id][2]);
		A.fill_rhs_entry(face_id * 2 + 1, 1, A_f[face_id][3]);
	}

	Sparse_Matrix* B = TransposeTimesSelf(&A, CCS, SYM_LOWER, true);
	solve_by_CHOLMOD(B);
	const std::vector<double>& uv = B->get_solution();
	int vertex_id; std::vector<OpenMesh::Vec3d> new_p(mesh_->n_vertices());
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		vertex_id = v_it.handle().idx();
		if (map_with_vertex_UV[vertex_id] >= 0)
		{
			new_p[vertex_id] = OpenMesh::Vec3d(uv[map_with_vertex_UV[vertex_id]], uv[map_with_vertex_UV[vertex_id] + vertex_count_ok], 0.0);
			//mesh_->data(v_it).set_New_Pos(OpenMesh::Vec3d(uv[map_with_vertex_UV[vertex_id]], uv[map_with_vertex_UV[vertex_id] + vertex_count_ok], 0.0));
		}
		else
		{
			new_p[vertex_id] = mesh_->data(v_it).get_New_Pos();
		}
	}
	int ff = 0;
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		double x1 = OpenMesh::dot(p1 - p0, e1_f[face_id]); double y1 = OpenMesh::dot(p1 - p0, e2_f[face_id]);
		double x2 = OpenMesh::dot(p2 - p0, e1_f[face_id]); double y2 = OpenMesh::dot(p2 - p0, e2_f[face_id]);

		const OpenMesh::Vec3d& q0 = new_p[v0.idx()];
		const OpenMesh::Vec3d& q1 = new_p[v1.idx()];
		const OpenMesh::Vec3d& q2 = new_p[v2.idx()];

		//affine matrix
		Eigen::Matrix2d XY, UV, A;
		XY << x1, x2, y1, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse();
		if (A.determinant() < 0)
		{
			++ff;
		}
		else
		{
			if (bound_k > 1)
			{
				Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
				Eigen::Vector2d s_v = svd.singularValues();
				if (is_conformal)
				{
					double temp_error = s_v(0) / s_v(1);
					if (temp_error > bound_k)
					{
						++ff;
					}
				}
				else
				{
					double temp_error = s_v(0) > 1.0 / s_v(1) ? s_v(0) : 1.0 / s_v(1);
					if (temp_error > bound_k)
					{
						++ff;
					}
				}
			}
		}
	}

	if (ff <= flipped_count)
	{
		for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
		{
			mesh_->data(v_it).set_New_Pos(new_p[v_it->idx()]);
		}
	}
	delete B;
}

void assemble_triangle_speedup_interface::optimize_ivf_one_block(Mesh* mesh_, int block_id, int omp_id)
{
	if (!check_one_block(block_id))
	{
		//printf("%d %d\n", block_id, omp_id);
		initilize_vector_field_block(mesh_, block_id, omp_id);
		optimize_IVF(mesh_, block_id, omp_id);
	}
}

#include "graph_coloring.h"
#include "omp.h"
void assemble_triangle_speedup_interface::optimize_integrable_vector_field_speedup(Mesh* mesh_, int max_iter_ /* = 100 */, double bound_k_, bool is_bounded_)
{
	//load_initial_uv(mesh_, filename_uv); 
	//distortion
	//scale_by_average_edge_length(mesh_);
	is_conformal = true; //conformal
	energy_method = 1; amips_s = 2.0; max_iter = max_iter_;
	bound_k = bound_k_; is_bounded = is_bounded_;
	if (bound_k < 1)
	{
		is_bounded = false;
	}

	initilize_vector_field_UV(mesh_);
	if (flipped_count == 0)
	{
		return;
	}

	initilize_vector_field_UV_0(mesh_);
	int update_count = 0; int k_ring = 4; bool updated_UV = false;
	energy_type = -1; //energy type
	bool use_omp = true; int iter_count = 0; need_reinitialize = true;
	long start_t = clock();

	while (iter_count < 2 && k_ring < 65)
	{
		select_block(mesh_, k_ring);
		
		int flipped_size = flipped_tri.size(); //v_color.resize(flipped_size);
		graph_coloring(flipped_size, graph_color_edge, v_same_color);
		int n_color_ = v_same_color.size();
		//printf("---------------------------------------------------------------\n");
		//printf("Flip_Count %d, N of Color: %d, Ring size : %d, %d\n", flipped_size, n_color_, k_ring, max_block_num);
		if (flipped_size > n_color_ * 1.5) use_omp = true;
		else use_omp = false;
		use_omp = false;
		if (use_omp)
		{
			for (unsigned i = 0; i < n_color_; ++i)
			{
				int one_color_size = v_same_color[i].size();
#pragma omp parallel for schedule(dynamic)
				for (int j = 0; j < one_color_size; ++j)
				{
					int id = omp_get_thread_num();
					int block_id = v_same_color[i][j];
					optimize_ivf_one_block(mesh_, block_id, id);
				}
				if (flipped_count == 0) break;
			}
		}
		else
		{
			for (unsigned i = 0; i < n_color_; ++i)
			{
				int one_color_size = v_same_color[i].size();
				for (int j = 0; j < one_color_size; ++j)
				{
					int block_id = v_same_color[i][j];
					optimize_ivf_one_block(mesh_, block_id, 0);
				}
				if (flipped_count == 0) break;
			}
		}
		//printf("---------------------------------------------------------------\n");
		if (flipped_count == 0) break;
		
		k_ring += 2; check_flip_by_A();
		if (k_ring > 20)
		{
			need_reinitialize = false;
			
			k_ring = 32; select_block(mesh_, k_ring);
			int flipped_size = flipped_tri.size(); //v_color.resize(flipped_size);
			graph_coloring(flipped_size, graph_color_edge, v_same_color);
		
			int one_color_size = v_same_color[0].size();
#pragma omp parallel for schedule(dynamic)
			for (int j = 0; j < one_color_size; ++j)
			{
				int id = omp_get_thread_num();
				int block_id = v_same_color[0][j];
				optimize_ivf_one_block(mesh_, block_id, id);
				assign_new_vector_field_block(mesh_, block_id, id);
			}
			reconstruct_UV(mesh_);
			initilize_vector_field_UV(mesh_);
			if (flipped_count == 0)
			{
				updated_UV = true;
				break;
			}
			//amips_s *= 0.25;
			k_ring = 8; ++iter_count; need_reinitialize = true;
		}
	}

	if (flipped_count > 0)
	{
		printf("---------------------------------------------------------------\n");
		printf("Failed, may be caused by low distortion bound!\n");
		printf("---------------------------------------------------------------\n");
	}

	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	printf("Optimization Time: %f s; Ring size : %d\n", t, k_ring);

	if (!updated_UV) reconstruct_UV(mesh_);
}

void assemble_triangle_speedup_interface::optimize_integrable_vector_field_optimal_bound(Mesh* mesh_, int max_iter_ /* = 100 */)
{
	is_conformal = true; //conformal
	energy_method = 1; amips_s = 2.0; max_iter = max_iter_;
	//bound_k = bound_k_;
	is_bounded = true; bound_k = 100;

	initilize_vector_field_UV(mesh_);
	bound_k = max_dis_k - 1e-3;
	flipped_tri.clear(); flipped_tri.push_back(max_dis_id);
	is_flipped.clear(); is_flipped.resize(mesh_->n_faces(), -1); is_flipped[max_dis_id] = 1; flipped_count = 1;
	printf("Maximal Distortion: %f\n", bound_k);

	initilize_vector_field_UV_0(mesh_);
	int update_count = 0; int k_ring = 4; bool updated_UV = false;
	energy_type = -1; //energy type
	bool use_omp = true; int iter_count = 0; need_reinitialize = true;
	long start_t = clock(); double f_bound_max = 0;
	
	while (iter_count < 200 )
	{
		select_block(mesh_, k_ring);
		int flipped_size = flipped_tri.size(); //v_color.resize(flipped_size);
		graph_coloring(flipped_size, graph_color_edge, v_same_color);
		int n_color_ = v_same_color.size();
		//printf("---------------------------------------------------------------\n");
		//printf("Flip_Count %d, N of Color: %d, Ring size : %d, %d\n", flipped_size, n_color_, k_ring, max_block_num);
		if (flipped_size > n_color_ * 1.5) use_omp = true;
		else use_omp = false;
		use_omp = false;
		if (use_omp)
		{
			for (unsigned i = 0; i < n_color_; ++i)
			{
				int one_color_size = v_same_color[i].size();
#pragma omp parallel for schedule(dynamic)
				for (int j = 0; j < one_color_size; ++j)
				{
					int id = omp_get_thread_num();
					int block_id = v_same_color[i][j];
					optimize_ivf_one_block(mesh_, block_id, id);
				}
				if (flipped_count == 0) break;
			}
		}
		else
		{
			for (unsigned i = 0; i < n_color_; ++i)
			{
				int one_color_size = v_same_color[i].size();
				for (int j = 0; j < one_color_size; ++j)
				{
					int block_id = v_same_color[i][j];
					optimize_ivf_one_block(mesh_, block_id, 0);
				}
				if (flipped_count == 0) break;
			}
		}
		//printf("---------------------------------------------------------------\n");

		if (flipped_count == 0 || k_ring == 8)
		{
			++iter_count;
			check_max_d_by_A();
			if (std::abs(f_bound_max - max_dis_k) < 1e-6)
			{
				f_bound_max = max_dis_k;
				updated_UV = true;
				reconstruct_UV(mesh_); initilize_vector_field_UV(mesh_);
				//printf("%f\n", std::abs(f_bound_max - max_dis_k));
				//if (std::abs(f_bound_max - max_dis_k) < 0.05)
				//{
					break;
				//}
			}
			//printf("[%d] Maximal Distortion: %f, %d; %d\n", iter_count, max_dis_k, flipped_count, k_ring);
			k_ring = 4;
			f_bound_max = max_dis_k;
		}
		else
		{
			k_ring += 2;
		}
	}

	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	printf("Optimization Time: %f s; Round : %d\n", t, iter_count);

	if (!updated_UV) reconstruct_UV(mesh_);
}