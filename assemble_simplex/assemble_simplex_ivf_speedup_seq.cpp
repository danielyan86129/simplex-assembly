#include "assemble_simplex.h"

void assemble_triangle_interface::initilize_vector_field_UV(Mesh* mesh_)
{
	unsigned nf = mesh_->n_faces(); flipped_tri.clear(); is_flipped.clear(); is_flipped.resize(nf, -1);
	U_f.resize(nf); V_f.resize(nf); e1_f.resize(nf); e2_f.resize(nf); A_f.resize(nf); src_face_area.resize(nf, 0);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& p0 = mesh_->point(v0);
		Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& p1 = mesh_->point(v1);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle v2 = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& p2 = mesh_->point(v2);

		OpenMesh::Vec3d e1 = (p1 - p0); double l01 = e1.norm(); e1.normalize();
		OpenMesh::Vec3d n = OpenMesh::cross(p1 - p0, p2 - p0); src_face_area[face_id] = n.norm() * 0.5;
		n.normalize();
		OpenMesh::Vec3d e2 = OpenMesh::cross(n, e1);
		double x2 = OpenMesh::dot(p2 - p0, e1); double y2 = OpenMesh::dot(p2 - p0, e2);

		e1_f[face_id] = e1; e2_f[face_id] = e2;

		const OpenMesh::Vec3d& q0 = mesh_->data(v0).get_New_Pos();
		const OpenMesh::Vec3d& q1 = mesh_->data(v1).get_New_Pos();
		const OpenMesh::Vec3d& q2 = mesh_->data(v2).get_New_Pos();

		//affine matrix
		Eigen::Matrix2d XY, UV, A;
		XY << l01, x2, 0, y2; UV << q1[0] - q0[0], q2[0] - q0[0], q1[1] - q0[1], q2[1] - q0[1];
		A = UV*XY.inverse();
		A_f[face_id][0] = A(0, 0); A_f[face_id][1] = A(0, 1); A_f[face_id][2] = A(1, 0); A_f[face_id][3] = A(1, 1);
		if (A.determinant() < 0)
		{
			flipped_tri.push_back(face_id);
			is_flipped[face_id] = 1;
		}
		else
		{
			Eigen::JacobiSVD<Eigen::Matrix2d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
			Eigen::Vector2d s_v = svd.singularValues();
			double temp_error = s_v(0) / s_v(1);
		}
	}
	//printf("Flipped Triangles: %d(>%f)\n", flipped_tri.size(), bound_K);
	printf("Flipped Triangles: %d\n", flipped_tri.size());
}

void assemble_triangle_interface::select_one_block(Mesh* mesh_, int seed_face, int k_ring)
{
	int nf = mesh_->n_faces(); int ne = mesh_->n_edges();
	map_tri.clear(); map_tri.resize(nf, -1); std::vector<int> map_edge(ne, -1);
	block_tri.clear(); block_tri.push_back(seed_face);
	int start_id = 0; map_tri[seed_face] = 0; int current_tri_size = -1; block_num = 1;
	for (int i = 0; i < k_ring; ++i)
	{
		current_tri_size = block_tri.size();
		for (int j = start_id; j < current_tri_size; ++j)
		{
			for (Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(mesh_->face_handle(block_tri[j])); fhe_it; ++fhe_it)
			{
				int edge_id = mesh_->edge_handle(fhe_it).idx();
				Mesh::FaceHandle fh = mesh_->face_handle(mesh_->opposite_halfedge_handle(fhe_it));
				int face_id = fh.idx();

				if (face_id != -1 && map_tri[face_id] == -1)
				{
					block_tri.push_back(face_id); map_tri[face_id] = block_num; ++block_num;
				}
			}
		}
		start_id = current_tri_size;
	}

	//check boundary cell with fold-over
	while (1)
	{
		int add_count = 0;
		for (int j = 0; j < flipped_tri.size(); ++j)
		{
			if (map_tri[flipped_tri[j]] == -1) //in the block
			{
				for (Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(mesh_->face_handle(flipped_tri[j])); fhe_it; ++fhe_it)
				{
					Mesh::FaceHandle fh = mesh_->face_handle(mesh_->opposite_halfedge_handle(fhe_it));
					int face_id = fh.idx();
					if (face_id != -1 && map_tri[face_id] > -1)
					{
						block_tri.push_back(flipped_tri[j]);
						map_tri[flipped_tri[j]] = block_num; ++block_num;
						++add_count;
						break;
					}
				}
			}
		}
		if (add_count == 0) break;
	}
}


//no flipped : true
bool assemble_triangle_interface::check_one_block()
{
	for (int i = 0; i < block_num; ++i)
	{
		if (is_flipped[block_tri[i]] == 1)
		{
			return false;
		}
	}
	return true;
}

bool assemble_triangle_interface::initilize_vector_field_block(Mesh* mesh_)
{
	unsigned nf = mesh_->n_faces();
	if (e1_f.size() != nf) return false;

	std::vector<Eigen::Triplet<double> > f_triplets; solution.resize(4 * block_num);
	for (int i = 0; i < block_tri.size(); ++i)
	{
		int face_id = block_tri[i];
		Eigen::Matrix2d A; A << A_f[face_id][0], A_f[face_id][1], A_f[face_id][2], A_f[face_id][3];
		initialize_one_matrix(A, true);

		solution(4 * i + 0) = A(0, 0); solution(4 * i + 1) = A(0, 1);
		solution(4 * i + 2) = A(1, 0); solution(4 * i + 3) = A(1, 1);
		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				f_triplets.push_back(Eigen::Triplet<double>(4 * i + j, 4 * i + k, 1.0));
			}
		}
	}

	std::vector<Eigen::Triplet<double> > c_triplets; std::vector<Eigen::Triplet<double> > triplets;
	int constraint_count = 0; std::vector<double> b;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		if (mesh_->is_boundary(e_it)) continue;

		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		Mesh::FaceHandle fh0 = mesh_->face_handle(heh0); int f0 = fh0.idx();
		Mesh::FaceHandle fh1 = mesh_->face_handle(heh1); int f1 = fh1.idx();

		if (map_tri[f0] < 0 && map_tri[f1] < 0) continue;

		int new_f0 = map_tri[f0]; int new_f1 = map_tri[f1]; //set new index

		const OpenMesh::Vec3d& p0 = mesh_->point(mesh_->to_vertex_handle(heh1));
		const OpenMesh::Vec3d& p1 = mesh_->point(mesh_->to_vertex_handle(heh0));
		OpenMesh::Vec3d e = (p1 - p0).normalize();

		double ee1_0 = OpenMesh::dot(e, e1_f[f0]); double ee2_0 = OpenMesh::dot(e, e2_f[f0]);
		double ee1_1 = OpenMesh::dot(e, e1_f[f1]); double ee2_1 = OpenMesh::dot(e, e2_f[f1]);

		int heh0_id = heh0.idx();

		if (new_f0 < 0 && new_f1 >= 0)
		{
			b.push_back(ee1_0*A_f[f0][0] + ee2_0*A_f[f0][1]);
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 0, ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 1, ee2_1));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 1, 1.0));

			b.push_back(ee1_0*A_f[f0][2] + ee2_0*A_f[f0][3]);
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 2, ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 3, ee2_1));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 3, 1.0));
		}
		else if (new_f1 < 0 && new_f0 >= 0)
		{
			b.push_back(ee1_1*A_f[f1][0] + ee2_1*A_f[f1][1]);
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 0, ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 1, ee2_0));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 1, 1.0));

			b.push_back(ee1_1*A_f[f1][2] + ee2_1*A_f[f1][3]);
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 2, ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 3, ee2_0));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 3, 1.0));
		}
		else
		{
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 0, ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 1, ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 0, -ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 1, -ee2_1));
			b.push_back(0);

			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 2, ee1_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 3, ee2_0));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 2, -ee1_1));
			triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 3, -ee2_1));
			b.push_back(0);

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f0 + 1, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 0, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count, 4 * new_f1 + 1, 1.0));

			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f0 + 3, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 2, 1.0));
			c_triplets.push_back(Eigen::Triplet<double>(constraint_count + 1, 4 * new_f1 + 3, 1.0));
		}
		constraint_count += 2;
	}

	Eigen::SparseMatrix<double> C; C.resize(constraint_count, 4 * block_num);
	C.setFromTriplets(c_triplets.begin(), c_triplets.end());
	CT = C.transpose();
	CTC.resize(4 * block_num, 4 * block_num);
	CTC = CT*C;
	Eigen::SparseMatrix<double> H;
	H.resize(4 * block_num, 4 * block_num);
	H.setFromTriplets(f_triplets.begin(), f_triplets.end());

	hessian.resize(4 * block_num, 4 * block_num);
	hessian = H + CTC;
	lltSolver.analyzePattern(hessian);

	C *= 0.0;
	C.setFromTriplets(triplets.begin(), triplets.end());
	CT = C.transpose();
	CTC = CT*C;

	CB.resize(constraint_count);
	for (int i = 0; i < constraint_count; ++i)
	{
		CB(i) = b[i];
	}
	CBTCB = CB.transpose() * CB;

	gradient.resize(4 * block_num); prevSolution.resize(4 * block_num); step.resize(4 * block_num);

	return true;
}

void assemble_triangle_interface::assign_new_vector_field_block(Mesh* mesh_)
{
	for (int i = 0; i < block_tri.size(); ++i)
	{
		int face_id = block_tri[i];
		U_f[face_id] = solution(4 * i + 0)*e1_f[face_id] + solution(4 * i + 1)*e2_f[face_id];
		V_f[face_id] = solution(4 * i + 2)*e1_f[face_id] + solution(4 * i + 3)*e2_f[face_id];

		A_f[face_id][0] = solution(4 * i + 0); A_f[face_id][1] = solution(4 * i + 1);
		A_f[face_id][2] = solution(4 * i + 2); A_f[face_id][3] = solution(4 * i + 3);
	}
	double ivf_error = 0.0;
	for (Mesh::EdgeIter e_it = mesh_->edges_begin(); e_it != mesh_->edges_end(); ++e_it)
	{
		if (mesh_->is_boundary(e_it)) continue;

		Mesh::HalfedgeHandle heh0 = mesh_->halfedge_handle(e_it, 0);
		Mesh::HalfedgeHandle heh1 = mesh_->halfedge_handle(e_it, 1);
		const OpenMesh::Vec3d& p0 = mesh_->point(mesh_->to_vertex_handle(heh1));
		const OpenMesh::Vec3d& p1 = mesh_->point(mesh_->to_vertex_handle(heh0));
		OpenMesh::Vec3d e = (p1 - p0).normalize();

		Mesh::FaceHandle fh0 = mesh_->face_handle(heh0); int f0 = fh0.idx();
		Mesh::FaceHandle fh1 = mesh_->face_handle(heh1); int f1 = fh1.idx();

		double ee1_0 = OpenMesh::dot(e, e1_f[f0]); double ee2_0 = OpenMesh::dot(e, e2_f[f0]);
		double ee1_1 = OpenMesh::dot(e, e1_f[f1]); double ee2_1 = OpenMesh::dot(e, e2_f[f1]);

		int heh0_id = heh0.idx();
		double err = A_f[f0][0] * ee1_0 + A_f[f0][1] * ee2_0 - A_f[f1][0] * ee1_1 - A_f[f1][1] * ee2_1;
		ivf_error += err*err;
		err = A_f[f0][2] * ee1_0 + A_f[f0][3] * ee2_0 - A_f[f1][2] * ee1_1 - A_f[f1][3] * ee2_1;
		ivf_error += err*err;
	}
	printf("IVF error : %e\n", ivf_error);
}

void assemble_triangle_interface::check_flip_by_A()
{
	flipped_tri.clear(); //int nf = is_flipped.size(); is_flipped.clear(); is_flipped.res
	for (int face_id = 0; face_id < A_f.size(); ++face_id)
	{
		double det = A_f[face_id][0] * A_f[face_id][3] - A_f[face_id][1] * A_f[face_id][2];
		if (det < 0)
		{
			flipped_tri.push_back(face_id);
			is_flipped[face_id] = 1;
		}
		else
		{
			is_flipped[face_id] = -1;
		}
	}
	printf("A Flipped : %d\n", flipped_tri.size());
}

#include "SPARSE\Sparse_Matrix.h"
#include "SPARSE\Sparse_Solver.h"
void assemble_triangle_interface::reconstruct_UV(Mesh* mesh_)
{
	unsigned nv = mesh_->n_vertices(); unsigned nf = mesh_->n_faces();
	//fix two points
	Mesh::FaceHandle fh = mesh_->face_handle(0);
	Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(fh);
	Mesh::VertexHandle v0 = mesh_->from_vertex_handle(fhe_it);
	mesh_->data(v0).set_New_Pos(OpenMesh::Vec3d(0.0, 0.0, 0.0)); mesh_->data(v0).set_new_pos_fixed(true);
	const OpenMesh::Vec3d& p0 = mesh_->point(v0);
	Mesh::VertexHandle v1 = mesh_->to_vertex_handle(fhe_it);
	const OpenMesh::Vec3d& p1 = mesh_->point(v1);
	double x1 = OpenMesh::dot(p1 - p0, e1_f[0]); double y1 = OpenMesh::dot(p1 - p0, e2_f[0]);
	mesh_->data(v1).set_New_Pos(OpenMesh::Vec3d(x1*A_f[0][0] + y1*A_f[0][1], x1*A_f[0][2] + y1*A_f[0][3], 0.0)); mesh_->data(v1).set_new_pos_fixed(true);

	int vertex_count_ok = 0;
	std::vector<int> map_with_vertex_UV(nv, -1);
	for (unsigned i = 0; i < nv; ++i)
	{
		if (mesh_->data(mesh_->vertex_handle(i)).get_new_pos_fixed())
		{
			continue;
		}
		else
		{
			map_with_vertex_UV[i] = vertex_count_ok;
			++vertex_count_ok;
		}
	}

	Sparse_Matrix A(2 * nf, vertex_count_ok, NOSYM, CCS, 2);
	for (Mesh::FaceIter f_it = mesh_->faces_begin(); f_it != mesh_->faces_end(); ++f_it)
	{
		int face_id = f_it.handle().idx();
		Mesh::FaceHalfedgeIter fhe_it = mesh_->fh_iter(f_it);
		Mesh::VertexHandle vi = mesh_->from_vertex_handle(fhe_it); const OpenMesh::Vec3d& pi = mesh_->point(vi);
		double xi = 0; double yi = 0;
		Mesh::VertexHandle vj = mesh_->to_vertex_handle(fhe_it); const OpenMesh::Vec3d& pj = mesh_->point(vj);
		double xj = OpenMesh::dot(pj - pi, e1_f[face_id]); double yj = OpenMesh::dot(pj - pi, e2_f[face_id]);
		Mesh::HalfedgeHandle heh = mesh_->next_halfedge_handle(fhe_it);
		Mesh::VertexHandle vk = mesh_->to_vertex_handle(heh); const OpenMesh::Vec3d& pk = mesh_->point(vk);
		double xk = OpenMesh::dot(pk - pi, e1_f[face_id]); double yk = OpenMesh::dot(pk - pi, e2_f[face_id]);
		double face_area = OpenMesh::cross(pj - pi, pk - pi).norm();
		double i_face_area = 1.0 / face_area;
		//i
		if (mesh_->data(vi).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vi).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yj - yk)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xk - xj)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yj - yk)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xk - xj)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vi.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yj - yk));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xk - xj));
		}

		//j
		if (mesh_->data(vj).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vj).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yk - yi)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xi - xk)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yk - yi)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xi - xk)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vj.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yk - yi));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xi - xk));
		}
		//k
		if (mesh_->data(vk).get_new_pos_fixed())
		{
			OpenMesh::Vec3d np = mesh_->data(vk).get_New_Pos();
			//u
			A.fill_rhs_entry(face_id * 2 + 0, 0, -i_face_area*(yi - yj)*np[0]);
			A.fill_rhs_entry(face_id * 2 + 1, 0, -i_face_area*(xj - xi)*np[0]);
			//v
			A.fill_rhs_entry(face_id * 2 + 0, 1, -i_face_area*(yi - yj)*np[1]);
			A.fill_rhs_entry(face_id * 2 + 1, 1, -i_face_area*(xj - xi)*np[1]);
		}
		else
		{
			int var_id = map_with_vertex_UV[vk.idx()];
			A.fill_entry(face_id * 2 + 0, var_id, i_face_area*(yi - yj));
			A.fill_entry(face_id * 2 + 1, var_id, i_face_area*(xj - xi));
		}

		A.fill_rhs_entry(face_id * 2 + 0, 0, A_f[face_id][0]);
		A.fill_rhs_entry(face_id * 2 + 1, 0, A_f[face_id][1]);

		A.fill_rhs_entry(face_id * 2 + 0, 1, A_f[face_id][2]);
		A.fill_rhs_entry(face_id * 2 + 1, 1, A_f[face_id][3]);
	}

	Sparse_Matrix* B = TransposeTimesSelf(&A, CCS, SYM_LOWER, true);
	solve_by_CHOLMOD(B);
	const std::vector<double>& uv = B->get_solution();
	int vertex_id;
	for (Mesh::VertexIter v_it = mesh_->vertices_begin(); v_it != mesh_->vertices_end(); ++v_it)
	{
		vertex_id = v_it.handle().idx();
		if (map_with_vertex_UV[vertex_id] >= 0)
		{
			mesh_->data(v_it).set_New_Pos(OpenMesh::Vec3d(uv[map_with_vertex_UV[vertex_id]], uv[map_with_vertex_UV[vertex_id] + vertex_count_ok], 0.0));
		}
	}
	delete B;
}

void assemble_triangle_interface::optimize_integrable_vector_field_speedup_seq(Mesh* mesh_, const char* filename_uv, int max_iter_ /* = 100 */)
{
	load_initial_uv(mesh_, filename_uv);
	initilize_vector_field_UV(mesh_);
	//distortion
	bool is_c = true; //conformal
	energy_method = 1; amips_s = 1.0; max_iter = max_iter_; energy_type = 2;

	int update_count = 0; int k_ring = 4;
	
	long start_t = clock();
	while (k_ring < 65)
	{
		for (int i = 0; i < flipped_tri.size(); ++i)
		{
			select_one_block(mesh_, flipped_tri[i], k_ring);
			if (!check_one_block())
			{
				initilize_vector_field_block(mesh_);
				if (optimize_IVF(mesh_, is_c))
				{
					assign_new_vector_field_block(mesh_);
					check_flip_by_A();
					printf("%d %d\n", i, flipped_tri.size());
					++update_count;
				}
			}
		}
		check_flip_by_A();
		if (flipped_tri.size() == 0) break;

		printf("%d %d %d\n", k_ring, update_count, flipped_tri.size());
		//if (update_count == 0)
		{
			k_ring *= 2;
		}
	}
	long end_t = clock();
	long diff = end_t - start_t;
	double t = (double)(diff) / CLOCKS_PER_SEC;
	//printf("Optimize Time: %f s; Ring size : %d, %d\n", t, k_ring, block_num);
	printf("Optimization Time: %f s\n", t);

	reconstruct_UV(mesh_);
	std::string uv(filename_uv);
	uv.append("_result.uv");
	save_result_uv(mesh_, uv.c_str());
}
