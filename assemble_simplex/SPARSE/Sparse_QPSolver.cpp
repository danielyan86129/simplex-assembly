#include "Sparse_Config.h"
#include "Sparse_QPSolver.h"

#include "QpGenData.h"
#include "QpGenVars.h"
#include "QpGenResiduals.h"
#include "GondzioSolver.h"
#include "QpGenSparseMa27.h"

#pragma comment (lib, "libMA27.lib")
#pragma comment (lib, "libooqpbase.lib")
#pragma comment (lib, "libooqpgensparse.lib")
#pragma comment (lib, "libooqpgondzio.lib")
#pragma comment (lib, "libooqpsparse.lib")

int Sparse_QPSolver(int N, 
					double *solution, 
					Sparse_Matrix *Q, 
					double *c, 
					Sparse_Matrix *A, 
					double *b, 
					Sparse_Matrix *C, 
					double *d, 
					double *f, 
					char *di, 
					char *fi, 
					double *l, 
					double *u, 
					char *li, 
					char *ui
					)
{
	if (Q && Q->issymmetric() == false)
	{
		return -1;
	}
	std::vector<int> Q_row_vec, Q_col_vec;
	std::vector<double> Q_val_vec;
	int *Q_rowid = NULL;
	int *Q_colid = NULL;
	double *Q_values = NULL;
	int Qnnz = 0;
	Sparse_Matrix *tmp_Q = Q;
	if (Q != NULL)
	{
		if (Q->get_storage_type() != TRIPLE || Q->get_storage_type() == SYM_BOTH)
		{
			tmp_Q = convert_sparse_matrix_storage(Q, TRIPLE, SYM_LOWER);
		}
		tmp_Q->get_compress_data(Q_row_vec, Q_col_vec, Q_val_vec);
		Q_rowid = &Q_row_vec[0];
		Q_colid = &Q_col_vec[0];
		Q_values = &Q_val_vec[0];
		Qnnz = (int)Q_val_vec.size();
	}

	int A_row = 0;
	int *A_rowid = NULL;
	int *A_colid = NULL;
	double *A_values = NULL;
	int Annz = 0;
	std::vector<int> A_row_vec, A_col_vec;
	std::vector<double> A_val_vec;
	Sparse_Matrix *tmp_A = A;

	if (A != NULL)
	{
		if (A->get_storage_type() != TRIPLE || A->issym_store_upper_or_lower())
		{
			tmp_A = convert_sparse_matrix_storage(A, TRIPLE, A->issym_store_upper_or_lower()?SYM_BOTH:NOSYM);
		}
		tmp_A->get_compress_data(A_row_vec, A_col_vec, A_val_vec);
		A_rowid = &A_row_vec[0];
		A_colid = &A_col_vec[0];
		A_values = &A_val_vec[0];
		Annz = (int)A_val_vec.size();
		A_row = (int)A->rows();
	}

	int C_row = 0;
	int *C_rowid = NULL;
	int *C_colid = NULL;
	double *C_values = NULL;
	int Cnnz = 0;
	std::vector<int> C_row_vec, C_col_vec;
	std::vector<double> C_val_vec;
	Sparse_Matrix *tmp_C = C;
	if (C != NULL)
	{
		if (C->get_storage_type() != TRIPLE || C->issym_store_upper_or_lower())
		{
			tmp_C = convert_sparse_matrix_storage(C, TRIPLE, C->issym_store_upper_or_lower()?SYM_BOTH:NOSYM);
		}
		tmp_C->get_compress_data(C_row_vec, C_col_vec, C_val_vec);
		C_rowid = &C_row_vec[0];
		C_colid = &C_col_vec[0];
		C_values = &C_val_vec[0];
		Cnnz = (int)C_val_vec.size();
		C_row = (int)C->rows();
	}

	QpGenSparseMa27 *qp = new QpGenSparseMa27( N, A_row, C_row, Qnnz, Annz, Cnnz);

	QpGenData *prob = (QpGenData * ) qp->copyDataFromSparseTriple(
		c, Q_rowid, Qnnz, Q_colid, Q_values, 
		l, li, u, ui, 
		A_rowid, Annz, A_colid, A_values, b, 
		C_rowid, Cnnz, C_colid, C_values, 
		d, di, f, fi);

	QpGenVars *vars = (QpGenVars *) qp->makeVariables( prob );
	QpGenResiduals *resid = (QpGenResiduals *) qp->makeResiduals( prob );

	GondzioSolver *s = new GondzioSolver( qp, prob );

	int ierr = s->solve(prob, vars, resid);
	vars->x->copyIntoArray(solution);

	if (Q && tmp_Q != Q)
	{
		delete tmp_Q;
	}
	if (A && tmp_A != A)
	{
		delete tmp_A;
	}
	if (C && tmp_C != C)
	{
		delete tmp_C;
	}
	delete qp;
	delete prob;
	delete vars;
	delete resid;
	delete s;
	return ierr;
	return 0;
}
