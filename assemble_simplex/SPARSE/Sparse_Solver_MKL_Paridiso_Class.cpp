#include "Sparse_Solver_MKL_Paridiso_Class.h"

sparse_solver_mkl_pardiso::sparse_solver_mkl_pardiso(SOLVER_TYPE_MKL_PARDISO solver_type_)
{
	init_para(solver_type_);
}

sparse_solver_mkl_pardiso::~sparse_solver_mkl_pardiso()
{}

void sparse_solver_mkl_pardiso::init_para(SOLVER_TYPE_MKL_PARDISO solver_type_)
{
	have_initialized = false;

	solver_type = solver_type_;
	if(solver_type == SOLVER_MKL)
	{
		mtype = 11;
		pt.resize(64, NULL);
		iparm.resize(64,0);
		maxfct = 0; mnum = 0; phase = 0;error = 0; msglvl = 0;

		iparm[0] = 0;			// No solver default				// revised by jie @ 14/05/2007
		iparm[1] = 2;			// Fill-in reordering from METIS */
		iparm[2] = 16;			// omp_get_max_threads();	/* Numbers of processors, value of OMP_NUM_THREADS */
		iparm[7] = 2;			// Max numbers of iterative refinement steps
		iparm[9] = 13;			// Perturb the pivot elements with 1E-13
		iparm[10] = 1;			// Use nonsymmetric permutation and scaling MPS
		iparm[17] = -1;			// Output: Number of nonzeros in the factor LU
		iparm[18] = -1;			// Output: Mflops for LU factorization
		iparm[19] = 0;			// Output: Numbers of CG Iterations
		maxfct = 1;				// Maximum number of numerical factorizations
		mnum = 1;				// Which factorization to use
		//msglvl = 1;				// Print statistical information in file
		error = 0;				// Initialize error flag
	}
#ifdef USE_PARDISO
	else if(solver_type == SOLVER_PARDISO)
	{
		mtype = 11;
		pt.resize(64, NULL);
		iparm.resize(64,0);
		dparm.resize(64,0.0);
		maxfct = 0; mnum = 0; phase = 0;error = 0; msglvl = 0; solver = 0;
		pardisoinit(&pt[0],  &mtype, &solver, &iparm[0], &dparm[0], &error);
		if (error != 0) 
		{
			if (error == -10 )
				printf("No license file found \n");
			if (error == -11 )
				printf("License is expired \n");
			if (error == -12 )
				printf("Wrong username or hostname \n");
		}
		else
		{
			printf("[PARDISO]: License check was successful ... \n");
		}

		char* var = getenv("OMP_NUM_THREADS"); int num_procs;
		if(var != NULL)
		{
			sscanf( var, "%d", &num_procs );
		}
		else 
		{
			printf("Set environment OMP_NUM_THREADS to 1");
			exit(1);
		}
		iparm[2]  = num_procs;
	}
#endif
}

bool sparse_solver_mkl_pardiso::factorize_sparse_matrix(Sparse_Matrix* m_sparse_matrix, bool positive_define)
{
	if (m_sparse_matrix==NULL)
	{
		return false;
	}
	Sparse_Matrix *A = m_sparse_matrix;
#ifdef USE_MKL
	if(solver_type == SOLVER_MKL)
	{
		if (m_sparse_matrix->get_storage_type() != CCS || m_sparse_matrix->issym_store_upper_or_lower())
		{
			A = convert_sparse_matrix_storage(m_sparse_matrix, CCS, m_sparse_matrix->issym_store_upper_or_lower()?SYM_BOTH:m_sparse_matrix->get_symmetric_type());
		}
		int row = (int)A->rows();
		int col = (int)A->cols();
		A->get_compress_data(rowind, colptr, values, FORTRAN_TYPE);
		m_num_column_of_RHS = (int)A->get_num_rhs();
		n = row;
		if( m_sparse_matrix->issymmetric() )
		{
			mtype = 1;
		}
		//manually specify the matrix type
		if(positive_define)
		{
			mtype = 2;
		}

		phase = 11;
		PARDISO (&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &values.front(), &colptr.front(), &rowind.front(),
			&idum, &m_num_column_of_RHS, &iparm.front(), &msglvl, &ddum, &ddum, &error);

		if (error != 0)
		{
			printf("\nERROR during symbolic factorization: %d", error);
			return false;
		}
		//////////////////////////////////////////////////////////////////////////
		// .. Numerical factorization
		//////////////////////////////////////////////////////////////////////////
		phase = 22;
		PARDISO (&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &values.front(), &colptr.front(), &rowind.front(), 
			&idum, &m_num_column_of_RHS, &iparm.front(), &msglvl, &ddum, &ddum, &error);
		if (error != 0)
		{
			printf("\nERROR during numerical factorization: %d", error);
			return false;
		}
	}
#endif

#ifdef USE_PARDISO
	if(solver_type == SOLVER_PARDISO)
	{
		if (m_sparse_matrix->get_storage_type() != CRS || m_sparse_matrix->issym_store_upper_or_lower())
		{
			A = convert_sparse_matrix_storage(m_sparse_matrix, CRS, m_sparse_matrix->issym_store_upper_or_lower()?m_sparse_matrix->get_symmetric_type():SYM_UPPER);
		}

		int row = (int)A->rows();
		int col = (int)A->cols();
		A->get_compress_data(rowind, colptr, values, FORTRAN_TYPE);
		m_num_column_of_RHS = (int)A->get_num_rhs();
		if( m_sparse_matrix->issymmetric() )
		{
			mtype = -2;
		}

		if(positive_define)
		{
			mtype = 2;
		}

		/* -------------------------------------------------------------------- */
		/*  .. pardiso_chk_matrix(...)                                          */
		/*     Checks the consistency of the given matrix.                      */
		/*     Use this functionality only for debugging purposes               */
		/* -------------------------------------------------------------------- */

		pardiso_chkmatrix(&mtype, &n, &values[0], &rowind[0] ,&colptr[0], &error);
		if (error != 0) 
		{
			printf("\nERROR in consistency of matrix: %d", error);
			return false;
		}

		/* -------------------------------------------------------------------- */
		/* ..  pardiso_chkvec(...)                                              */
		/*     Checks the given vectors for infinite and NaN values             */
		/*     Input parameters (see PARDISO user manual for a description):    */
		/*     Use this functionality only for debugging purposes               */
		/* -------------------------------------------------------------------- */

		//pardiso_chkvec(&n, &m_num_column_of_RHS, &B[0], &error);
		if (error != 0)
		{
			printf("\nERROR  in right hand side: %d", error);
			return false;
		}

		/* -------------------------------------------------------------------- */
		/* .. pardiso_printstats(...)                                           */
		/*    prints information on the matrix to STDOUT.                       */
		/*    Use this functionality only for debugging purposes                */
		/* -------------------------------------------------------------------- */

		//pardiso_printstats(&mtype, &n, &values[0], &rowind[0] ,&colptr[0] , &m_num_column_of_RHS, &B.front(), &error);
		if (error != 0)
		{
			printf("\nERROR right hand side: %d", error);
			return false;
		}


		//////////////////////////////////////////////////////////////////////////
		// .. Reordering and Symbolic Factorization. This step also allocates
		// all memory that is necessary for the factorization. */
		//////////////////////////////////////////////////////////////////////////
		phase = 11;
		pardiso(&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &values[0], &rowind[0] ,&colptr[0] , &idum, &m_num_column_of_RHS, &iparm[0], &msglvl, &ddum, &ddum, &error, &dparm[0]);

		//pardiso (&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &values[0], &colptr[0], &rowind[0],
		//&idum, &m_num_column_of_RHS, &iparm[0], &msglvl, &ddum, &ddum, &error);

		if (error != 0)
		{
			printf("\nERROR during symbolic factorization: %d", error);
			return false;
		}


		//////////////////////////////////////////////////////////////////////////
		// .. Numerical factorization
		//////////////////////////////////////////////////////////////////////////
		phase = 22;
		pardiso(&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &values.front(), &rowind[0] ,&colptr[0] , &idum, &m_num_column_of_RHS, &iparm.front(), &msglvl, &ddum, &ddum, &error, &dparm[0]);
		if (error != 0)
		{
			printf("\nERROR during numerical factorization: %d", error);
			return false;
		}
	}
#endif

	if(A != m_sparse_matrix)
		delete A;

	have_initialized = true;
	return true;
}

bool sparse_solver_mkl_pardiso::back_substitution(std::vector<double>& B, std::vector<double>& X)
{
	int B_Size = B.size();
	if(B_Size % n != 0)
		return false;

#ifdef USE_MKL
	if(solver_type == SOLVER_MKL)
	{
		//////////////////////////////////////////////////////////////////////////
		// .. Back substitution and iterative refinement
		//////////////////////////////////////////////////////////////////////////
		phase = 33;
		PARDISO (&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &values.front(), &colptr.front(), &rowind.front(), 
			&idum, &m_num_column_of_RHS, &iparm.front(), &msglvl, &B.front(), &X.front(), &error);
		if (error != 0) {
			printf("\nERROR during solution: %d", error);
			return false;
		}

	}
#endif

#ifdef USE_PARDISO
	if(solver_type == SOLVER_PARDISO)
	{
		//////////////////////////////////////////////////////////////////////////
		// .. Back substitution and iterative refinement
		//////////////////////////////////////////////////////////////////////////
		phase = 33;
		pardiso(&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &values.front(), &rowind[0] ,&colptr[0] , &idum, &m_num_column_of_RHS, &iparm.front(), &msglvl, &B.front(), &X.front(), &error, &dparm[0]);
		if (error != 0)
		{
			printf("\nERROR during solution: %d", error);
			return false;
		}
	}
#endif

	return true;
}

bool sparse_solver_mkl_pardiso::release_memory()
{
#ifdef USE_MKL
	if(solver_type == SOLVER_MKL)
	{
		phase = -1; /* Release internal memory. */
		PARDISO (&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &ddum, &colptr.front(), &rowind.front(), 
			&idum, &m_num_column_of_RHS, &iparm.front(), &msglvl, &ddum, &ddum, &error);

	}
#endif

#ifdef USE_PARDISO
	if(solver_type == SOLVER_PARDISO)
	{
		phase = -1; /* Release internal memory. */
		pardiso(&pt.front(), &maxfct, &mnum, &mtype, &phase, &n, &ddum,  &rowind[0] ,&colptr[0] , &idum, &m_num_column_of_RHS, &iparm.front(), &msglvl, &ddum, &ddum, &error, &dparm[0]);
	}
#endif

	return true;
}