#ifndef SPARSE_CONFIG_H
#define SPARSE_CONFIG_H

//	Special version for Geometry4Architecture

/** \defgroup MathSuite MathSuite: Sparse Matrix, Solver, QPSolver */

//! BLAS and LAPACK
/*!
*	<A HREF="http://www.tacc.utexas.edu/resources/software/"> GOTOBLAS 1.26 </A>
*	<A HREF="http://www.netlib.org/lapack/"> LAPACK 3.1.1 </A> 
*/
//////////////////////////////////////////////////////////////////////////
//#pragma comment (lib, "libgoto_banias-r1.26.lib") 
//#pragma comment (lib, "libgoto_core2-r1.26.lib") 
#pragma comment (lib, "BLAS.lib") 
#pragma comment (lib, "LAPACK.lib") 


#define USE_CHOLMOD
//#define USE_TAUCS
#define USE_UMFPACK
//#define USE_ARPACK
//#define USE_SUPERLU
//#define USE_MKL
//#define USE_PARDISO
#define USE_EIGEN

#endif //SPARSE_CONFIG_H
