#ifndef SPARSE_SOLVER_MKL_PARDISO_H
#define SPARSE_SOLVER_MKL_PARDISO_H
#include "Sparse_Matrix.h"
#include "Sparse_Config.h"

#ifdef USE_MKL
#include <mkl.h>
#include <mkl_spblas.h>
#pragma comment (lib, "mkl_solver_lp64.lib")
#pragma comment (lib, "mkl_intel_lp64_dll.lib")
#pragma comment (lib, "mkl_intel_thread_dll.lib")
#pragma comment (lib, "mkl_core_dll.lib")
#pragma comment (lib, "libiomp5md.lib")
#endif

#ifdef USE_PARDISO
#pragma comment (lib, "libpardiso412-WIN-X86-64.lib")
extern "C" void pardisoinit (void   *, int    *,   int *, int *, double *, int *);
extern "C" void pardiso     (void   *, int    *,   int *, int *,    int *, int *, 
	double *, int    *,    int *, int *,   int *, int *,
	int *, double *, double *, int *, double *);
extern "C" void pardiso_chkmatrix  (int *, int *, double *, int *, int *, int *);
extern "C" void pardiso_chkvec     (int *, int *, double *, int *);
extern "C" void pardiso_printstats (int *, int *, double *, int *, int *, int *, double *, int *);
#endif

enum SOLVER_TYPE_MKL_PARDISO
{
	SOLVER_MKL = 0,
	SOLVER_PARDISO
};

class sparse_solver_mkl_pardiso
{
public:
	sparse_solver_mkl_pardiso(SOLVER_TYPE_MKL_PARDISO solver_type_);
	~sparse_solver_mkl_pardiso();

	void init_para(SOLVER_TYPE_MKL_PARDISO solver_type_);
	bool factorize_sparse_matrix(Sparse_Matrix* m_sparse_matrix, bool positive_define = false);
	bool back_substitution(std::vector<double>& B, std::vector<double>& X);
	bool release_memory();

	bool have_initialized;
private:
	SOLVER_TYPE_MKL_PARDISO solver_type;
	int n;
	int mtype;
	std::vector<void*> pt;
	std::vector<int> iparm; std::vector<double> dparm;
	int maxfct;int mnum;int phase;int error;int msglvl; int solver;
	double ddum; int idum;

	std::vector<int> rowind;
	std::vector<int> colptr;
	std::vector<double> values;
	int m_num_column_of_RHS;
};

#endif
